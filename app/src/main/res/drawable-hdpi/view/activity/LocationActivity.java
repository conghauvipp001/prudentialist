package vn.mobiapps.pruf.view.activity;

import android.content.Intent;
import android.os.Bundle;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.utils.Constant;
import vn.mobiapps.pruf.utils.ShareReference;

public class LocationActivity extends vn.mobiapps.pruf.view.base.BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        Constant.context = this;
        pushFragment(new vn.mobiapps.pruf.view.fragment.location.LocationFragment());
    }

    @Override
    public void onBackPressed() {
        popFragment();
    }

    @Override
    public void endFragment() {
        super.endFragment();
        if (ShareReference.isLogin) {
            Intent intent = new Intent(this, vn.mobiapps.pruf.view.activity.CustomerPageActivity.class);
            startActivity(intent);
            this.finish();
        } else {
            Intent intent = new Intent(this, vn.mobiapps.pruf.view.activity.LoginActivity.class);
            startActivity(intent);
            this.finish();
        }
    }
}
