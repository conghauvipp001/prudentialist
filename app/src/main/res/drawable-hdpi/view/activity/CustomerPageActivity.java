package vn.mobiapps.pruf.view.activity;

import android.os.Bundle;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.utils.Constant;
import vn.mobiapps.pruf.view.fragment.customerpage.CustomerPageFragment;


public class CustomerPageActivity extends vn.mobiapps.pruf.view.base.TabbarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentTabbar(R.layout.activity_customer_page);
        Constant.context = this;
        pushFragment(new CustomerPageFragment());
    }

    @Override
    public void onBackPressed() {
        popFragment();
    }

    @Override
    public void endFragment() {
        super.endFragment();
        this.finish();
    }
}
