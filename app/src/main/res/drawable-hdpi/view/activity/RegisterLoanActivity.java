package vn.mobiapps.pruf.view.activity;

import android.content.Intent;
import android.os.Bundle;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.model.model_view.RegisterModel;
import vn.mobiapps.pruf.utils.Constant;
import vn.mobiapps.pruf.view.base.BaseChooseAppActivity;
import vn.mobiapps.pruf.view.fragment.regiterloan.InputValuesFragment;


public class RegisterLoanActivity extends BaseChooseAppActivity {

    public RegisterModel registerModel;
    public String cuid = "";
    public int countRequest = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentChoose(R.layout.activity_register_loan);
        Constant.context = this;
        registerModel = new RegisterModel();
        pushFragment(new InputValuesFragment());
    }

    @Override
    public void onBackPressed() {
        popFragment();
    }

    @Override
    public void endFragment() {
        super.endFragment();
        Intent intent = new Intent(this, vn.mobiapps.pruf.view.activity.LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        this.finish();
    }

    public RegisterModel getRegisterModel() {
        return registerModel;
    }

    public void setRegisterModel(RegisterModel registerModel) {
        this.registerModel = registerModel;
    }
}
