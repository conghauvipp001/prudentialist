package vn.mobiapps.pruf.view.activity;

import android.content.Intent;
import android.os.Bundle;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.utils.Constant;
import vn.mobiapps.pruf.view.fragment.changepin.ChangePinFragment;


public class ChangePinActivity extends vn.mobiapps.pruf.view.base.BaseActivity {

    public String currenPin = "", newPin = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pin);
        Constant.context = this;
        pushFragment(new ChangePinFragment());
    }

    @Override
    public void onBackPressed() {
        popFragment();
    }

    @Override
    public void endFragment() {
        super.endFragment();
        Intent intent = new Intent(this, vn.mobiapps.pruf.view.activity.CustomerPageActivity.class);
        startActivity(intent);
        this.finish();
    }
}
