package vn.mobiapps.pruf.view.activity;

import android.content.Intent;
import android.os.Bundle;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.model.model_data.myprofile.UserProfile;
import vn.mobiapps.pruf.utils.Constant;


public class MyProfileActivity extends vn.mobiapps.pruf.view.base.BaseActivity {

    public UserProfile userProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        Constant.context = this;
        userProfile = (UserProfile) getIntent().getSerializableExtra("UserProfile");
        pushFragment(new vn.mobiapps.pruf.view.fragment.myprofile.MyProfileFragment());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        popFragment();
    }

    @Override
    public void endFragment() {
        super.endFragment();
        Intent intent = new Intent(this, vn.mobiapps.pruf.view.activity.CustomerPageActivity.class);
        startActivity(intent);
        this.finish();
        //this.overridePendingTransition(R.anim.pop_enter_menu, R.anim.pop_exit_menu);
    }
}
