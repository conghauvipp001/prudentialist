package vn.mobiapps.pruf.view.activity;

import android.os.Bundle;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.utils.Constant;


public class LoginActivity extends vn.mobiapps.pruf.view.base.BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Constant.context = this;
        pushFragment(new vn.mobiapps.pruf.view.fragment.login.LoginFragment());
    }
}
