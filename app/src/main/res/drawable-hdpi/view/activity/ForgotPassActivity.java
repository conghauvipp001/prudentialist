package vn.mobiapps.pruf.view.activity;

import android.content.Intent;
import android.os.Bundle;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.utils.Constant;
import vn.mobiapps.pruf.view.base.BaseChooseAppActivity;
import vn.mobiapps.pruf.view.fragment.forgotpass.ForFotPassStep1;


public class ForgotPassActivity extends BaseChooseAppActivity {

    public String cuid = "", styleClick = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentChoose(R.layout.activity_forgotpass);
        Constant.context = this;
        pushFragment(new ForFotPassStep1());
    }

    @Override
    public void onBackPressed() {
        popFragment();
    }

    @Override
    public void endFragment() {
        super.endFragment();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        this.finish();
    }
}
