package vn.mobiapps.pruf.view.activity;

import android.os.Bundle;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.utils.Constant;
import vn.mobiapps.pruf.view.base.BaseActivity;
import vn.mobiapps.pruf.view.fragment.changepass.ChangePassFragment;

public class ChangePassActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);
        Constant.context = this;
        pushFragment(new ChangePassFragment());
    }
}
