package vn.mobiapps.pruf.view.activity;

import android.os.Bundle;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.utils.Constant;
import vn.mobiapps.pruf.view.fragment.tutorial.TutorialFragment;


public class TutorialActivity extends vn.mobiapps.pruf.view.base.BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        Constant.context = this;
        pushFragment(new TutorialFragment());
    }
}
