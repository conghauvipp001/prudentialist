package vn.mobiapps.pruf.view.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.apimanager.APIManager;
import vn.mobiapps.pruf.services.FirebaseIDTask;
import vn.mobiapps.pruf.utils.ShareReference;
import vn.mobiapps.pruf.utils.preference.StringPrefs;

import static vn.mobiapps.pruf.utils.Constant.context;
import static vn.mobiapps.pruf.utils.ShareReference.MY_PREFS_NAME;

public class FlashActivity extends AppCompatActivity {

    private final int SPLASH_TIME_OUT = 3000;
    private String username;
    ShareReference preference = new ShareReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash);
        context = this;

        // set isLogin = false
        SharedPreferences.Editor editor = this.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putBoolean("isLogin", false);
        editor.apply();
        ShareReference.isLogin = false;

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        username = prefs.getString("Username", null);
        String accessToken = prefs.getString("AccessToken", "123");
        APIManager.mAccessToken_FB = accessToken;

        //Get firebase token
        if (ShareReference.firebaseTokenPref == null) {
            ShareReference.firebaseTokenPref = new StringPrefs(context, "firebase_token", "");
        }
        FirebaseMessaging.getInstance().subscribeToTopic("Prud");
        String token = FirebaseInstanceId.getInstance().getToken();
        if (token != null) {
            FirebaseIDTask firebase = new FirebaseIDTask(getApplication());
            firebase.execute(token);
            Log.e("FirebaseToken : ", token);
            // Luu vao SharedPreferences
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editorSave = preferences.edit();
            editorSave.putString("FirebaseToken", token);
            editorSave.apply();
            preference.setFirebaseToken(token);
        }
        this.GoToLoginActivity();
    }

    private void GoToLoginActivity() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(vn.mobiapps.pruf.view.activity.FlashActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
