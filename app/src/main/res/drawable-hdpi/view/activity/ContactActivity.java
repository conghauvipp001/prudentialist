package vn.mobiapps.pruf.view.activity;

import android.content.Intent;
import android.os.Bundle;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.utils.Constant;


public class ContactActivity extends vn.mobiapps.pruf.view.base.BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        Constant.context = this;
        pushFragment(new vn.mobiapps.pruf.view.fragment.contact.ContactFragment());
    }

    @Override
    public void onBackPressed() {
        popFragment();
    }

    @Override
    public void endFragment() {
        super.endFragment();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        this.fileList();
    }
}
