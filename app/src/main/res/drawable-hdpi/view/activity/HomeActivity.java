package vn.mobiapps.pruf.view.activity;

import android.content.Intent;
import android.os.Bundle;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.model.model_data.login.ResponeLogin;
import vn.mobiapps.pruf.utils.Constant;
import vn.mobiapps.pruf.view.base.BaseChooseAppActivity;


public class HomeActivity extends BaseChooseAppActivity {

    public ResponeLogin logndata;
    public Boolean isRetrieve;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentChoose(R.layout.activity_home);
        Constant.context = this;
        logndata = (ResponeLogin) getIntent().getSerializableExtra("LognData");
        isRetrieve = getIntent().getBooleanExtra("isRetrieve", false);
        try {
            if (logndata != null && logndata.data != null) {
                if (Boolean.valueOf(logndata.data.verifyOTP)) {
                    vn.mobiapps.pruf.view.fragment.home.otpreceiving.OTPReceivingFragment f = new vn.mobiapps.pruf.view.fragment.home.otpreceiving.OTPReceivingFragment();
                    pushFragment(f);
                } else {
                    pushFragment(new vn.mobiapps.pruf.view.fragment.home.WelcomeFragment());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        popFragment();
    }

    @Override
    public void endFragment() {
        super.endFragment();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        this.finish();
    }
}
