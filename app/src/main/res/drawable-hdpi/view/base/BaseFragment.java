package vn.mobiapps.pruf.view.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import vn.mobiapps.pruf.model.model_view.ResponseErrorModel;
import vn.mobiapps.pruf.utils.Constant;
import vn.mobiapps.pruf.utils.ShareReference;
import vn.mobiapps.pruf.widgets.dialog.DialogFactory;

public abstract class BaseFragment extends Fragment implements vn.mobiapps.pruf.view.base.IBaseView, View.OnClickListener {
    protected abstract int resLayout();
    protected abstract void createFragment();
    protected abstract void setupFragment(View view);
    protected abstract void setOnClickListenerFragment();

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(resLayout(), container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createFragment();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupFragment(view);
        setOnClickListenerFragment();
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public void showLoading() {
        showProgressBar();
    }

    @Override
    public void hideLoading() {
        hideProgressBar();
    }

    @Override
    public void onSuccess(Object objectSuccess) {

    }

    @Override
    public void onError(Object objectError) {
        try {
            final String message = ((ResponseErrorModel) objectError).getResult_description();
            String code = ((ResponseErrorModel) objectError).getResult_code();
            if (ShareReference.isLogin) {
                if (code.equals(Constant.RESPONE_CODE)) {
                    DialogFactory.createMessageDialogSessionTimeout(getActivity(), message, code);
                } else {
                    showMessage(message);
                }
            } else {
                showMessage(message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateUI(Object object) {

    }

    public void setLocale(String lang) {
        if (lang != null && !lang.equals("")) {
            ((vn.mobiapps.pruf.view.base.BaseActivity)getActivity()).setLocale(lang);
        }
    }

    public void showProgressBar() {
        ((vn.mobiapps.pruf.view.base.BaseActivity)getActivity()).showProgressBar();
    }

    public void hideProgressBar() {
        ((vn.mobiapps.pruf.view.base.BaseActivity)getActivity()).hideProgressBar();
    }

    public void hideKeyboard() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((vn.mobiapps.pruf.view.base.BaseActivity)getActivity()).hideKeyboard();
            }
        });
    }

    protected void showMessage(final String message) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogFactory.dialog_Message(getActivity(), message);
            }
        });
    }

    public void CheckNetworkRequest(final vn.mobiapps.pruf.view.base.BaseActivity baseActivity, final CheckNetworkListener listener) {
        ((vn.mobiapps.pruf.view.base.BaseActivity)getActivity()).CheckNetworkRequest(baseActivity, listener);
    }

    public interface CheckNetworkListener {
        void Connected();
    }
}
