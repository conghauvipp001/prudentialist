package vn.mobiapps.pruf.view.base;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.apimanager.APIManager;
import vn.mobiapps.pruf.model.model_data.myprofile.ModelMyProfile;
import vn.mobiapps.pruf.model.model_view.ModelMenu;
import vn.mobiapps.pruf.presenter.tabbar.ITabbarPresenter;
import vn.mobiapps.pruf.presenter.tabbar.TabbarPresenterImpl;
import vn.mobiapps.pruf.utils.ShareReference;
import vn.mobiapps.pruf.view.activity.LoginActivity;
import vn.mobiapps.pruf.view.activity.MyProfileActivity;
import vn.mobiapps.pruf.widgets.residemenu.ResideMenu;

import static vn.mobiapps.pruf.utils.ShareReference.MY_PREFS_NAME;

public abstract class TabbarActivity extends BaseActivity implements IViewTabbarActivity{
    private FrameLayout layout_content;
    private ArrayList<ModelMenu> listmenu;
    private ImageView imgMenu;
    private LinearLayout layoutLogout;
    private TextView txtLogout, txtUndo;
    private ResideMenu resideMenu;
    private ITabbarPresenter presenter;
    private ModelMyProfile modelMyProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabbar);
        pushPop.setIDContentReplace(R.id.layout_content);//Notes
        if (presenter == null) {
            presenter = new TabbarPresenterImpl(this);
        }
        setUpviewTabbar();
        onClickListionerTabbar();
    }

    public FrameLayout setContentTabbar(@LayoutRes int layoutResID) {
        layout_content.removeAllViews();
        getLayoutInflater().inflate(layoutResID, layout_content, true);
        return layout_content;
    }

    private void setUpviewTabbar() {
        layout_content = findViewById(R.id.layout_content);
        imgMenu = findViewById(R.id.imgMenu);
        layoutLogout = findViewById(R.id.layoutLogout);
        txtLogout = findViewById(R.id.txtLogout);
        txtUndo = findViewById(R.id.txtUndo);
        vn.mobiapps.pruf.view.base.TabbarActivity parentActivity = (vn.mobiapps.pruf.view.base.TabbarActivity) this;
        resideMenu = parentActivity.getResideMenu();
        getMyProfile();
        setUpMenu();
    }

    private void onClickListionerTabbar() {
        imgMenu.setOnClickListener(this);
        layoutLogout.setOnClickListener(this);
        txtLogout.setOnClickListener(this);
        txtUndo.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgMenu: {
                hideKeyboard();
                resideMenu.openMenu();
            }
            break;
            case R.id.layoutLogout:
            case R.id.txtUndo: {
                hideKeyboard();
                layoutLogout.setVisibility(View.GONE);
            }
            break;
            case R.id.txtLogout: {
                hideKeyboard();
                gotoLogout();
            }
            break;
            default:
                break;
        }
    }

    /*Add listFilter Menu*/
    private void setUpMenu() {
        try {
            resideMenu = new ResideMenu(this, this);
            resideMenu.setUse3D(true);
            resideMenu.setBackground(R.color.colorWhite);
            initDataMenu();
            resideMenu.attachToActivity(this, listmenu);
            resideMenu.setMenuListener(menuListener);
            //valid scale factor is between 0.0f and 1.0f. leftmenu'width is 150dip.
            resideMenu.setScaleValue(0.6f);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
        @Override
        public void openMenu() {
            //Toast.makeText(TabbarActivity.this, "Menu is opened!", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void closeMenu() {
            //Toast.makeText(TabbarActivity.this, "Menu is closed!", Toast.LENGTH_SHORT).show();
        }
    };

    public void initDataMenu() {
        listmenu = new ArrayList<ModelMenu>();

        ArrayList<ModelMenu> listGroupIWant = new ArrayList<>();
        listGroupIWant.add(new ModelMenu(getString(R.string.txt_History_required), "", 0, "2", false, new ArrayList<ModelMenu>(), true));
        listGroupIWant.add(new ModelMenu(getString(R.string.txt_change_infor), "", 0, "2", false, new ArrayList<ModelMenu>(), true));
        listGroupIWant.add(new ModelMenu(getString(R.string.txt_Request_payment), "", 0, "2", false, new ArrayList<ModelMenu>(), true));
        listGroupIWant.add(new ModelMenu(getString(R.string.txt_Ask_for), "", 0, "2", false, new ArrayList<ModelMenu>(), true));
        listmenu.add(new ModelMenu(getString(R.string.txt_iwant), "", 0, "1", true, listGroupIWant, true));

        listmenu.add(new ModelMenu(getString(R.string.txt_Transaction), "", R.drawable.calendar, "1", false, new ArrayList<ModelMenu>(), true));
        /*listmenu.add(new ModelMenu(getString(R.string.txt_ChangePin), "",0, "1", false, new ArrayList<ModelMenu>(), true));
        listmenu.add(new ModelMenu(getString(R.string.txt_Password), "",0, "1", false, new ArrayList<ModelMenu>(), true));*/

        ArrayList<ModelMenu> listGroupSetting = new ArrayList<>();
        listGroupSetting.add(new ModelMenu(getString(R.string.txt_loans), "", 0, "2", false, new ArrayList<ModelMenu>(), true));
        listGroupSetting.add(new ModelMenu(getString(R.string.txt_Get_notifications), "", 0, "2", false, new ArrayList<ModelMenu>(), true));
        listGroupSetting.add(new ModelMenu(getString(R.string.txt_Receive_OTP), "", 0, "2", false, new ArrayList<ModelMenu>(), true));
        listGroupSetting.add(new ModelMenu(getString(R.string.txt_Language), "", 0, "3", false, new ArrayList<ModelMenu>(), true));
        listGroupSetting.add(new ModelMenu(getString(R.string.txt_Background), "", 0, "2", false, new ArrayList<ModelMenu>(), true));
        listGroupSetting.add(new ModelMenu(getString(R.string.txt_Equipment_management), "", 0, "2", false, new ArrayList<ModelMenu>(), true));
        listmenu.add(new ModelMenu(getString(R.string.txt_Setting), "", 0, "1", true, listGroupSetting, true));

        ArrayList<ModelMenu> listGroupAbout = new ArrayList<>();
        listGroupAbout.add(new ModelMenu(getString(R.string.txt_hotline_), "", 0, "2", false, new ArrayList<ModelMenu>(), true));
        listGroupAbout.add(new ModelMenu(getString(R.string.txt_email_1), "", 0, "2", false, new ArrayList<ModelMenu>(), true));
        listGroupAbout.add(new ModelMenu(getString(R.string.txt_map), "", 0, "2", false, new ArrayList<ModelMenu>(), true));
        listGroupAbout.add(new ModelMenu(getString(R.string.txt_chat), "", 0, "2", false, new ArrayList<ModelMenu>(), true));
        listmenu.add(new ModelMenu(getString(R.string.txt_ContactPVFC), "", 0, "1", true, listGroupAbout, true));
    }

    public void logout() {
        Animation slideenter = AnimationUtils.loadAnimation(this, R.anim.enter);
        layoutLogout.setVisibility(View.VISIBLE);
        layoutLogout.setAnimation(slideenter);
    }

    public void gotoLogout() {
        // save loginted
        SharedPreferences.Editor editor = this.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putBoolean("isLogin", false);
        editor.apply();
        ShareReference.isLogin = false;
        Intent intent = new Intent(vn.mobiapps.pruf.view.base.TabbarActivity.this, LoginActivity.class);
        startActivity(intent);
        this.finish();
    }

    public ResideMenu getResideMenu() {
        return resideMenu;
    }

    @Override
    public void getMyProfile() {
        CheckNetworkRequest((BaseActivity) this, new BaseFragment.CheckNetworkListener() {
            @Override
            public void Connected() {
                SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                String username = prefs.getString("Username", "");
                presenter.getMyProfile(APIManager.deviceID, username);
            }
        });
    }

    @Override
    public void onSuccess(Object objectSuccess) {
        super.onSuccess(objectSuccess);
        try {
            if (objectSuccess != null) {
                modelMyProfile = (ModelMyProfile) objectSuccess;
                if (modelMyProfile != null && modelMyProfile.data != null && modelMyProfile.data.userProfile != null){
                    ResideMenu.setNameAccount(modelMyProfile.data.userProfile.firstName);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Object objectError) {
        super.onError(objectError);
    }

    public void goToMyProFile() {
        Intent intent = new Intent(this, MyProfileActivity.class);
        if (modelMyProfile.data != null && modelMyProfile.data.userProfile != null) {
            intent.putExtra("UserProfile", modelMyProfile.data.userProfile);
        }
        this.startActivity(intent);
        this.finish();
    }
}
