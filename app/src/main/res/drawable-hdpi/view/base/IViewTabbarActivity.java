package vn.mobiapps.pruf.view.base;

/**
 * Created by Truong Thien on 3/7/2019.
 */

public interface IViewTabbarActivity extends IBaseView{
    void getMyProfile();
}
