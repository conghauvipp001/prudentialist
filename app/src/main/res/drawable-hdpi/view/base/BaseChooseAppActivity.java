package vn.mobiapps.pruf.view.base;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.LayoutRes;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import vn.mobiapps.pruf.R;

public class BaseChooseAppActivity extends vn.mobiapps.pruf.view.base.BaseActivity {
    public static final String TAG = vn.mobiapps.pruf.view.base.BaseChooseAppActivity.class.getSimpleName();

    public LinearLayout layoutChoose, layoutOpenApp, layoutSupport;
    public TextView txtGmailApp, txtMessageApp, txtUndo_, txtTitle, txtQuestion;
    private FrameLayout layout_chooseApp;
    private ImageView imgQuestion, imgShowQuestion;
    public String textSupport = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_choose_app);
        pushPop.setIDContentReplace(R.id.layout_chooseApp);//Notes
        setupView();
        setOnClickListener();
    }

    public FrameLayout setContentChoose(@LayoutRes int layoutResID) {
        layout_chooseApp.removeAllViews();
        getLayoutInflater().inflate(layoutResID, layout_chooseApp, true);
        return layout_chooseApp;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        setImageDefault();
    }

    private void setupView() {
        layout_chooseApp = findViewById(R.id.layout_chooseApp);
        layoutChoose = findViewById(R.id.layoutChoose);
        layoutOpenApp = findViewById(R.id.layoutOpenApp);
        layoutSupport = findViewById(R.id.layoutSupport);
        txtGmailApp = findViewById(R.id.txtGmailApp);
        txtMessageApp = findViewById(R.id.txtMessageApp);
        txtUndo_ = findViewById(R.id.txtUndo_);
        txtTitle = findViewById(R.id.txtTitle);
        txtQuestion = findViewById(R.id.txtQuestion);
        imgQuestion = findViewById(R.id.imgQuestion);
        imgShowQuestion = findViewById(R.id.imgShowQuestion);
    }

    private void setOnClickListener() {
        layoutChoose.setOnClickListener(this);
        layoutOpenApp.setOnClickListener(this);
        txtUndo_.setOnClickListener(this);
        txtGmailApp.setOnClickListener(this);
        txtMessageApp.setOnClickListener(this);
        imgShowQuestion.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.txtUndo_:
            case R.id.layoutChoose:
                hidePupUpChooseApps();
                break;
            case R.id.imgShowQuestion:
                if (!textSupport.equals("")) {
                    showQuestion(textSupport);
                    textSupport = "";
                }
                break;
            default:
                break;
        }
    }

    public void showPopUpChooApps() {
        hideSupport();
        Animation slideenter = AnimationUtils.loadAnimation(this, R.anim.enter);
        layoutChoose.setVisibility(View.VISIBLE);
        layoutOpenApp.setVisibility(View.VISIBLE);
        layoutOpenApp.setAnimation(slideenter);
    }

    public void hidePupUpChooseApps() {
        layoutChoose.setVisibility(View.GONE);
    }

    public void showSupport() {
        layoutSupport.setVisibility(View.VISIBLE);
    }

    public void hideSupport() {
        layoutSupport.setVisibility(View.GONE);
    }

    public void showQuestion(String suggestions) {
        imgQuestion.setVisibility(View.GONE);
        txtQuestion.setVisibility(View.VISIBLE);
        txtQuestion.setText(suggestions);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                txtQuestion.setVisibility(View.GONE);
                showDialog(View.GONE);
            }
        }, 2000);
    }

    public void hideQuestion() {
        imgQuestion.setVisibility(View.VISIBLE);
        txtQuestion.setVisibility(View.GONE);
    }

    public void setContent(String content) {
        this.textSupport = content;
        hideQuestion();
    }

    public void setImageDefault () {
        imgQuestion.setImageDrawable(null);
        imgShowQuestion.setImageDrawable(null);
    }

    public void setMarginBottom (int marginBottom){
        ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) layoutSupport.getLayoutParams();
        p.setMargins(0, 0, 0, marginBottom);
        layoutSupport.requestLayout();
    }
}
