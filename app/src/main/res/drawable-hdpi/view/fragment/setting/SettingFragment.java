package vn.mobiapps.pruf.view.fragment.setting;

import android.view.View;

import vn.mobiapps.pruf.view.base.BaseFragment;

/**
 * Created by Truong Thien on 12/14/2018.
 */

public class SettingFragment extends BaseFragment {
    @Override
    protected int resLayout() {
        return 0;
    }

    @Override
    protected void createFragment() {

    }

    @Override
    protected void setupFragment(View view) {

    }

    @Override
    protected void setOnClickListenerFragment() {

    }
}
