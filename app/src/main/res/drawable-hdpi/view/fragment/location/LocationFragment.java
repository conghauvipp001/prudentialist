package vn.mobiapps.pruf.view.fragment.location;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.adapter.OfficeAdapter;
import vn.mobiapps.pruf.apimanager.APIManager;
import vn.mobiapps.pruf.model.model_data.location.ListOffice;
import vn.mobiapps.pruf.model.model_data.location.Offices;
import vn.mobiapps.pruf.presenter.location.ILocationPresenter;
import vn.mobiapps.pruf.presenter.location.LocationPresenterImpl;
import vn.mobiapps.pruf.view.activity.LocationActivity;
import vn.mobiapps.pruf.view.base.BaseFragment;
import vn.mobiapps.pruf.widgets.LocationResolver;
import vn.mobiapps.pruf.widgets.googlemap.DirectionsJSONParser;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by Truong Thien on 12/21/2018.
 */

public class LocationFragment extends BaseFragment implements LocationListener, ILocationView {
    private static final String TAG = vn.mobiapps.pruf.view.fragment.location.LocationFragment.class.getSimpleName();

    private static final int REQUEST_LOCATION = 9999;
    public GoogleMap map;
    private Location myLocation = null;
    public LocationResolver mLocationResolver;
    private ProgressDialog progressDialog;
    private ListView lvOffice;
    private OfficeAdapter adapter;
    private ArrayList<ListOffice> listOffice;
    private ImageView imgBack;
    private LinearLayout layout_South, layout_Northern, layout_Central;
    private TextView status_South, status_Northern, status_Central;
    private ILocationPresenter presenter;
    private String tab = "SOUTH";
    private Marker marker;

    @Override
    protected int resLayout() {
        return R.layout.fragment_location;
    }

    @Override
    protected void createFragment() {
        if (presenter == null) {
            presenter = new LocationPresenterImpl(this);
        }
    }

    @Override
    protected void setupFragment(View view) {
        lvOffice = view.findViewById(R.id.lvOffice);
        imgBack = view.findViewById(R.id.imgBack);
        layout_South = view.findViewById(R.id.layout_South);
        layout_Northern = view.findViewById(R.id.layout_Northern);
        layout_Central = view.findViewById(R.id.layout_Central);
        status_South = view.findViewById(R.id.status_South);
        status_Northern = view.findViewById(R.id.status_Northern);
        status_Central = view.findViewById(R.id.status_Central);
        loadGoogleMap();
        getListOffice(APIManager.mlanguage, tab);
    }

    @Override
    protected void setOnClickListenerFragment() {
        layout_South.setOnClickListener(this);
        layout_Northern.setOnClickListener(this);
        layout_Central.setOnClickListener(this);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LocationActivity) getActivity()).onBackPressed();
            }
        });

        lvOffice.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    if (listOffice.get(position).longitude != null && !listOffice.get(position).longitude.equals("") &&
                            listOffice.get(position).latitude != null && !listOffice.get(position).latitude.equals("")) {

                        LatLng destFrom = new LatLng(Double.parseDouble(listOffice.get(position).latitude), Double.parseDouble(listOffice.get(position).longitude));
                        if (myLocation != null) {
                            LatLng destTo = new LatLng(Double.parseDouble(myLocation.getLatitude() + ""), Double.parseDouble(myLocation.getLongitude() + ""));
                            showDiretion(destTo, destFrom, "", listOffice.get(position).address);
                        }
                        marker = map.addMarker(new MarkerOptions()
                                .position(destFrom)
                                .title(listOffice.get(position).name)
                                .snippet(listOffice.get(position).address));

                        selectedMarker(destFrom, marker);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.layout_South:
                tab = "SOUTH";
                showSouth();
                getListOffice("vi", tab);
                break;
            case R.id.layout_Northern:
                tab = "NORTHER";
                showNorthern();
                getListOffice("vi", tab);
                break;
            case R.id.layout_Central:
                tab = "CENTRAL";
                showCentral();
                getListOffice("vi", tab);
                break;
            default:
                break;
        }
    }

    private void showNorthern() {
        status_Northern.setVisibility(View.VISIBLE);
        status_Central.setVisibility(View.INVISIBLE);
        status_South.setVisibility(View.INVISIBLE);
    }

    private void showCentral() {
        status_Northern.setVisibility(View.INVISIBLE);
        status_Central.setVisibility(View.VISIBLE);
        status_South.setVisibility(View.INVISIBLE);
    }

    private void showSouth() {
        status_Northern.setVisibility(View.INVISIBLE);
        status_Central.setVisibility(View.INVISIBLE);
        status_South.setVisibility(View.VISIBLE);
    }

    //==============GOOGLE MAP================/
    private void loadGoogleMap() {
        try {
            SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapLocation);
            // Sét đặt sự kiện thời điểm GoogleMap đã sẵn sàng.
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    onMyMapReady(googleMap);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Kiểm tra khi nào map tải xong, khi nào tải xong thì nhảy vào đây
    private void onMyMapReady(GoogleMap googleMap) {
        // Show Dialog Progress
        if (googleMap != null) {
            map = googleMap;// Lấy đối tượng Google Map ra:
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle("Thông Báo");
            progressDialog.setMessage("Đang tải GoogleMap");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

            // Thiết lập sự kiện đã tải Map thành công
            map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    // Đã tải thành công thì tắt Dialog Progress đi
                    progressDialog.dismiss();
                    initPermission();
                }
            });

            if (!(Build.VERSION.SDK_INT > Build.VERSION_CODES.M)) {
                progressDialog.dismiss();
            }
        }
    }

    // Tìm một nhà cung cấp vị trị hiện thời đang được mở.
    private String getEnabledLocationProvider() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        // Tiêu chí để tìm một nhà cung cấp vị trí.
        Criteria criteria = new Criteria();
        // Tìm một nhà cung vị trí hiện thời tốt nhất theo tiêu chí trên.
        // ==> "gps", "network",...
        String bestProvider = locationManager.getBestProvider(criteria, true);
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isPassiveEnabled = locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean isEnabledBest = locationManager.isProviderEnabled(bestProvider);// 1 trong 3 trường hợp trên
        if (isGPSEnabled || isNetworkEnabled || isPassiveEnabled || isEnabledBest) {
            if (isPassiveEnabled) {
                return LocationManager.PASSIVE_PROVIDER; //Hiện tại only run case this
            }
        } else {
            Log.e(TAG, "No location provider enabled!");
            return null;
        }
        return bestProvider; //LocationManager.PASSIVE_PROVIDER - LocationManager.GPS_PROVIDER
    }

    //Hiển thị vị trí hiện thời trên bản đồ. - Chỉ gọi phương thức này khi đã có quyền xem vị trí người dùng.
    public void showMyLocation() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        /*boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);*/

        String locationProvider = this.getEnabledLocationProvider();
        if (locationProvider == null) {
            return;
        }
        // Millisecond
        final long MIN_TIME_BW_UPDATES = 1000;
        // Met
        final float MIN_DISTANCE_CHANGE_FOR_UPDATES = 1;

        try {
            // Đoạn code nay cần người dùng cho phép (Hỏi ở trên ***).
            locationManager.requestLocationUpdates(
                    locationProvider,
                    MIN_TIME_BW_UPDATES,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

            // Lấy ra vị trí.
            myLocation = locationManager.getLastKnownLocation(locationProvider);

            if (myLocation == null && mLocationResolver != null) { //API 22 --> lower
                mLocationResolver.resolveLocation(getActivity(), new LocationResolver.OnLocationResolved() {
                    @Override
                    public void onLocationResolved(Location location) {
                        myLocation = location;
                        displayMyLocation(location);
                    }
                });
            } else {
                displayMyLocation(myLocation);
            }

        } catch (SecurityException e) { // Với Android API >= 23 phải catch SecurityException.
            Toast.makeText(getActivity(), "Show My Location Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
            Log.e(TAG, "Show My Location Error:" + e.getMessage());
            e.printStackTrace();
            return;
        }
    }

    private void displayMyLocation(Location myLocation) {
        try {
            if (myLocation != null) {
                LatLng latLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                //map.clear();
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13));
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(latLng)             // Sets the center of the map to location user
                        .zoom(17)                   // Sets the zoom
                        .bearing(90)                // Sets the orientation of the camera to east
                        .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                        .build();                   // Creates a CameraPosition from the builder
                map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                // Thêm Marker cho Map:
                MarkerOptions option = new MarkerOptions();
                //option.title("Vị trí hiện tại");
                //option.snippet("....");
                //option.icon(BitmapDescriptorFactory.fromResource(R.drawable.location_red));
                option.position(latLng);
                Marker currentMarker = map.addMarker(option);
                currentMarker.showInfoWindow();
            } else {
                //Toast.makeText(getActivity(), "Vui lòng bật GPS !", Toast.LENGTH_LONG).show();
                getListOffice(APIManager.mlanguage, tab);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void selectedMarker(LatLng location, Marker marker) {
        if (location != null && marker != null) {
            /*int height = 80;
            int width = 80;
            BitmapDrawable bitmapdraw = (BitmapDrawable) getActivity().getResources().getDrawable(R.drawable.icon_map);
            Bitmap b = bitmapdraw.getBitmap();
            Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);*/
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 13));
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(location)
                    .zoom(15)
                    .bearing(90)
                    .tilt(40)
                    .build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            marker.showInfoWindow();
            //marker.setIcon(BitmapDescriptorFactory.fromBitmap(smallMarker));
        }
    }

    //=============================Cấp quyền GoogleMap===========================


    //Yêu cầu cấp quyền
    public void initPermission() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //--------------Location - Bluetoooth
                if (getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                        getActivity().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    //Permisson don't granted
                    if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) &&
                            shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        Log.e(TAG, "Permission isn't granted");
                    }
                    // Permisson don't granted and dont show dialog again.
                    else {
                        Log.e(TAG, "Permisson don't granted and dont show dialog again");
                    }
                    requestPermissions(new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    }, REQUEST_LOCATION);
                } else {
                    // Hiển thị vị trí hiện thời trên bản đồ khi đã cấp quyền
                    if (map != null) {
                        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                        //map.getUiSettings().setZoomControlsEnabled(true);
                        map.setMyLocationEnabled(true);
                    }
                    showMyLocation();
                }
            } else {
                LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    Toast.makeText(getActivity(), "Vui lòng bật GPS !", Toast.LENGTH_SHORT).show();
                } else {
                    // Hiển thị vị trí hiện thời trên bản đồ.
                    if (map != null) {
                        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                        //map.getUiSettings().setZoomControlsEnabled(true);
                        map.setMyLocationEnabled(true);
                    }
                    showMyLocation();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("My Current loction", strReturnedAddress.toString());
            } else {
                Log.w("My Current loction", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current loction", "Canont get Address!");
        }
        return strAdd.trim();
    }

    // Khi người dùng trả lời yêu cầu cấp quyền (cho phép hoặc từ chối).
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        try {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            switch (requestCode) {
                case REQUEST_LOCATION: {
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        if ((ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) &&
                                (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
                            Log.e(TAG, "Permision is Granted");

                            // Hiển thị vị trí hiện thời trên bản đồ.
                            if (map != null) {
                                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                                //map.getUiSettings().setZoomControlsEnabled(true);
                                map.setMyLocationEnabled(true);
                            }
                            showMyLocation();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Bạn không cấp quyền! Một số tính năng có thể không hoạt động!", Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
                default: {
                    mLocationResolver.onRequestPermissionsResult(requestCode, permissions, grantResults);
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    //=== DIRECTION===
    public void showDiretion(LatLng destTo, LatLng destFrom, String title, String snippet) {
        if (destTo != null && destFrom != null) {
            map.clear();
            // Getting URL to the Google Directions API
            String url = getDirectionsUrl(destTo, destFrom);
            //Log.e("Url for Api Diretion: ", url);
            DownloadTask downloadTask = new DownloadTask();
            // Start downloading json data from Google Directions API
            downloadTask.execute(url);
        } else {
            Toast.makeText(getActivity(), "False Points", Toast.LENGTH_SHORT).show();
        }
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Sensor enabled
        String sensor = "sensor=false";
        //Key
        String key = "&key=" + getResources().getString(R.string.api_direction_key);
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + key;
        return url;
    }

    //A method to download json data from url
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            //Log.e(TAG, "Exception while downloading url : " + e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {
        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {
            // For storing data from web service
            String data = "";
            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    //A class to parse the Google Places in JSON format
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();
                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = "";
            String duration = "";

            if (result.size() < 1) {
                //Toast.makeText(activity, "No Points", Toast.LENGTH_SHORT).show();
                return;
            }

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    if (j == 0) {    // Get distance from the list
                        distance = (String) point.get("distance");
                        continue;
                    } else if (j == 1) { // Get duration from the list
                        duration = (String) point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(15);
                lineOptions.color(Color.RED);
            }

            //Log.e(TAG, "--->" + "Distance:" + distance + ", Duration:" + duration);
            // Drawing polyline in the Google Map for the i-th route
            map.addPolyline(lineOptions);
        }
    }
    //=== DIRECTION=== ./

    @Override
    public void getListOffice(final String language, final String region) {
        CheckNetworkRequest((vn.mobiapps.pruf.view.base.BaseActivity) getActivity(), new CheckNetworkListener() {
            @Override
            public void Connected() {
                presenter.getListOffice(language, region);
            }
        });
    }

    @Override
    public void onSuccess(Object objectSuccess) {
        try {
            if (objectSuccess != null) {
                Offices offices = (Offices) objectSuccess;
                if (offices.data.listOffice != null) {
                    if (listOffice != null) {
                        listOffice.removeAll(listOffice);
                    }
                    listOffice = new ArrayList<ListOffice>();
                    for (int i = 0; i < offices.data.listOffice.length; i++) {
                        listOffice.add(new ListOffice(offices.data.listOffice[i].id, offices.data.listOffice[i].longitude, offices.data.listOffice[i].latitude, offices.data.listOffice[i].name, offices.data.listOffice[i].address, offices.data.listOffice[i].hotline, offices.data.listOffice[i].email));
                        // Thêm Marker cho Map:
                        if (offices.data.listOffice[i].latitude != null && offices.data.listOffice[i].longitude != null) {
                            LatLng latLng = new LatLng(Double.parseDouble(offices.data.listOffice[i].latitude), Double.parseDouble(offices.data.listOffice[i].longitude));
                            MarkerOptions option = new MarkerOptions();
                            option.title(offices.data.listOffice[i].name);
                            // option.snippet(offices.data.listOffices[i].address);
                            //option.icon(BitmapDescriptorFactory.fromResource(R.drawable.location_red));
                            option.position(latLng);
                            Marker currentMarker = map.addMarker(option);
                            currentMarker.showInfoWindow();
                        }
                    }
                    LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                    if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        LatLng latLng = new LatLng(Double.parseDouble(offices.data.listOffice[0].latitude), Double.parseDouble(offices.data.listOffice[0].longitude));
                        Marker marker1 = map.addMarker(new MarkerOptions()
                                .position(latLng)
                                .title(offices.data.listOffice[0].name)
                                .snippet(offices.data.listOffice[0].address));
                        selectedMarker(latLng, marker1);
                    }
                    adapter = new OfficeAdapter(getActivity(), R.layout.item_office, listOffice);
                    lvOffice.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Object objectError) {
        super.onError(objectError);
    }
}
