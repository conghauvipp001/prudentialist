package vn.mobiapps.pruf.view.fragment.location;

/**
 * Created by Truong Thien on 1/28/2019.
 */

public interface ILocationView extends vn.mobiapps.pruf.view.base.IBaseView {
    void getListOffice(String language, String region);
}
