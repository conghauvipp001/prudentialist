package vn.mobiapps.pruf.view.fragment.myprofile;

import android.graphics.Bitmap;

import vn.mobiapps.pruf.view.base.IBaseView;

/**
 * Created by Truong Thien on 3/7/2019.
 */

public interface IViewMyProfile extends IBaseView {
    void uploadAvarta(Bitmap bitmap);
    void onSuccess();
}
