package vn.mobiapps.pruf.view.fragment.myprofile;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.model.model_data.myprofile.UserProfile;
import vn.mobiapps.pruf.presenter.myprofile.IMyProfilePresenter;
import vn.mobiapps.pruf.presenter.myprofile.MyProfilePresenterImpl;
import vn.mobiapps.pruf.utils.ShareReference;
import vn.mobiapps.pruf.utils.Utils;
import vn.mobiapps.pruf.view.activity.ChangePassActivity;
import vn.mobiapps.pruf.view.activity.ChangePinActivity;
import vn.mobiapps.pruf.view.activity.MyProfileActivity;
import vn.mobiapps.pruf.view.base.BaseActivity;
import vn.mobiapps.pruf.view.base.BaseFragment;
import vn.mobiapps.pruf.widgets.CaptureImage;

/**
 * Created by Truong Thien on 3/6/2019.
 */

public class MyProfileFragment extends BaseFragment implements IViewMyProfile, CaptureImage.onCaptureImage{

    private ImageView imgdefault, imgBack;
    private Bitmap bitmap;
    private CaptureImage image;
    private IMyProfilePresenter presenter;
    private MyAsyncTask mytt;
    private TextView txtFullName, txtChangePass, txtChangePin, txtChangeEmail, txtChangePhone, txtChangeAddress, txtChangeNationalId;
    private EditText edtName, edtPass, edtEmail, edtPhone, edtPin, edtAddress, edtIdentityCard;

    @Override
    protected int resLayout() {
        return R.layout.fragment_my_profile;
    }

    @Override
    protected void createFragment() {
        if (presenter == null) {
            presenter = new MyProfilePresenterImpl(this);
        }
        if (image == null) {
            image = new CaptureImage(getActivity(), this, this);
        }
        image.initPermission();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mytt != null) {
            mytt.cancel(true);
        }
    }

    @Override
    protected void setupFragment(View view) {
        imgdefault = view.findViewById(R.id.imgdefault);
        imgBack = view.findViewById(R.id.imgBack);
        txtFullName = view.findViewById(R.id.txtFullName);
        txtChangePass = view.findViewById(R.id.txtChangePass);
        txtChangePin = view.findViewById(R.id.txtChangePin);
        txtChangeEmail = view.findViewById(R.id.txtChangeEmail);
        txtChangePhone = view.findViewById(R.id.txtChangePhone);
        txtChangeAddress = view.findViewById(R.id.txtChangeAddress);
        txtChangeNationalId = view.findViewById(R.id.txtChangeNationalId);
        edtName = view.findViewById(R.id.edtName);
        edtPass = view.findViewById(R.id.edtPass);
        edtEmail = view.findViewById(R.id.edtEmail);
        edtPhone = view.findViewById(R.id.edtPhone);
        edtPin = view.findViewById(R.id.edtPin);
        edtAddress = view.findViewById(R.id.edtAddress);
        edtIdentityCard = view.findViewById(R.id.edtIdentityCard);
        updateUI(((MyProfileActivity)getActivity()).userProfile);
    }

    @Override
    protected void setOnClickListenerFragment() {
        imgdefault.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        txtChangePass.setOnClickListener(this);
        txtChangePin.setOnClickListener(this);
        txtChangeEmail.setOnClickListener(this);
        txtChangePhone.setOnClickListener(this);
        txtChangeAddress.setOnClickListener(this);
        txtChangeNationalId.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.imgBack:
                ((MyProfileActivity)getActivity()).onBackPressed();
                break;
            case R.id.imgdefault:
                selectImage();
                break;
            case R.id.txtChangePass: {
                Intent intent = new Intent(getActivity(), ChangePassActivity.class);
                startActivity(intent);
            }
                break;
            case R.id.txtChangePin: {
                Intent intent = new Intent(getActivity(), ChangePinActivity.class);
                startActivity(intent);
            }
                break;
            case R.id.txtChangeEmail:
                edtEmail.setFocusableInTouchMode(true);
                edtEmail.requestFocus();
                break;
            case R.id.txtChangePhone:
                edtPhone.setFocusableInTouchMode(true);
                edtPhone.requestFocus();
                break;
            case R.id.txtChangeAddress:
                edtAddress.setFocusableInTouchMode(true);
                edtAddress.requestFocus();
                break;
            case R.id.txtChangeNationalId:
                edtIdentityCard.setFocusableInTouchMode(true);
                edtIdentityCard.requestFocus();
                break;
            default:
                break;
        }
    }

    public void updateUI(UserProfile userProfile) {
        try {
            if (userProfile != null) {
                if (userProfile.fullName != null) {
                    txtFullName.setText(userProfile.fullName);
                }
                if (userProfile.fullName != null) {
                    edtName.setText(userProfile.fullName);
                }
                edtPass.setText("********");

                if (userProfile.email != null) {
                    edtEmail.setText(hideEmail(userProfile.email));
                }

                if (userProfile.phoneNumber != null) {
                    edtPhone.setText(hidePhone(userProfile.phoneNumber));
                }

                edtPin.setText("******");

                if (userProfile.nationalId != null) {
                    edtIdentityCard.setText(hideNationalId(userProfile.nationalId));
                }

                if (userProfile.address != null) {
                    edtAddress.setText(userProfile.address);
                }

                if (userProfile.avatar != null && !userProfile.avatar.equals("")) {
                    Glide.with(getActivity())
                            .load(userProfile.avatar)
                            .asBitmap()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    ShareReference.bitmap =  resource;
                                    imgdefault.setVisibility(View.VISIBLE);
                                    imgdefault.setImageBitmap(resource);
                                }
                                @Override
                                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                    super.onLoadFailed(e, errorDrawable);
                                    imgdefault.setVisibility(View.VISIBLE);
                                }
                            });
                } else {
                    if (bitmap == null) {
                        mytt = new MyAsyncTask();
                        mytt.execute();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void uploadAvarta(final Bitmap bitmap) {
        CheckNetworkRequest((BaseActivity) getActivity(), new CheckNetworkListener() {
            @Override
            public void Connected() {
                presenter.uploadAvarta(bitmap);
            }
        });
    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void onError(Object objectError) {
        super.onError(objectError);
    }

    // ====================================== choose img avarta ========================================//
    /* Hàm chọn hình từ thư viện hoặc camera */
    public void selectImage() {
        try {
            final CharSequence[] items = {getActivity().getString(R.string.TakePhoto), getActivity().getString(R.string.ChoosefromLibrary), getActivity().getString(R.string.Cancel)};
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.AddPhoto);
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    boolean result = Utils.checkPermission(getActivity());
                    if (items[item].equals(getActivity().getString(R.string.TakePhoto))) {
                        if (result)
                            image.cameraIntent();
                    } else if (items[item].equals(getActivity().getString(R.string.ChoosefromLibrary))) {
                        if (result)
                            image.galleryIntent();
                    } else if (items[item].equals(getActivity().getString(R.string.Cancel))) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        image.onActivityResult(requestCode, resultCode, data);
    }

    // Khi người dùng trả lời yêu cầu cấp quyền (cho phép hoặc từ chối).
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        image.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onCaptureImage_(Bitmap bitmap) {

    }

    @Override
    public void onCaptureImageFile_(Bitmap bitmap) {
        setBitmap(bitmap);
    }

    @Override
    public void onSelectFromGallery_(Bitmap bitmap) {
        setBitmap(bitmap);
    }

    //===================================== Save Catch ===========================
    public void saveImageCatch(Bitmap img, Context context) {
        try {
            File testFile = new File(context.getCacheDir().getPath(), "TestFile.jpg");
            if (!testFile.exists())
                testFile.createNewFile();

            FileOutputStream fos = new FileOutputStream(testFile);
            img.compress(Bitmap.CompressFormat.JPEG, 90, fos);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            //Log.e("ReadWriteFile", "Unable to write to the File\n" + e.getMessage());
        }
    }

    public Bitmap readImageCatch(Context context) {
        Bitmap value = null;
        try {
            File testFile = new File(context.getCacheDir().getPath(), "TestFile.jpg");
            if (testFile != null) {
                try {
                    FileInputStream fis = new FileInputStream(testFile);
                    Bitmap bm = BitmapFactory.decodeStream(fis);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 90, baos);
                    baos.flush();
                    return bm;
                } catch (Exception e) {
                    //Log.e("ReadWriteFile", "Unable to read the File\n" + e.getMessage());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    private void setBitmap (Bitmap bitmap) {
        if (bitmap != null) {
            this.bitmap = bitmap;
            saveImageCatch(bitmap, getActivity());
            imgdefault.setVisibility(View.VISIBLE);
            imgdefault.setImageBitmap(bitmap);
            uploadAvarta(bitmap);
        }
    }

    public class MyAsyncTask extends AsyncTask<Void, Void, Void> { //AsyncTask<Params, Progress, Result>
        public MyAsyncTask() {
        }

        //Begin
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        //Run
        @Override
        protected Void doInBackground(Void... params) {
            if (bitmap == null) {
                bitmap = readImageCatch(getActivity());
            }
            return null;
        }

        //Update UI
        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        //End
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ShareReference.bitmap = bitmap;
            if (ShareReference.bitmap != null) {
                imgdefault.setVisibility(View.VISIBLE);
                imgdefault.setImageBitmap(ShareReference.bitmap);
            } else {
                imgdefault.setVisibility(View.GONE);
            }
        }
    }

    private String hideEmail(String email) {
        String mail = "";
        if (email != null && !email.equals("") && email.length() >= 6) {
            mail = email.substring(0, 4) + "************" + email.substring(email.length() - 2, email.length());
        } else {
            mail = email;
        }
        return mail;
    }

    private String hidePhone(String phone) {
        String phone_ = "";
        if (phone != null && !phone.equals("") && phone.length() >= 8) {
            phone_ = phone.substring(0, 3) + "***" + phone.substring(4, 6) + "***" + phone.substring(phone.length() - 3, phone.length());
        } else {
            phone_ = phone;
        }
        return phone_;
    }

    private String hideNationalId(String nationalId) {
        String national = "";
        if (nationalId != null && !nationalId.equals("") && nationalId.length() >= 5) {
            national = "***" + nationalId.substring(3, 5) + "***" + nationalId.substring(nationalId.length() - 3, nationalId.length());
        } else {
            national = nationalId;
        }
        return national;
    }
}
