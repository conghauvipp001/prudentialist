package vn.mobiapps.pruf.view.fragment.regiterloan;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.model.model_data.regiterloan.ResponseCheckRegisterCondition;
import vn.mobiapps.pruf.model.model_view.ChatBoxModel;
import vn.mobiapps.pruf.presenter.regiterloan.checkstatusaccount.CheckStatusAccountPresenterImpl;
import vn.mobiapps.pruf.presenter.regiterloan.checkstatusaccount.ICheckStatusAccountPresenter;
import vn.mobiapps.pruf.utils.Constant;
import vn.mobiapps.pruf.utils.Utils;
import vn.mobiapps.pruf.view.base.BaseFragment;
import vn.mobiapps.pruf.widgets.ListViewChatBox;

/**
 * Created by Truong Thien on 12/26/2018.
 */

public class InputValuesFragment extends BaseFragment implements vn.mobiapps.pruf.view.fragment.regiterloan.ICheckStatusView {

    private ImageView imgSend, imgBack;
    private EditText edtValues;
    private LinearLayout layoutInput;
    private TextView txtNext;
    private ListViewChatBox lvChatBox;
    private ArrayList<ChatBoxModel> listChat;
    private ICheckStatusAccountPresenter presenter;
    private int index = 0;

    @Override
    protected int resLayout() {
        return R.layout.fragment_input_values;
    }

    @Override
    protected void createFragment() {
        if (presenter == null) {
            presenter = new CheckStatusAccountPresenterImpl(this);
        }
    }

    @Override
    protected void setupFragment(View view) {
        imgSend = view.findViewById(R.id.imgSend);
        imgBack = view.findViewById(R.id.imgBack);
        edtValues = view.findViewById(R.id.edtValues);
        layoutInput = view.findViewById(R.id.layoutInput);
        lvChatBox = view.findViewById(R.id.lvChatBox);
        txtNext = view.findViewById(R.id.txtNext);
        addListChat();
    }

    @Override
    protected void setOnClickListenerFragment() {
        imgBack.setOnClickListener(this);
        imgSend.setOnClickListener(this);
        edtValues.setOnClickListener(this);
        txtNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.txtNext: {
                checkRegisterCondition();
            }
            break;
            case R.id.imgBack:
                hideKeyboard();
                ((vn.mobiapps.pruf.view.activity.RegisterLoanActivity) getActivity()).onBackPressed();
                break;
            case R.id.imgSend:
                clickAdd();
                break;
            case R.id.edtValues: {
                if (listChat.get(index - 1).type.equals("Birthday")) {
                    DatePicker();
                }
            }
            break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((vn.mobiapps.pruf.view.activity.RegisterLoanActivity)getActivity()).showSupport();
    }

    private void addListChat() {
        try {
            listChat = new ArrayList<ChatBoxModel>();
            lvChatBox.setData(getActivity(), listChat, this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    listChat.add(new ChatBoxModel(getString(R.string.txt_enter_CMND), "", false, "CMND", false));
                    index = 1;
                    ((vn.mobiapps.pruf.view.activity.RegisterLoanActivity)getActivity()).setContent(getString(R.string.txt_enter_cmnd));
                    lvChatBox.adapter.currentPositionQuestion = 0;
                    lvChatBox.updateAdapter();
                }
            }, 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void clickAdd() {
        try {
            hideKeyboard();
            if (checkValidate(edtValues)) {
                if (listChat.get(index - 1).type.equals("CMND")) {
                    if (listChat.size() > 1) {
                        listChat.remove(1);
                    }
                    if (checkCMND(edtValues.getText().toString())) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                listChat.add(1, new ChatBoxModel(edtValues.getText().toString(), "", true, Constant.TEXTVIEW, false));
                                lvChatBox.adapter.currentPositionQuestion = 1;
                                lvChatBox.updateAdapter();
                                lvChatBox.adapter.currentPositionQuestion = -1;
                                edtValues.setText("");
                            }
                        }, 1000);

                        if (listChat.size() == 2) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    listChat.add(2, new ChatBoxModel(getString(R.string.txt_enter_birthday), "", false, "Birthday", false));
                                    ((vn.mobiapps.pruf.view.activity.RegisterLoanActivity)getActivity()).setContent(getString(R.string.txt_endter_birthday));
                                    lvChatBox.adapter.currentPositionQuestion = 2;
                                    lvChatBox.updateAdapter();
                                }
                            }, 2000);

                        }
                        edtValues.setFocusable(false);
                        index = 3;
                    } else {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                listChat.add(1, new ChatBoxModel(edtValues.getText().toString(), "Số chứng minh nhân dân không hợp lệ", true, Constant.TEXTVIEW, false));
                                lvChatBox.adapter.currentPositionQuestion = 1;
                                lvChatBox.updateAdapter();
                            }
                        }, 1000);
                    }
                } else if (listChat.get(index - 1).type.equalsIgnoreCase("Birthday")) {
                    if (listChat.size() > 3) {
                        listChat.remove(3);
                    }
                    if (Utils.countYear(edtValues.getText().toString())) {
                        listChat.add(3, new ChatBoxModel(edtValues.getText().toString(), "", true, Constant.TEXTVIEW, false));
                        index = 4;
                        layoutInput.setVisibility(View.GONE);
                        txtNext.setVisibility(View.VISIBLE);
                    } else {
                        listChat.add(3, new ChatBoxModel(edtValues.getText().toString(), getString(R.string.txt_old), true, Constant.TEXTVIEW, false));
                    }
                }

                //lvChatBox.updateAdapter();
                lvChatBox.setSelection(index - 1);
                if (listChat.size() == 4) {
                    ((vn.mobiapps.pruf.view.activity.RegisterLoanActivity)getActivity()).setContent(getString(R.string.txt_conti));
                    layoutInput.setVisibility(View.GONE);
                    txtNext.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void DatePicker() {
        try {
            final Calendar calendar = Calendar.getInstance();
            int day, month, year;
            day = calendar.get(Calendar.DAY_OF_MONTH);
            month = calendar.get(Calendar.MONTH);
            year = calendar.get(Calendar.YEAR);
            final DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    calendar.set(year, month, dayOfMonth);
                    SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
                    edtValues.setText(sdf1.format(calendar.getTime()));
                }
            }, year, month, day);
            //calendar.set(year - 18, 11, 31);
            datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
            datePickerDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*Call api checkRegisterCondition*/
    @Override
    public void checkRegisterCondition() {
        CheckNetworkRequest((vn.mobiapps.pruf.view.base.BaseActivity) getActivity(), new CheckNetworkListener() {
            @Override
            public void Connected() {
                //presenter.checkRegisterCondition("20/10/1990", "215361444");
                presenter.checkRegisterCondition(listChat.get(4).content, listChat.get(2).content, "IS_REGISTER");
            }
        });
    }

    @Override
    public void onSuccess(Object objectSuccess) {
        super.onSuccess(objectSuccess);
        try {
            if (objectSuccess != null) {
                ResponseCheckRegisterCondition reponse = (ResponseCheckRegisterCondition) objectSuccess;
                if (reponse.result_code.equals("0")) {
                    if (reponse.data != null) {
                        ((vn.mobiapps.pruf.view.activity.RegisterLoanActivity) getActivity()).cuid = reponse.data.cuid;
                    }
                    vn.mobiapps.pruf.view.fragment.regiterloan.CheckStatusAccountFragment f = new vn.mobiapps.pruf.view.fragment.regiterloan.CheckStatusAccountFragment();
                    Bundle args = new Bundle();
                    args.putSerializable("Response", reponse);
                    f.setArguments(args);
                    ((vn.mobiapps.pruf.view.activity.RegisterLoanActivity) getActivity()).pushFragment(f);
                } else {
                    showMessage(reponse.result_description);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Object objectError) {
        super.onError(objectError);
    }

    private Boolean checkValidate(EditText edtValues) {
        if (edtValues == null || edtValues.getText().toString().equals("")) {
            showMessage(getString(R.string.txt_not_null));
            return false;
        }
        return true;
    }

    public void onClickEdit(int posision) {
        try {
            index = posision;
            if (listChat.get(posision - 1).type.equalsIgnoreCase("CMND")) {
                ((vn.mobiapps.pruf.view.activity.RegisterLoanActivity)getActivity()).setContent(getString(R.string.txt_enter_cmnd));
                edtValues.setText(listChat.get(posision).content);
                index = posision;
                edtValues.setFocusableInTouchMode(true);
            } else if (listChat.get(posision - 1).type.equalsIgnoreCase("Birthday")) {
                ((vn.mobiapps.pruf.view.activity.RegisterLoanActivity)getActivity()).setContent(getString(R.string.txt_endter_birthday));
                DatePicker();
                edtValues.setFocusable(false);
                index = posision;
            }
            layoutInput.setVisibility(View.VISIBLE);
            txtNext.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Boolean checkCMND(String cmnd) {
        if (cmnd != null && !cmnd.equals("")) {
            if (cmnd.length() == 9 || cmnd.length() == 12) {
                return true;
            }
        }
        return false;
    }
}
