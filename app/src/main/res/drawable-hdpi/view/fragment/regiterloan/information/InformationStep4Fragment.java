package vn.mobiapps.pruf.view.fragment.regiterloan.information;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.view.base.BaseFragment;

/**
 * Created by Truong Thien on 1/11/2019.
 */

public class InformationStep4Fragment extends BaseFragment{
    private TextView txtGotoLogin;

    @Override
    protected int resLayout() {
        return R.layout.fragment_information_step4;
    }

    @Override
    protected void createFragment() {

    }

    @Override
    protected void setupFragment(View view) {
        txtGotoLogin = view.findViewById(R.id.txtGotoLogin);
    }

    @Override
    protected void setOnClickListenerFragment() {
        txtGotoLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.txtGotoLogin:
                Intent intent = new Intent(getActivity(), vn.mobiapps.pruf.view.activity.LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            break;
            default:
                break;
        }
    }
}


