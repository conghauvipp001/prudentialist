package vn.mobiapps.pruf.view.fragment.regiterloan;

import android.content.Intent;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.view.activity.LoginActivity;
import vn.mobiapps.pruf.view.activity.RegisterLoanActivity;
import vn.mobiapps.pruf.view.base.BaseFragment;

/**
 * Created by Truong Thien on 3/1/2019.
 */

public class DoneCreateAccount extends BaseFragment {

    private CountDownTimer cTimer = null;
    private ProgressBar mprogressBar;
    private int minuti = 1;
    private TextView txtSetTime, txtRequest;

    @Override
    protected int resLayout() {
        return R.layout.fragment_done_create_account;
    }

    @Override
    protected void createFragment() {

    }

    @Override
    public void onResume() {
        super.onResume();
        ((RegisterLoanActivity)getActivity()).hideSupport();
    }

    @Override
    protected void setupFragment(View view) {
        txtSetTime = view.findViewById(R.id.txtSetTime);
        txtRequest = view.findViewById(R.id.txtRequest);
        mprogressBar = view.findViewById(R.id.mprogressBar);
        setCountDownTimer();
        txtRequest.setText(R.string.txt_create_done);
    }

    @Override
    protected void setOnClickListenerFragment() {

    }

    public void setCountDownTimer() {
        try {
            cTimer = new CountDownTimer(5 * 1000, 500) {
                @Override
                public void onTick(long leftTimeInMilliseconds) {
                    long seconds = leftTimeInMilliseconds / 1000;
                    mprogressBar.setProgress((int) seconds);
                    txtSetTime.setText(/*String.format("%02d", seconds / 60) + ":" +*/ String.format("%2d", seconds % 60) + "s");
                }
                @Override
                public void onFinish() {
                    txtSetTime.setText("0s");
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancelTimer() {
        if (cTimer != null)
            cTimer.cancel();
    }
}
