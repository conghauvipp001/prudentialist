package vn.mobiapps.pruf.view.fragment.regiterloan.information;

import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.model.model_view.RegisterModel;
import vn.mobiapps.pruf.utils.Utils;
import vn.mobiapps.pruf.view.activity.RegisterLoanActivity;
import vn.mobiapps.pruf.view.base.BaseFragment;

/**
 * Created by Truong Thien on 1/10/2019.
 */

public class InformationStep3Fragment extends BaseFragment {

    private TextView txt_text, txtSubmitLoan, txtEmail, txtPhoneNumber, txtAddress, txtMonthlyIncome, txtLoanAmount;
    private RegisterModel registerModel;
    private ImageView imgCardFront, imgCardBehind, imgIncomePapers;

    @Override
    protected int resLayout() {
        return R.layout.fragment_information_step3;
    }

    @Override
    protected void createFragment() {
        registerModel = ((RegisterLoanActivity)getActivity()).getRegisterModel();
    }

    @Override
    protected void setupFragment(View view) {
        txt_text = view.findViewById(R.id.txt_text);
        txtSubmitLoan = view.findViewById(R.id.txtSubmitLoan);
        txtEmail = view.findViewById(R.id.txtEmail);
        txtPhoneNumber = view.findViewById(R.id.txtPhoneNumber);
        txtAddress = view.findViewById(R.id.txtAddress);
        txtMonthlyIncome = view.findViewById(R.id.txtMonthlyIncome);
        txtLoanAmount = view.findViewById(R.id.txtLoanAmount);
        imgCardFront = view.findViewById(R.id.imgCardFront);
        imgCardBehind = view.findViewById(R.id.imgCardBehind);
        imgIncomePapers = view.findViewById(R.id.imgIncomePapers);

        txt_text.setText(Html.fromHtml(getResources().getString(R.string.txt_text)));
    }

    @Override
    protected void setOnClickListenerFragment() {
        txtSubmitLoan.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.txtSubmitLoan:
                InformationStep4Fragment f = new InformationStep4Fragment();
                ((RegisterLoanActivity)getActivity()).pushFragment(f);
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        settValues(registerModel);
    }


    private void settValues(RegisterModel registerModel) {
        try {
            if (registerModel.Email != null) {
                txtEmail.setText(registerModel.Email);
            }
            if (registerModel.PhoneNumber != null) {
                txtPhoneNumber.setText(registerModel.PhoneNumber);
            }
            if (registerModel.Address != null) {
                txtAddress.setText(registerModel.Address);
            }
            if (registerModel.MonthlyIncome != null) {
                txtMonthlyIncome.setText(Utils.formatMoney(registerModel.MonthlyIncome) + " VND");
            }
            if (registerModel.LoanAmount != null) {
                txtLoanAmount.setText(Utils.formatMoney(registerModel.LoanAmount) + " VND");
            }
            if (registerModel.IdentityCardFront != null) {
                imgCardFront.setImageBitmap(registerModel.IdentityCardFront);
            }
            if (registerModel.IdentityCardBehind != null) {
                imgCardBehind.setImageBitmap(registerModel.IdentityCardBehind);
            }
            if (registerModel.IncomePapers != null) {
                imgIncomePapers.setImageBitmap(registerModel.IncomePapers);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
