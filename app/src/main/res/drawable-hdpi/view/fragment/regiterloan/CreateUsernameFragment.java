package vn.mobiapps.pruf.view.fragment.regiterloan;

import android.os.Bundle;
import android.provider.Settings;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.adapter.ChatBoxAdapter;
import vn.mobiapps.pruf.model.model_data.BaseRespone;
import vn.mobiapps.pruf.model.model_data.regiterloan.SignUpRequest;
import vn.mobiapps.pruf.model.model_view.ChatBoxModel;
import vn.mobiapps.pruf.presenter.regiterloan.createusername.CreateUsernamePresenterImpl;
import vn.mobiapps.pruf.presenter.regiterloan.createusername.ICreateUsernamePresenter;
import vn.mobiapps.pruf.utils.Constant;
import vn.mobiapps.pruf.utils.Utils;
import vn.mobiapps.pruf.view.activity.RegisterLoanActivity;
import vn.mobiapps.pruf.view.base.BaseActivity;
import vn.mobiapps.pruf.view.base.BaseFragment;

/**
 * Created by Truong Thien on 12/26/2018.
 */

public class CreateUsernameFragment extends BaseFragment implements vn.mobiapps.pruf.view.fragment.regiterloan.ICreateUsernameView {

    private TextView txtDone;
    private LinearLayout layoutInput;
    private EditText edtValues;
    private ImageView imgSend, imgBack;
    private ListView lvChatBox;
    private ChatBoxAdapter adapter;
    private ArrayList<ChatBoxModel> listChat;
    private ICreateUsernamePresenter presenter;
    private int index = 0;

    @Override
    protected int resLayout() {
        return R.layout.fragment_create_username;
    }

    @Override
    protected void createFragment() {
        if (presenter == null) {
            presenter = new CreateUsernamePresenterImpl(this);
        }
    }

    @Override
    protected void setupFragment(View view) {
        txtDone = view.findViewById(R.id.txtDone);
        layoutInput = view.findViewById(R.id.layoutInput);
        edtValues = view.findViewById(R.id.edtValues);
        imgSend = view.findViewById(R.id.imgSend);
        imgBack = view.findViewById(R.id.imgBack);
        lvChatBox = view.findViewById(R.id.lvChatBox);
        addListChat();
    }

    @Override
    protected void setOnClickListenerFragment() {
        txtDone.setOnClickListener(this);
        imgSend.setOnClickListener(this);
        imgBack.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((RegisterLoanActivity)getActivity()).showSupport();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.imgBack:
                ((RegisterLoanActivity) getActivity()).popFragment();
                break;
            case R.id.txtDone:
                if (checkVeri()) {
                    vn.mobiapps.pruf.view.fragment.regiterloan.ConfirmAccountFragment f = new vn.mobiapps.pruf.view.fragment.regiterloan.ConfirmAccountFragment();
                    Bundle args = new Bundle();
                    args.putSerializable("Username", listChat.get(1).content);
                    args.putSerializable("PassWord", listChat.get(3).content);
                    f.setArguments(args);
                    ((RegisterLoanActivity) getActivity()).pushFragment(f);
                }

                break;
            case R.id.imgSend:
                clickSend();
                break;
            default:
                break;
        }
    }

    private void clickSend() {
        try {
            if (checkValidate()) {
                if (listChat.get(index - 1).type.equalsIgnoreCase("Username")) {
                    hideKeyboard();
                    checkUsernameAvailability(edtValues.getText().toString());
                } else if (listChat.get(index - 1).type.equalsIgnoreCase("NewPass")) {
                    if (listChat.size() > 3) {
                        listChat.remove(3);
                    }
                    if (Utils.validatePassword(edtValues.getText().toString())) {
                        listChat.add(3, new ChatBoxModel(edtValues.getText().toString(), "", true, Constant.EDT_PASSWORD, false));
                        if (listChat.size() == 4) {
                            listChat.add(4, new ChatBoxModel(getString(R.string.txt_veri_pass), getString(R.string.txt_vali), false, "VeriPass", false));
                            ((RegisterLoanActivity)getActivity()).setContent(getString(R.string.txt_enter_pass));
                        }
                        index = 5;
                        edtValues.setText("");
                    } else {
                        listChat.add(3, new ChatBoxModel(edtValues.getText().toString(), getString(R.string.txt_pass_fail), true, Constant.EDT_PASSWORD, false));
                    }
                } else if (listChat.get(index - 1).type.equalsIgnoreCase("VeriPass")) {
                    if (listChat.size() > 5) {
                        listChat.remove(5);
                    }
                    if (Utils.validatePassword(edtValues.getText().toString())) {
                        listChat.add(new ChatBoxModel(edtValues.getText().toString(), "", true, Constant.EDT_PASSWORD, false));
                        index = 6;
                        hideKeyboard();
                        layoutInput.setVisibility(View.GONE);
                        txtDone.setVisibility(View.VISIBLE);
                        edtValues.setText("");
                    } else {
                        listChat.add(new ChatBoxModel(edtValues.getText().toString(), getString(R.string.txt_Password_not_match), true, Constant.EDT_PASSWORD, false));
                    }
                }

                if (listChat.size() == 6) {
                    ((RegisterLoanActivity)getActivity()).setContent(getString(R.string.txt_done_create));
                    hideKeyboard();
                    edtValues.setFocusable(false);
                    layoutInput.setVisibility(View.GONE);
                    txtDone.setVisibility(View.VISIBLE);
                }
                lvChatBox.setAdapter(adapter);
                //adapter.notifyDataSetChanged();
                lvChatBox.setSelection(index - 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addListChat() {
        try {
            listChat = new ArrayList<ChatBoxModel>();
            listChat.add(new ChatBoxModel(getString(R.string.txt_create_username), "", false, "Username", false));
            index = 1;
            ArrayList<Integer> listResouce = new ArrayList<Integer>();
            listResouce.add(R.layout.item_chat_question);
            listResouce.add(R.layout.item_chat_answer);

            adapter = new ChatBoxAdapter(getActivity(), listResouce, listChat, this);
            lvChatBox.setAdapter(adapter);
            //adapter.notifyDataSetChanged();
            ((RegisterLoanActivity)getActivity()).setContent(getString(R.string.txt_enter_username));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void registerUser() {
        CheckNetworkRequest((BaseActivity) getActivity(), new CheckNetworkListener() {
            @Override
            public void Connected() {
                final String deviceId = Settings.Secure.getString(getActivity().getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                final String deviceName = android.os.Build.MODEL;
                SignUpRequest request = new SignUpRequest();
                request.cuid = ((RegisterLoanActivity) getActivity()).cuid;
                request.deviceId = deviceId;
                request.deviceName = deviceName;
                request.username = listChat.get(1).content;
                request.password = listChat.get(3).content;
                presenter.registerUser(request);
            }
        });
    }

    @Override
    public void onSuccess(Object objectSuccess) {
        super.onSuccess(objectSuccess);
        /*Toast.makeText(getActivity(), "Tuyệt vời, tài khoản của bạn đã được tạo thành công", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();*/
        vn.mobiapps.pruf.view.fragment.regiterloan.ConfirmAccountFragment f = new vn.mobiapps.pruf.view.fragment.regiterloan.ConfirmAccountFragment();
        ((RegisterLoanActivity) getActivity()).pushFragment(f);
    }

    @Override
    public void onError(Object objectError) {
        super.onError(objectError);
    }

    /*Call api check username*/
    @Override
    public void checkUsernameAvailability(final String username) {
        CheckNetworkRequest((BaseActivity) getActivity(), new CheckNetworkListener() {
            @Override
            public void Connected() {
                presenter.checkUsernameAvailability(username);
            }
        });
    }

    @Override
    public void onSuccessCheckUserName(Object onSuccess) {
        try {
            BaseRespone respone = (BaseRespone) onSuccess;
            if (respone.result_code.equals("0")) {
                if (listChat.size() > 1) {
                    listChat.remove(1);
                }
                listChat.add(1, new ChatBoxModel(edtValues.getText().toString(), "", true, Constant.TEXTVIEW, false));
                if (listChat.size() == 2) {
                    listChat.add(2, new ChatBoxModel(getString(R.string.txt_new_pass), getString(R.string.txt_vali), false, "NewPass", false));
                    ((RegisterLoanActivity)getActivity()).setContent(getString(R.string.txt_enter_newpass));
                }
                index = 3;
                edtValues.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            } else {
                if (listChat.size() > 1) {
                    listChat.remove(1);
                }
                listChat.add(1, new ChatBoxModel(edtValues.getText().toString(), respone.result_description, true, Constant.TEXTVIEW, false));
            }
            lvChatBox.setAdapter(adapter);
            //adapter.notifyDataSetChanged();
            lvChatBox.setSelection(index - 1);
            edtValues.setText("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorCheckUserName(Object onError) {
        //super.onError(onError);
        if (listChat.size() > 1) {
            listChat.remove(1);
        }
        listChat.add(1, new ChatBoxModel(edtValues.getText().toString(), getString(R.string.txt_not_username1), true, Constant.TEXTVIEW, false));
    }

    private Boolean checkValidate() {
        if (edtValues == null || edtValues.getText().toString().equals("")) {
            showMessage(getString(R.string.txt_not_null));
            return false;
        }
        return true;
    }

    private Boolean checkVeri() {
        if (listChat != null) {
            if (!listChat.get(3).content.equalsIgnoreCase(listChat.get(5).content)) {
                showMessage(getString(R.string.txt_password_unlike));
                return false;
            }
        }
        return true;
    }

    public void onClickEdit(int posision) {
        try {
            if (listChat.get(posision - 1).type.equalsIgnoreCase("Username")) {
                ((RegisterLoanActivity)getActivity()).setContent(getString(R.string.txt_enter_username));
                edtValues.setInputType(InputType.TYPE_CLASS_TEXT);
                edtValues.setText(listChat.get(posision).content);
                edtValues.setInputType(InputType.TYPE_CLASS_TEXT);
                lvChatBox.setSelection(posision - 1);
            } else if (listChat.get(posision - 1).type.equalsIgnoreCase("NewPass")) {
                edtValues.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                edtValues.setText(listChat.get(posision).content);
                lvChatBox.setSelection(posision - 1);
            } else if (listChat.get(posision - 1).type.equalsIgnoreCase("VeriPass")) {
                edtValues.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                edtValues.setText(listChat.get(posision).content);
                lvChatBox.setSelection(posision - 1);
            }
            index = posision;
            edtValues.setFocusableInTouchMode(true);
            layoutInput.setVisibility(View.VISIBLE);
            txtDone.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
