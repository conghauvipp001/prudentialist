package vn.mobiapps.pruf.view.fragment.regiterloan;

import android.provider.Settings;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.safetynet.SafetyNet;
import com.google.android.gms.safetynet.SafetyNetApi;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.model.model_data.regiterloan.ResponseSigup;
import vn.mobiapps.pruf.model.model_data.regiterloan.SignUpRequest;
import vn.mobiapps.pruf.presenter.regiterloan.createusername.CreateUsernamePresenterImpl;
import vn.mobiapps.pruf.presenter.regiterloan.createusername.ICreateUsernamePresenter;
import vn.mobiapps.pruf.view.activity.RegisterLoanActivity;
import vn.mobiapps.pruf.view.base.BaseActivity;
import vn.mobiapps.pruf.view.base.BaseFragment;

/**
 * Created by Truong Thien on 3/1/2019.
 */

public class ConfirmAccountFragment extends BaseFragment implements vn.mobiapps.pruf.view.fragment.regiterloan.ICreateUsernameView {

    String SITE_KEY = "6LeRu5QUAAAAACpCxO0OGO-p86iMxob4dN4oUtrq";
    String SITE_SECRET_KEY = "6LeRu5QUAAAAAAU9kpViWWOW6kILzxvxarw5Ne2y";
    private RequestQueue queue;
    private ImageView imgcapcha;
    private Button btn_confirm;
    private ICreateUsernamePresenter presenter;
    private String username = "", password = "";
    private ImageView imgBack;


    @Override
    protected int resLayout() {
        return R.layout.fragment_confirm_account;
    }

    @Override
    protected void createFragment() {
        username = getArguments().getString("Username");
        password = getArguments().getString("PassWord");

        if (presenter == null) {
            presenter = new CreateUsernamePresenterImpl(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((RegisterLoanActivity)getActivity()).hideSupport();
    }

    @Override
    protected void setupFragment(View view) {
        imgcapcha = view.findViewById(R.id.imgcapcha);
        btn_confirm = view.findViewById(R.id.btn_confirm);
        imgBack = view.findViewById(R.id.imgBack);
        queue = Volley.newRequestQueue(getContext());
        btn_confirm.setEnabled(false);
    }

    @Override
    protected void setOnClickListenerFragment() {
        imgcapcha.setOnClickListener(this);
        btn_confirm.setOnClickListener(this);
        imgcapcha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                capcha();
            }
        });

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((RegisterLoanActivity)getActivity()).popFragment();
            }
        });
    }

    private void capcha() {
        try {
            SafetyNet.getClient(getActivity()).verifyWithRecaptcha(SITE_KEY)
                    .addOnSuccessListener(getActivity(), new OnSuccessListener<SafetyNetApi.RecaptchaTokenResponse>() {
                        @Override
                        public void onSuccess(SafetyNetApi.RecaptchaTokenResponse response) {
                            if (!response.getTokenResult().isEmpty()) {
                                handleCaptchaResult(response.getTokenResult());
                            }
                        }
                    })
                    .addOnFailureListener(getActivity(), new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            if (e instanceof ApiException) {
                                ApiException apiException = (ApiException) e;
                                Log.e("TAG", "Error message: " + CommonStatusCodes.getStatusCodeString(apiException.getStatusCode()));
                            } else {
                                Log.e("tag", "Unknown type of error: " + e.getMessage());
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleCaptchaResult(final String responseToken) {
        String url = "https://www.google.com/recaptcha/api/siteverify";
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("success")) {
                                imgcapcha.setImageDrawable(getResources().getDrawable(R.drawable.img_yescapcha));
                                btn_confirm.setEnabled(true);
                            }
                        } catch (Exception ex) {
                            Log.e("", "Error message: " + ex.getMessage());

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("", "Error message: " + error.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("secret", SITE_SECRET_KEY);
                params.put("response", responseToken);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

    @Override
    public void registerUser() {
        CheckNetworkRequest((BaseActivity) getActivity(), new CheckNetworkListener() {
            @Override
            public void Connected() {
                final String deviceId = Settings.Secure.getString(getActivity().getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                final String deviceName  = android.os.Build.MODEL;
                SignUpRequest request = new SignUpRequest();
                request.cuid = ((RegisterLoanActivity) getActivity()).cuid;
                request.deviceId = deviceId;
                request.deviceName = deviceName;
                request.username = username;
                request.password = password;
                presenter.registerUser(request);
            }
        });
    }

    @Override
    public void onSuccess(Object objectSuccess) {
        super.onSuccess(objectSuccess);
        try {
            if (objectSuccess != null) {
                ResponseSigup sigup = (ResponseSigup) objectSuccess;
                if (sigup.result_code.equals("0")) {
                    DoneCreateAccount f = new DoneCreateAccount();
                    ((RegisterLoanActivity) getActivity()).pushFragment(f);
                } else {
                    showMessage(sigup.result_description);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Object objectError) {
        super.onError(objectError);
    }

    @Override
    public void checkUsernameAvailability(String username) {

    }

    @Override
    public void onSuccessCheckUserName(Object onSuccess) {

    }

    @Override
    public void onErrorCheckUserName(Object onError) {

    }
}
