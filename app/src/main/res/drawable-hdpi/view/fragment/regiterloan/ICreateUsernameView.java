package vn.mobiapps.pruf.view.fragment.regiterloan;

import vn.mobiapps.pruf.view.base.IBaseView;

/**
 * Created by Truong Thien on 1/18/2019.
 */

public interface ICreateUsernameView<T> extends IBaseView {
    void registerUser();
    void checkUsernameAvailability(String username);

    void onSuccessCheckUserName(T onSuccess);
    void onErrorCheckUserName(T onError);
}
