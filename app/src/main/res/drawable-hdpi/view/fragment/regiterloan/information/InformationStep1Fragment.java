package vn.mobiapps.pruf.view.fragment.regiterloan.information;

import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.adapter.ChatBoxAdapter;
import vn.mobiapps.pruf.model.model_view.ChatBoxModel;
import vn.mobiapps.pruf.model.model_view.RegisterModel;
import vn.mobiapps.pruf.utils.Constant;
import vn.mobiapps.pruf.utils.Utils;
import vn.mobiapps.pruf.view.activity.RegisterLoanActivity;
import vn.mobiapps.pruf.view.base.BaseFragment;
import vn.mobiapps.pruf.widgets.SpinnerTextView;
import vn.mobiapps.pruf.widgets.SpinnerTextViewListener;

/**
 * Created by Truong Thien on 12/27/2018.
 */

public class InformationStep1Fragment extends BaseFragment{

    private ListView lvChatBox;
    private LinearLayout layoutInput, layoutSeekBar, layoutAddress;
    private EditText edtValues;
    private ImageView imgSend;
    private TextView txtNext, txtMoney, txtDone;
    private SeekBar seekBar;
    private ChatBoxAdapter adapter;
    private ArrayList<ChatBoxModel> listChat;
    private SpinnerTextView spnAddress;
    private RegisterModel registerModel;

    @Override
    protected int resLayout() {
        return R.layout.fragment_information_step1;
    }

    @Override
    protected void createFragment() {
        registerModel = ((RegisterLoanActivity)getActivity()).getRegisterModel();
    }

    @Override
    protected void setupFragment(View view) {
        lvChatBox = view.findViewById(R.id.lvChatBox);
        layoutInput = view.findViewById(R.id.layoutInput);
        edtValues = view.findViewById(R.id.edtValues);
        imgSend = view.findViewById(R.id.imgSend);
        txtNext = view.findViewById(R.id.txtNext);
        txtMoney = view.findViewById(R.id.txtMoney);
        txtDone = view.findViewById(R.id.txtDone);
        seekBar = view.findViewById(R.id.seekBar);
        layoutSeekBar = view.findViewById(R.id.layoutSeekBar);
        layoutAddress = view.findViewById(R.id.layoutAddress);
        spnAddress = view.findViewById(R.id.spnAddress);
        edtValues.setFocusable(false);
        addListChat();
        settSeekBar();
    }

    @Override
    protected void setOnClickListenerFragment() {
        txtDone.setOnClickListener(this);
        imgSend.setOnClickListener(this);
        txtNext.setOnClickListener(this);
        edtValues.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.imgSend:
                if (listChat.size() == 7) {
                    listChat.add(new ChatBoxModel(edtValues.getText().toString(), "", true, Constant.TEXTVIEW, false));
                    registerModel.PhoneNumber = edtValues.getText().toString();
                    listChat.add(new ChatBoxModel(getString(R.string.txt_email_), "", false, "", false));
                    edtValues.setInputType(InputType.TYPE_CLASS_TEXT);
                    edtValues.setText("");
                } else if (listChat.size() == 9) {
                    listChat.add(new ChatBoxModel(edtValues.getText().toString(), "", true, Constant.TEXTVIEW, false));
                    registerModel.Email = edtValues.getText().toString();
                    edtValues.setText("");
                    hideKeyboard();
                    layoutInput.setVisibility(View.GONE);
                    txtNext.setVisibility(View.VISIBLE);
                }
                lvChatBox.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                lvChatBox.setSelection(listChat.size() - 1);
            break;
            case R.id.txtDone:
                if (listChat.size() == 1) {
                    listChat.add(new ChatBoxModel(Utils.formatMoney(seekBar.getProgress() + "") + " VND", "", true, Constant.TEXTVIEW, false));
                    registerModel.LoanAmount = seekBar.getProgress() + "";
                    listChat.add(new ChatBoxModel(getString(R.string.txt_monthly_income), "", false, "", false));
                    seekBar.setProgress(0);
                    hideKeyboard();
                } else if (listChat.size() == 3) {
                    listChat.add(new ChatBoxModel(Utils.formatMoney(seekBar.getProgress() + "") + " VND", "", true, Constant.TEXTVIEW, false));
                    registerModel.MonthlyIncome = seekBar.getProgress() + "";
                    listChat.add(new ChatBoxModel(getString(R.string.txt_residence), "", false, "", false));
                    layoutSeekBar.setVisibility(View.GONE);
                    layoutAddress.setVisibility(View.VISIBLE);
                }
                lvChatBox.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                lvChatBox.setSelection(listChat.size() - 1);
                break;
            case R.id.txtNext:
                InformationStep2Fragment f = new InformationStep2Fragment();
                ((RegisterLoanActivity)getActivity()).pushFragment(f);
                break;
            case R.id.edtValues:
                lvChatBox.setSelection(listChat.size() - 1);
                break;
            default:
                break;
        }
    }

    private void addListChat() {
        try {
            layoutSeekBar.setVisibility(View.VISIBLE);
            // setValues for Provincial
            String[] arrayProvincial = getResources().getStringArray(R.array.Provincial);
            ArrayList<String> provincial = new ArrayList<String>(Arrays.asList(arrayProvincial));
            spnAddress.setListValue(getString(R.string.txt_provincial), provincial, new SpinnerTextViewListener() {
                @Override
                public void onClick() {
                    if (listChat.size() == 5) {
                        listChat.add(new ChatBoxModel(spnAddress.getSelectedValue(), "", true, Constant.TEXTVIEW, false));
                        registerModel.Address = spnAddress.getSelectedValue();
                        listChat.add(new ChatBoxModel(getString(R.string.txt_phone), "", false, "", false));
                        layoutSeekBar.setVisibility(View.GONE);
                        layoutAddress.setVisibility(View.GONE);
                        edtValues.setFocusableInTouchMode(true);
                        lvChatBox.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        lvChatBox.setSelection(listChat.size() - 1);
                    }
                }
            });

            listChat = new ArrayList<ChatBoxModel>();
            listChat.add(new ChatBoxModel(getString(R.string.txtLoan), getString(R.string.txt_min_max), false, "", false));

            ArrayList<Integer> listResouce = new ArrayList<Integer>();
            listResouce.add(R.layout.item_chat_question);
            listResouce.add(R.layout.item_chat_answer);

            adapter = new ChatBoxAdapter(getActivity(), listResouce, listChat, this);
            lvChatBox.setAdapter(adapter);
            Utils.setListViewHeightBasedOnItems(lvChatBox);
            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void settSeekBar() {
        txtMoney.setText(Utils.formatMoney(seekBar.getProgress() + "") + " VND");

        this.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;
            // Khi giá trị progress thay đổi.
            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                progress = progressValue;
                txtMoney.setText(Utils.formatMoney(progressValue + "") + " VND");
            }
            // Khi người dùng bắt đầu cử chỉ kéo thanh gạt.
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }
            // Khi người dùng kết thúc cử chỉ kéo thanh gạt.
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                txtMoney.setText(Utils.formatMoney(progress + "") + " VND");
            }
        });
    }
}
