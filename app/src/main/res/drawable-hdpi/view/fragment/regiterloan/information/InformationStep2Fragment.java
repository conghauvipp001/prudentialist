package vn.mobiapps.pruf.view.fragment.regiterloan.information;

import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.model.model_view.RegisterModel;
import vn.mobiapps.pruf.view.activity.RegisterLoanActivity;
import vn.mobiapps.pruf.view.base.BaseFragment;
import vn.mobiapps.pruf.widgets.CaptureImage;

/**
 * Created by Truong Thien on 1/10/2019.
 */

public class InformationStep2Fragment extends BaseFragment implements CaptureImage.onCaptureImage {

    private Button btnConfirm;
    public CaptureImage captureImage;
    private ImageView imgCmnd, imgTree;
    private TextView txtPicturesAgain, txt_title_img;
    private int numberClick = 1;
    private RegisterModel registerModel;

    @Override
    protected int resLayout() {
        return R.layout.fragment_information_step2;
    }

    @Override
    protected void createFragment() {
        if (this.captureImage == null) {
            this.captureImage = new CaptureImage(getActivity(), this, this);
        }
        registerModel = ((RegisterLoanActivity)getActivity()).getRegisterModel();
    }

    @Override
    protected void setupFragment(View view) {
        btnConfirm = view.findViewById(R.id.btnConfirm);
        imgCmnd = view.findViewById(R.id.imgCmnd);
        imgTree = view.findViewById(R.id.imgTree);
        txtPicturesAgain = view.findViewById(R.id.txtPicturesAgain);
        txt_title_img = view.findViewById(R.id.txt_title_img);
    }

    @Override
    protected void setOnClickListenerFragment() {
        btnConfirm.setOnClickListener(this);
        imgCmnd.setOnClickListener(this);
        txtPicturesAgain.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btnConfirm:{
                numberClick = numberClick + 1;
                imgCmnd.setImageResource(R.drawable.otherpapers);
                setValues(numberClick);
                if (numberClick == 3) {
                    imgTree.setImageResource(R.drawable.tree3);
                }
                if (numberClick == 4) {
                    vn.mobiapps.pruf.view.fragment.regiterloan.information.InformationStep3Fragment f = new vn.mobiapps.pruf.view.fragment.regiterloan.information.InformationStep3Fragment();
                    ((RegisterLoanActivity)getActivity()).pushFragment(f);
                }
            }
            break;
            case R.id.imgCmnd:{
                captureImage.selectImage();
            }
            break;
            case R.id.txtPicturesAgain:{
                captureImage.selectImage();
            }
            break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        numberClick = 1;
    }

    private void setValues(int numberClick) {
        try {
            if (numberClick == 1) {
                txt_title_img.setText(R.string.txt_IdentityCardFront);
            } else if (numberClick == 2) {
                txt_title_img.setText(R.string.txt_IdentityCardBehind);
            } else if (numberClick == 3) {
                txt_title_img.setText(R.string.txt_IncomePapers);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //=======================Code chọn hinh============================
    /*Lắng nghe kết quả chọn là chọn cái nào*/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("RequestCode: >>", requestCode + "ResultCode" + resultCode);
        captureImage.onActivityResult(requestCode, resultCode, data);
    }

    // Khi người dùng trả lời yêu cầu cấp quyền (cho phép hoặc từ chối).
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        captureImage.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /*Hàm trả về bitmap khi chụp hình*/
    @Override
    public void onCaptureImage_(Bitmap thumbnail) {
        setCaptureImage(thumbnail);
    }

    /*Hàm trả về bitmap khi chụp hình lấy từ URI*/
    @Override
    public void onCaptureImageFile_(Bitmap bm) {
        this.setCaptureImage(bm);
    }

    /*Hàm trả về bitmap khi chọn từ thư viện*/
    @Override
    public void onSelectFromGallery_(Bitmap bm) {
        setCaptureImage(bm);
    }
    //====================================End code chọn hình=============================

    private void setCaptureImage(Bitmap bm) {
        try {
            if (bm != null) {
                imgCmnd.setImageBitmap(bm);
                if (numberClick == 1) {
                    registerModel.IdentityCardFront = bm;
                } else if (numberClick == 2) {
                    registerModel.IdentityCardBehind = bm;
                } else if (numberClick == 3) {
                    registerModel.IncomePapers = bm;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
