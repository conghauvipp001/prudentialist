package vn.mobiapps.pruf.view.fragment.regiterloan;

/**
 * Created by Truong Thien on 1/18/2019.
 */

public interface ICheckStatusView extends vn.mobiapps.pruf.view.base.IBaseView {
    void checkRegisterCondition();
}
