package vn.mobiapps.pruf.view.fragment.regiterloan;

import android.content.Intent;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.model.model_data.regiterloan.ResponseCheckRegisterCondition;
import vn.mobiapps.pruf.view.activity.RegisterLoanActivity;
import vn.mobiapps.pruf.view.base.BaseFragment;

/**
 * Created by Truong Thien on 12/20/2018.
 */

public class CheckStatusAccountFragment extends BaseFragment {
    private TextView txtWelcomeName, txtRequest;
    private ResponseCheckRegisterCondition reponse;
    private ImageView imgBack, img;
    private Button btn_Action, btn_ActionRegietr;

    @Override
    protected int resLayout() {
        return R.layout.fragment_check_status_account;
    }

    @Override
    protected void createFragment() {
        reponse = (ResponseCheckRegisterCondition) getArguments().getSerializable("Response");
    }

    @Override
    public void onResume() {
        super.onResume();
        ((RegisterLoanActivity)getActivity()).hideSupport();
    }

    @Override
    protected void setupFragment(View view) {
        txtWelcomeName = view.findViewById(R.id.txtWelcomeName);
        txtRequest = view.findViewById(R.id.txtRequest);
        btn_Action = view.findViewById(R.id.btn_Action);
        btn_ActionRegietr = view.findViewById(R.id.btn_ActionRegietr);
        imgBack = view.findViewById(R.id.imgBack);
        img = view.findViewById(R.id.img);
        setValues(reponse);
    }

    @Override
    protected void setOnClickListenerFragment() {
        btn_Action.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        btn_ActionRegietr.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_Action:
                hideKeyboard();
                BaseFragment fragment = null;
                if (reponse.data.status.equals("NATIONALID_DOB_SUCCESS")) {
                    fragment = new vn.mobiapps.pruf.view.fragment.regiterloan.CreateUsernameFragment();
                } else if (reponse.data.status.equals("NATIONALID_DOB_EXIST")) {
                    Intent intent = new Intent(getActivity(), vn.mobiapps.pruf.view.activity.LoginActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                    //fragment = new CreateUsernameFragment();
                } else if (reponse.data.status.equals("NATIONALID_DOB_FAIL")) {
                    Intent intent = new Intent(getActivity(), vn.mobiapps.pruf.view.activity.LoginActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                } else if (reponse.data.status.equals("CORRECT_NATIONALID_WRONG_DOB")) {
                    ((RegisterLoanActivity)getActivity()).onBackPressed();
                } else if (reponse.data.status.equals("PROCESSING_LOAN_PROFILE")) {
                    Intent intent = new Intent(getActivity(), vn.mobiapps.pruf.view.activity.LoginActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                } else if (reponse.data.status.equals("NO_LOAN_PROFILE")) {
                    Intent intent = new Intent(getActivity(), vn.mobiapps.pruf.view.activity.LoginActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
                if (fragment != null) {
                    ((RegisterLoanActivity) getActivity()).pushFragment(fragment);
                }
                break;
            case R.id.imgBack: {
                ((RegisterLoanActivity) getActivity()).popFragment();
            }
            break;
            case R.id.btn_ActionRegietr: {
                //((RegisterLoanActivity) getActivity()).popFragment();
            }
            break;
            default:
                break;
        }
    }

    private void setValues(ResponseCheckRegisterCondition reponse) {
        if (reponse != null && reponse.data != null) {
            String name = "";
            if (reponse.data.fullName != null) {
                name = reponse.data.fullName;
            }
            if (reponse.data.status.equals("NATIONALID_DOB_EXIST")) {
                txtWelcomeName.setText(Html.fromHtml("Chúc mừng, <br> <font color='#00FFCC'> " + name));
                txtRequest.setText(Html.fromHtml(getString(R.string.txt_exits_accont)));
                btn_Action.setText(R.string.txt_login_);
            } else if (reponse.data.status.equals("NATIONALID_DOB_SUCCESS")) {
                txtWelcomeName.setText(Html.fromHtml("Chúc mừng, <br> <font color='#00FFCC'> " + name));
                txtRequest.setText(Html.fromHtml("Bạn đã có tài khoản iShinh@n. Đăng nhập ngay để khám phá những tính năng tài chính thú vị"));
                btn_Action.setText(R.string.txt_create_account_);
            } else if (reponse.data.status.equals("NATIONALID_DOB_FAIL")) {
                txtWelcomeName.setText(Html.fromHtml("Không tìm thấy thông tin"));
                txtRequest.setText(Html.fromHtml("Bạn cần có khoản vay với Shinhan Finance để sử dụng iShinh@n. Hãy để lại thông tin đăng ký để được tư vấn nhé"));
                btn_Action.setText(R.string.txt_register_1);
            } else if (reponse.data.status.equals("CORRECT_NATIONALID_WRONG_DOB")) {
                if (((RegisterLoanActivity)getActivity()).countRequest == 1) {
                    txtWelcomeName.setText(Html.fromHtml("Không tìm thấy thông tin"));
                    txtRequest.setText(Html.fromHtml("Có vẻ ngày tháng năm sinh của bạn không khớp. Hãy thử lại nhé"));
                    btn_Action.setText(R.string.txt_agen);
                } else {
                    txtWelcomeName.setText(Html.fromHtml("Không tìm thấy thông tin"));
                    txtRequest.setText(Html.fromHtml("Ngày tháng năm sinh vẫn chưa khớp. <br> Bạn có muốn thử lại? <br> Hoặc bạn có thể đăng ký khoản vay mới với Shinhan Finance"));
                    btn_Action.setText(R.string.txt_agen);
                    btn_ActionRegietr.setVisibility(View.VISIBLE);
                }

                ((RegisterLoanActivity)getActivity()).countRequest += 1;
            } else if (reponse.data.status.equals("PROCESSING_LOAN_PROFILE")) {

            } else if (reponse.data.status.equals("NO_LOAN_PROFILE")) {
                txtWelcomeName.setText(Html.fromHtml("Xin chào, <br> <font color='#00FFCC'> " + name));
                txtRequest.setText(Html.fromHtml("Bạn không có hồ sơ đề nghị vay nào đang được sử lý. Để sử dụng iShinh@n, bạn cần đăng ký với Shinhan Frinance"));
                btn_Action.setText(R.string.txt_register_1);
            }
        }
    }
}
