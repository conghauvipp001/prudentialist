package vn.mobiapps.pruf.view.fragment.customerpage;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.adapter.CustomAdapter;
import vn.mobiapps.pruf.model.model_view.Manager;
import vn.mobiapps.pruf.view.base.BaseFragment;

/**
 * Created by Truong Thien on 12/13/2018.
 */

public class CustomerPageFragment extends BaseFragment implements vn.mobiapps.pruf.view.fragment.customerpage.ICustomerPageView {

    private ListView lvVay, lvHoso;
    private LinearLayout lnLoanManagement, lnRecordsManagement;
    private ArrayList<Manager> listVay;
    private ArrayList<Manager> listHoSo;
    private CustomAdapter customAdapter;
    private CustomAdapter customAdapter1;

    @Override
    protected int resLayout() {
        return R.layout.fragment_customer_page;
    }

    @Override
    protected void createFragment() {

    }

    @Override
    protected void setupFragment(View view) {
        lvVay = view.findViewById(R.id.lvVay);
        lvHoso = view.findViewById(R.id.lvHoso);
        lnLoanManagement = view.findViewById(R.id.lnLoanManagement);
        lnRecordsManagement = view.findViewById(R.id.lnRecordsManagement);
        listVay = new ArrayList<>();
        listVay.add(new Manager("Số hợp Đồng", "values"));
        listVay.add(new Manager("Ngày hợp đồng", "values"));
        listVay.add(new Manager("Loại tiền", "values"));
        listVay.add(new Manager("Tỉ giá", "values"));
        customAdapter = new CustomAdapter(getActivity(), listVay);
        lvVay.setAdapter(customAdapter);

        listHoSo = new ArrayList<>();
        listHoSo.add(new Manager("Thông tin hợp đồng", ""));
        listHoSo.add(new Manager("Thông tin về phí và khoản tạm ứng", "values"));
        listHoSo.add(new Manager("Thông tin hợp đồng", ""));
        listHoSo.add(new Manager("Thông tin về phí và khoản tạm ứng", "values"));
        listHoSo.add(new Manager("Thông tin hợp đồng", ""));
        listHoSo.add(new Manager("Thông tin về phí và khoản tạm ứng", "values"));

        customAdapter1 = new CustomAdapter(getActivity(), listHoSo);
        lvHoso.setAdapter(customAdapter1);
        setListViewHeightBasedOnChildren(lvVay);
        setListViewHeightBasedOnChildren(lvHoso);
    }

    @Override
    protected void setOnClickListenerFragment() {
        //lnLoanManagement.setOnClickListener(this);
        //lnRecordsManagement.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.lnLoanManagement:
                hideKeyboard();
                if (lvVay.getVisibility() == View.GONE) {
                    lvVay.setVisibility(View.VISIBLE);
                    lvHoso.setVisibility(View.GONE);
                } else if (lvVay.getVisibility() == View.VISIBLE) {
                    lvVay.setVisibility(View.GONE);
                }
                break;
            case R.id.lnRecordsManagement:
                hideKeyboard();
                if (lvHoso.getVisibility() == View.GONE) {
                    lvHoso.setVisibility(View.VISIBLE);
                    lvVay.setVisibility(View.GONE);
                } else if (lvHoso.getVisibility() == View.VISIBLE) {
                    lvHoso.setVisibility(View.GONE);
                }
                break;
            default:
                break;
        }
    }

    public void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}
