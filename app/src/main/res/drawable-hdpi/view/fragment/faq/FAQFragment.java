package vn.mobiapps.pruf.view.fragment.faq;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.adapter.FAQAdapter;
import vn.mobiapps.pruf.model.model_data.faq.FAQ;
import vn.mobiapps.pruf.model.model_data.faq.FaqDetail;
import vn.mobiapps.pruf.model.model_view.FAQModel;
import vn.mobiapps.pruf.presenter.faq.FaqPresenterImpl;
import vn.mobiapps.pruf.presenter.faq.IFaqPresenter;
import vn.mobiapps.pruf.view.base.BaseFragment;

/**
 * Created by Truong Thien on 12/27/2018.
 */

public class FAQFragment extends BaseFragment implements vn.mobiapps.pruf.view.fragment.faq.IFaqView {

    private ListView lvFAQ;
    private ImageView imgBack;
    private ArrayList<FAQModel> listModel;
    private FAQAdapter adapter;
    private IFaqPresenter presenter;

    @Override
    protected int resLayout() {
        return R.layout.fragment_faq;
    }

    @Override
    protected void createFragment() {
        if (presenter == null) {
            presenter = new FaqPresenterImpl(this);
        }
    }

    @Override
    protected void setupFragment(View view) {
        lvFAQ = view.findViewById(R.id.lvFAQ);
        imgBack = view.findViewById(R.id.imgBack);
        getListFAQ("vi", "");
    }

    @Override
    protected void setOnClickListenerFragment() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((vn.mobiapps.pruf.view.activity.FAQActivity)getActivity()).popFragment();
            }
        });

        lvFAQ.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!listModel.get(position).isSectionHeader) {
                    getFAQById(listModel.get(position).id);
                }
            }
        });
    }

    @Override
    public void getListFAQ(final String language, final String status) {
        CheckNetworkRequest((vn.mobiapps.pruf.view.base.BaseActivity) getActivity(), new CheckNetworkListener() {
            @Override
            public void Connected() {
                presenter.getListFAQ(language, status);
            }
        });
    }

    @Override
    public void onSuccess(Object objectSuccess) {
        super.onSuccess(objectSuccess);
        try {
            if (objectSuccess != null) {
                FAQ offices = (FAQ) objectSuccess;
                listModel = new ArrayList<FAQModel>();
                for (int i = 0; i < offices.data.listFAQ.length; i++) {
                    listModel.add(new FAQModel("", "", "", offices.data.listFAQ[i].title, true));
                    for (int j = 0; j < offices.data.listFAQ[i].answerQuestion.length; j++) {
                        listModel.add(new FAQModel(offices.data.listFAQ[i].answerQuestion[j].answer_question_id, "", offices.data.listFAQ[i].answerQuestion[j].question, "", false));
                    }
                }
                ArrayList<Integer> listResoures = new ArrayList<Integer>();
                listResoures.add(R.layout.item_faq);
                listResoures.add(R.layout.header_faq);

                adapter = new FAQAdapter(getActivity(), listResoures, listModel);
                lvFAQ.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Object objectError) {
        super.onError(objectError);
    }


    /*Cail api get detail*/
    @Override
    public void getFAQById(final String id) {
        CheckNetworkRequest((vn.mobiapps.pruf.view.base.BaseActivity) getActivity(), new CheckNetworkListener() {
            @Override
            public void Connected() {
               presenter.getFAQById(id);
            }
        });
    }

    @Override
    public void onSuccessDetail(Object onSuccess) {
        try {
            if (onSuccess != null) {
                FaqDetail detail = (FaqDetail) onSuccess;

                vn.mobiapps.pruf.view.fragment.faq.FAQDetailFragment f = new vn.mobiapps.pruf.view.fragment.faq.FAQDetailFragment();
                Bundle args = new Bundle();
                args.putSerializable("DataDetail", detail.data.faq);
                f.setArguments(args);
                ((vn.mobiapps.pruf.view.activity.FAQActivity)getActivity()).pushFragment(f);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorDetail(Object onError) {

    }
}
