package vn.mobiapps.pruf.view.fragment.faq;

import android.text.Html;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.view.base.BaseFragment;

/**
 * Created by Truong Thien on 12/27/2018.
 */

public class FAQDetailFragment extends BaseFragment {

    private ImageView imgBack;
    private String dataDetail;
    private WebView webview;
    private TextView txt_values;

    @Override
    protected int resLayout() {
        return R.layout.fragment_faq_detail;
    }

    @Override
    protected void createFragment() {
        dataDetail = getArguments().getString("DataDetail");
    }

    @Override
    protected void setupFragment(View view) {
        imgBack = view.findViewById(R.id.imgBack);
        txt_values = view.findViewById(R.id.txt_values);
        //webview = view.findViewById(R.id.webview);

        if (dataDetail != null) {
            //webview.loadData(dataDetail, "answer/html", "utf-8");
            txt_values.setText(Html.fromHtml(dataDetail));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        webview = null;
    }

    @Override
    protected void setOnClickListenerFragment() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((vn.mobiapps.pruf.view.activity.FAQActivity)getActivity()).onBackPressed();
            }
        });
    }
}
