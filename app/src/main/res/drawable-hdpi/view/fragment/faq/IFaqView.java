package vn.mobiapps.pruf.view.fragment.faq;

import vn.mobiapps.pruf.view.base.IBaseView;

/**
 * Created by Truong Thien on 1/29/2019.
 */

public interface IFaqView<T> extends IBaseView {
    void getListFAQ(String language, String status);
    void getFAQById(String id);

    void onSuccessDetail(T onSuccess);
    void onErrorDetail(T onError);
}
