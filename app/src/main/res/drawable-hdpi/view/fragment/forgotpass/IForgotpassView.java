package vn.mobiapps.pruf.view.fragment.forgotpass;

import vn.mobiapps.pruf.view.base.IBaseView;

/**
 * Created by Truong Thien on 1/28/2019.
 */

public interface IForgotpassView<T> extends IBaseView {
    void checkRegisterCondition();
    void retrievePassword();
    void retrieveUsername();

    void methodSend(String methodSend);

    void onSuccessMethodSend(Object objectSuccess);
    void onErrorMethodSend(Object objectError);
}
