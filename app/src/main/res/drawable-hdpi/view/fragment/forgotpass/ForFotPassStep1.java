package vn.mobiapps.pruf.view.fragment.forgotpass;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.view.base.BaseFragment;

/**
 * Created by Truong Thien on 12/24/2018.
 */

public class ForFotPassStep1 extends BaseFragment {

    private RelativeLayout layout_back;
    private TextView txtForgotUsername, txtForgotPass;

    @Override
    protected int resLayout() {
        return R.layout.fragment_forgotpass_step1;
    }

    @Override
    protected void createFragment() {

    }

    @Override
    protected void setupFragment(View view) {
        layout_back = view.findViewById(R.id.layout_back);
        txtForgotUsername = view.findViewById(R.id.txtForgotUsername);
        txtForgotPass = view.findViewById(R.id.txtForgotPass);
        ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).showSupport();
    }

    @Override
    protected void setOnClickListenerFragment() {
        layout_back.setOnClickListener(this);
        txtForgotUsername.setOnClickListener(this);
        txtForgotPass.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.txtForgotUsername:{
                ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).styleClick = "ForgotUsername";
                vn.mobiapps.pruf.view.fragment.forgotpass.ForFotPassStep2 f = new vn.mobiapps.pruf.view.fragment.forgotpass.ForFotPassStep2();
                ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).pushFragment(f);
            }
            break;
            case R.id.txtForgotPass:
                ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).styleClick = "ForgotPass";
                vn.mobiapps.pruf.view.fragment.forgotpass.ForFotPassStep2 f = new vn.mobiapps.pruf.view.fragment.forgotpass.ForFotPassStep2();
                ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).pushFragment(f);
                break;
            case R.id.layout_back:
                ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).hideSupport();
    }
}
