package vn.mobiapps.pruf.view.fragment.forgotpass;

import android.content.Intent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.utils.Utils;
import vn.mobiapps.pruf.view.activity.LoginActivity;
import vn.mobiapps.pruf.view.base.BaseFragment;

/**
 * Created by Truong Thien on 12/24/2018.
 */

public class ForFotPassStep3 extends BaseFragment {

    private TextView txtNotReceiving, txtOpenApp, txtGmail, txtMessage, txtUndo;
    private LinearLayout layoutChooseApp, layoutOpen;
    private Boolean isOpenApp = false;

    @Override
    protected int resLayout() {
        return R.layout.fragment_forgotpass_step3;
    }

    @Override
    protected void createFragment() {

    }

    @Override
    protected void setupFragment(View view) {
        txtNotReceiving = view.findViewById(R.id.txtNotReceiving);
        txtOpenApp = view.findViewById(R.id.txtOpenApp);
        txtGmail = view.findViewById(R.id.txtGmail);
        txtMessage = view.findViewById(R.id.txtMessage);
        txtUndo = view.findViewById(R.id.txtUndo);
        layoutChooseApp = view.findViewById(R.id.layoutChooseApp);
        layoutOpen = view.findViewById(R.id.layoutOpen);
    }

    @Override
    protected void setOnClickListenerFragment() {
        txtNotReceiving.setOnClickListener(this);
        txtOpenApp.setOnClickListener(this);
        txtGmail.setOnClickListener(this);
        txtMessage.setOnClickListener(this);
        txtUndo.setOnClickListener(this);
        layoutChooseApp.setOnClickListener(this);
        layoutOpen.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.txtNotReceiving:
                break;
            case R.id.txtOpenApp:
                Animation slideenter = AnimationUtils.loadAnimation(getActivity(), R.anim.enter);
                layoutChooseApp.setVisibility(View.VISIBLE);
                layoutOpen.setVisibility(View.VISIBLE);
                layoutOpen.setAnimation(slideenter);
                isOpenApp = true;
                break;
            case R.id.txtGmail:
                Utils.openEmail(getContext());
                break;
            case R.id.txtMessage:
                Utils.openSMS(getContext());
                break;
            case R.id.txtUndo:
            case R.id.layoutChooseApp:
                layoutChooseApp.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).hideSupport();
        if (isOpenApp) {
            isOpenApp = false;
            String style = ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).styleClick;
            if (style.equals("ForgotUsername")) {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            } else {
                vn.mobiapps.pruf.view.fragment.forgotpass.ForFotPassStep4 f = new vn.mobiapps.pruf.view.fragment.forgotpass.ForFotPassStep4();
                ((vn.mobiapps.pruf.view.activity.ForgotPassActivity) getActivity()).pushFragment(f);
            }
        }
    }
}
