package vn.mobiapps.pruf.view.fragment.forgotpass;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.model.model_data.BaseRespone;
import vn.mobiapps.pruf.model.model_data.home.MethodSend;
import vn.mobiapps.pruf.model.model_data.regiterloan.ResponseCheckRegisterCondition;
import vn.mobiapps.pruf.model.model_view.ChatBoxModel;
import vn.mobiapps.pruf.presenter.forgotpass.ForgotPassPresenterImpl;
import vn.mobiapps.pruf.presenter.forgotpass.IForgotPassPresenter;
import vn.mobiapps.pruf.utils.Constant;
import vn.mobiapps.pruf.utils.Utils;
import vn.mobiapps.pruf.view.base.BaseActivity;
import vn.mobiapps.pruf.view.base.BaseChooseAppActivity;
import vn.mobiapps.pruf.view.base.BaseFragment;
import vn.mobiapps.pruf.view.fragment.home.inputotp.InputOTPFragment;
import vn.mobiapps.pruf.widgets.ListViewChatBox;

/**
 * Created by Truong Thien on 12/24/2018.
 */

public class ForFotPassStep2 extends BaseFragment implements IForgotpassView {

    private TextView txtNext;
    private LinearLayout layoutInput, layoutShowButton, lyInput;
    private EditText edtValues;
    private ImageView imgSend, imgBack;
    private Button btnGetUser, btnGetPass;
    private ArrayList<ChatBoxModel> listChat;
    private ListViewChatBox lvChatBox;
    private int index = 0;
    private IForgotPassPresenter presenter;

    @Override
    protected int resLayout() {
        return R.layout.fragment_forgotpass_step2;
    }

    @Override
    protected void createFragment() {
        if (presenter == null) {
            presenter = new ForgotPassPresenterImpl(this);
        }
    }

    @Override
    protected void setupFragment(View view) {
        txtNext = view.findViewById(R.id.txtNext);
        layoutInput = view.findViewById(R.id.layoutInput);
        layoutShowButton = view.findViewById(R.id.layoutShowButton);
        lyInput = view.findViewById(R.id.lyInput);
        edtValues = view.findViewById(R.id.edtValues);
        imgSend = view.findViewById(R.id.imgSend);
        imgBack = view.findViewById(R.id.imgBack);
        lvChatBox = view.findViewById(R.id.lvChatBox);
        btnGetUser = view.findViewById(R.id.btnGetUser);
        btnGetPass = view.findViewById(R.id.btnGetPass);
        ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).showSupport();
        ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).setMarginBottom(400);
        addListChat();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).showSupport();
    }

    @Override
    protected void setOnClickListenerFragment() {
        txtNext.setOnClickListener(this);
        imgSend.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        edtValues.setOnClickListener(this);
        btnGetUser.setOnClickListener(this);
        btnGetPass.setOnClickListener(this);
        ((BaseChooseAppActivity)getActivity()).txtGmailApp.setOnClickListener(this);
        ((BaseChooseAppActivity)getActivity()).txtMessageApp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btnGetUser:

            break;
            case R.id.btnGetPass:

            break;
            case R.id.txtNext: {
                checkRegisterCondition();
            }
            break;
            case R.id.imgBack:
                hideKeyboard();
                ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).onBackPressed();
            break;
            case R.id.imgSend:
                clickAdd();
                break;
            case R.id.edtValues: {
                if (listChat.get(index - 1).type.equals("Birthday")) {
                    DatePicker();
                }
            }
            break;
            case R.id.txtGmailApp: {
                ((BaseChooseAppActivity)getActivity()).hidePupUpChooseApps();
                sendMethod("MAIL");
            }
                break;
            case R.id.txtMessageApp: {
                ((BaseChooseAppActivity)getActivity()).hidePupUpChooseApps();
                sendMethod("SMS");
            }
                break;
            default:
                break;
        }
    }

    private void sendMethod(String methodSend){
        ((BaseChooseAppActivity)getActivity()).hidePupUpChooseApps();
        if (!methodSend.equals("")) {
            methodSend(methodSend);
        }
    }

    private void clickAdd() {
        try {
            hideKeyboard();
            if (checkValidate(edtValues)) {
                if (listChat.get(index - 1).type.equals("CMND")) {
                    if (listChat.size() > 2) {
                        listChat.remove(2);
                    }
                    if (checkCMND(edtValues.getText().toString())) {
                        listChat.add(2, new ChatBoxModel(edtValues.getText().toString(), "", true, Constant.TEXTVIEW, false));
                        if (listChat.size() == 3) {
                            listChat.add(3, new ChatBoxModel(getString(R.string.txt_enter_birthday), "", false, "Birthday", false));
                            ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).setContent(getString(R.string.txt_endter_birthday));
                        }
                        edtValues.setFocusable(false);
                        index = 4;
                        edtValues.setText("");
                    } else {
                        listChat.add(2, new ChatBoxModel(edtValues.getText().toString(), "Số chứng minh nhân dân không hợp lệ", true, Constant.TEXTVIEW, false));
                    }
                } else if (listChat.get(index - 1).type.equalsIgnoreCase("Birthday")) {
                    if (listChat.size() > 4) {
                        listChat.remove(4);
                    }
                    if (Utils.countYear(edtValues.getText().toString())) {
                        listChat.add(4, new ChatBoxModel(edtValues.getText().toString(), "", true, Constant.TEXTVIEW, false));
                        index = 5;
                        layoutInput.setVisibility(View.GONE);
                        txtNext.setVisibility(View.VISIBLE);
                    } else {
                        listChat.add(4, new ChatBoxModel(edtValues.getText().toString(), getString(R.string.txt_old), true, Constant.TEXTVIEW, false));
                    }
                }
                lvChatBox.updateAdapter();
                lvChatBox.setSelection(index - 1);
                if (listChat.size() == 5) {
                    ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).setContent(getString(R.string.txt_enter_agan));
                    layoutInput.setVisibility(View.GONE);
                    txtNext.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addListChat() {
        try {
            listChat = new ArrayList<ChatBoxModel>();
            lvChatBox.setData(getActivity(), listChat, this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    listChat.add(new ChatBoxModel("Có thể bạn cần lấy lại Tên đăng nhập hoặc tạo mật khẩu mới", "", false, "", false));
                    ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).setContent("Bấm vào LẤY TÊN ĐĂNG NHẬP hoặc LẤY MẬT KHẨU");
                    lvChatBox.adapter.currentPositionQuestion = 0;
                    lvChatBox.updateAdapter();
                    showButton();
                }
            }, 1000);
            /*new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    String style = ((ForgotPassActivity) getActivity()).styleClick;
                    if (style.equals("ForgotUsername")) {
                        listChat.add(new ChatBoxModel(getString(R.string.txt_ressendUsername), "", false, "", false));
                    } else if (style.equalsIgnoreCase("ForgotPass")) {
                        listChat.add(new ChatBoxModel(getString(R.string.txt_ressendPass), "", false, "", false));
                    }
                    lvChatBox.adapter.currentPositionQuestion = 0;
                    lvChatBox.updateAdapter();
                }
            }, 1000);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    listChat.add(new ChatBoxModel(getString(R.string.txt_enter_CMND), "", false, "CMND", false));
                    index = 2;
                    ((ForgotPassActivity)getActivity()).setContent(getString(R.string.txt_enter_cmnd));
                    lvChatBox.adapter.currentPositionQuestion = 1;
                    lvChatBox.updateAdapter();
                }
            }, 2000);*/

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void DatePicker() {
        try {
            final Calendar calendar = Calendar.getInstance();
            int day, month, year;
            day = calendar.get(Calendar.DAY_OF_MONTH);
            month = calendar.get(Calendar.MONTH);
            year = calendar.get(Calendar.YEAR);
            final DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    calendar.set(year, month, dayOfMonth);
                    SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
                    edtValues.setText(sdf1.format(calendar.getTime()));
                }
            }, year, month, day);
            //calendar.set(year - 18, 11, 31);
            datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
            datePickerDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onClickEdit(int posision) {
        try {
            index = posision;
            if (listChat.get(posision - 1).type.equalsIgnoreCase("CMND")) {
                ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).setContent(getString(R.string.txt_enter_cmnd));
                edtValues.setText(listChat.get(posision).content);
                index = posision;
                edtValues.setFocusableInTouchMode(true);
            } else if (listChat.get(posision - 1).type.equalsIgnoreCase("Birthday")) {
                ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).setContent(getString(R.string.txt_endter_birthday));
                DatePicker();
                edtValues.setFocusable(false);
                index = posision;
            }
            layoutInput.setVisibility(View.VISIBLE);
            txtNext.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Boolean checkValidate(EditText edtValues) {
        if (edtValues == null || edtValues.getText().toString().equals("")) {
            showMessage(getString(R.string.txt_not_null));
            return false;
        }
        return true;
    }

    /*Call api checkRegisterCondition*/
    @Override
    public void checkRegisterCondition() {
        CheckNetworkRequest((BaseActivity) getActivity(), new CheckNetworkListener() {
            @Override
            public void Connected() {
                presenter.checkRegisterCondition(listChat.get(4).content, listChat.get(2).content, "IS_RETRIEVE_USERNAME_PASSWORD");
            }
        });
    }

    @Override
    public void onSuccess(Object objectSuccess) {
        super.onSuccess(objectSuccess);
        try {
            if (objectSuccess != null) {
                ResponseCheckRegisterCondition reponse = (ResponseCheckRegisterCondition) objectSuccess;
                if (reponse.result_code.equals("0") && reponse.data != null && reponse.data.status != null) {
                    if (reponse.data.status.equals("NATIONALID_DOB_EXIST")) {
                        ((vn.mobiapps.pruf.view.activity.ForgotPassActivity) getActivity()).cuid = reponse.data.cuid;
                        String style = ((vn.mobiapps.pruf.view.activity.ForgotPassActivity) getActivity()).styleClick;
                        if (style.equals("ForgotUsername")) {
                            ((BaseChooseAppActivity)getActivity()).txtTitle.setText(R.string.txt_get_username);
                        } else {
                            ((BaseChooseAppActivity)getActivity()).txtTitle.setText(R.string.txt_get_OTP);
                        }
                        ((BaseChooseAppActivity)getActivity()).showPopUpChooApps();
                    } else if (reponse.data.status.equals("CORRECT_NATIONALID_WRONG_DOB")) {
                        if (listChat.size() > 4) {
                            listChat.remove(4);
                        }
                        listChat.add(4, new ChatBoxModel(edtValues.getText().toString(), reponse.result_description, true, Constant.TEXTVIEW, false));
                        lvChatBox.updateAdapter();
                        lvChatBox.setSelection(index - 1);
                    }
                } else {
                    showMessage(reponse.result_description);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Object objectError) {
        super.onError(objectError);
    }

    /*Call api retrievePassword*/
    @Override
    public void retrievePassword() {

    }

    /*Call api retrieveUsername*/
    @Override
    public void retrieveUsername() {

    }

    @Override
    public void methodSend(final String methodSend_) {
        CheckNetworkRequest((BaseActivity) getActivity(), new CheckNetworkListener() {
            @Override
            public void Connected() {
                presenter.methodSend(new MethodSend(((vn.mobiapps.pruf.view.activity.ForgotPassActivity) getActivity()).cuid, methodSend_));
            }
        });
    }

    @Override
    public void onSuccessMethodSend(Object objectSuccess) {
        try {
            if (objectSuccess != null) {
                BaseRespone response = (BaseRespone) objectSuccess;
                if (response.result_code.equals("0")) {
                    String style = ((vn.mobiapps.pruf.view.activity.ForgotPassActivity) getActivity()).styleClick;
                    if (style.equals("ForgotUsername")) {
                        ForFotPassStep3 f = new ForFotPassStep3();
                        ((vn.mobiapps.pruf.view.activity.ForgotPassActivity) getActivity()).pushFragment(f);
                    } else {
                        InputOTPFragment f = new InputOTPFragment();
                        Bundle args = new Bundle();
                        args.putSerializable("isCreatePin", false);
                        args.putSerializable("isRetrieve", true);
                        args.putSerializable("cuid", ((vn.mobiapps.pruf.view.activity.ForgotPassActivity) getActivity()).cuid);
                        f.setArguments(args);
                        ((vn.mobiapps.pruf.view.activity.ForgotPassActivity) getActivity()).pushFragment(f);
                    }
                } else {
                    showMessage(response.result_description);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorMethodSend(Object objectError) {
        super.onError(objectError);
    }

    private Boolean checkCMND(String cmnd) {
        if (cmnd != null && !cmnd.equals("")) {
            if (cmnd.length() == 9 || cmnd.length() == 12) {
                return  true;
            }
        }
        return false;
    }

    private void showButton() {
        lyInput.setVisibility(View.INVISIBLE);
        layoutShowButton.setVisibility(View.VISIBLE);
    }

    private void hideButton() {
        lyInput.setVisibility(View.VISIBLE);
        layoutShowButton.setVisibility(View.GONE);
    }
}
