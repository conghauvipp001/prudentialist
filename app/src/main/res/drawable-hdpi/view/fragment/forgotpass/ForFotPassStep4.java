package vn.mobiapps.pruf.view.fragment.forgotpass;

import android.content.Intent;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.model.model_data.BaseRespone;
import vn.mobiapps.pruf.model.model_data.forgotpass.RetrievePassword;
import vn.mobiapps.pruf.model.model_data.forgotpass.RetrieveUsername;
import vn.mobiapps.pruf.model.model_view.ChatBoxModel;
import vn.mobiapps.pruf.presenter.forgotpass.ForgotPassPresenterImpl;
import vn.mobiapps.pruf.presenter.forgotpass.IForgotPassPresenter;
import vn.mobiapps.pruf.utils.Constant;
import vn.mobiapps.pruf.utils.Utils;
import vn.mobiapps.pruf.view.activity.LoginActivity;
import vn.mobiapps.pruf.view.base.BaseFragment;
import vn.mobiapps.pruf.widgets.ListViewChatBox;

/**
 * Created by Truong Thien on 12/24/2018.
 */

public class ForFotPassStep4 extends BaseFragment implements IForgotpassView {

    private ListViewChatBox lvChatBox;
    private TextView txtNext;
    private LinearLayout layoutInput;
    private EditText edtValues;
    private ImageView imgSend;
    private ArrayList<ChatBoxModel> listChat;
    private int index = 0;
    private Boolean checkEdit = false;
    private IForgotPassPresenter presenter;

    @Override
    protected int resLayout() {
        return R.layout.fragment_forgotpass_step4;
    }

    @Override
    protected void createFragment() {
        if (presenter == null) {
            presenter = new ForgotPassPresenterImpl(this);
        }
    }

    @Override
    protected void setupFragment(View view) {
        lvChatBox = view.findViewById(R.id.lvChatBox);
        txtNext = view.findViewById(R.id.txtNext);
        layoutInput = view.findViewById(R.id.layoutInput);
        edtValues = view.findViewById(R.id.edtValues);
        imgSend = view.findViewById(R.id.imgSend);
        addListChat();
    }

    @Override
    protected void setOnClickListenerFragment() {
        txtNext.setOnClickListener(this);
        imgSend.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.txtNext:
                String style = ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).styleClick;
                if (style.equals("ForgotUsername")) {
                    retrieveUsername();
                } else {
                    if (listChat.get(1).content.equals(listChat.get(3).content)) {
                        retrievePassword();
                    } else {
                        showMessage(getString(R.string.txt_not_match_pass));
                    }
                }
                break;
            case R.id.imgSend:
                clickSend();
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).showSupport();
    }

    private void clickSend() {
        if (checkValidate(edtValues)) {
            if (listChat.get(index - 1).type.equalsIgnoreCase("NewPassword")) {
                if (listChat.size() > 1) {
                    listChat.remove(1);
                }
                if (Utils.validatePassword(edtValues.getText().toString())) {
                    listChat.add(1, new ChatBoxModel(edtValues.getText().toString(), "", true, Constant.EDT_PASSWORD, false));
                    if (listChat.size() == 2) {
                        listChat.add(2, new ChatBoxModel(getString(R.string.txt_veri_password), "", false, "VerifyPassword", false));
                        ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).setContent(getString(R.string.txt_enter_pass));
                    }
                    index = 3;
                    edtValues.setText("");
                } else {
                    listChat.add(1, new ChatBoxModel(edtValues.getText().toString(), getString(R.string.txt_pass_fail), true, Constant.EDT_PASSWORD, false));
                }
            } else if (listChat.get(index - 1).type.equalsIgnoreCase("VerifyPassword")) {
                if (listChat.size() > 3) {
                    listChat.remove(3);
                }
                if (checkVerify()) {
                    hideKeyboard();
                    listChat.add(3, new ChatBoxModel(edtValues.getText().toString(), "", true, Constant.EDT_PASSWORD, false));
                    index = 4;
                } else {
                    listChat.add(3, new ChatBoxModel(edtValues.getText().toString(), getString(R.string.txt_Password_not_match), true, Constant.EDT_PASSWORD, false));
                }
            }
        }
        if (listChat.size() == 4) {
            ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).setContent(getString(R.string.txt_enter_pass));
            hideKeyboard();
            layoutInput.setVisibility(View.GONE);
            txtNext.setVisibility(View.VISIBLE);
        }
        edtValues.setText("");
        lvChatBox.updateAdapter();
        lvChatBox.setSelection(index - 1);
    }

    private void addListChat() {
        try {
            listChat = new ArrayList<ChatBoxModel>();
            listChat.add(new ChatBoxModel(getString(R.string.txt_new_password), "", false, "NewPassword", false));
            index = 1;
            lvChatBox.setData(getActivity(), listChat, this);
            ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).setContent(getString(R.string.txt_enter_newpass));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onClickEdit(int posision) {
        try {
            if (listChat.get(posision -1).type.equalsIgnoreCase("NewPassword")) {
                ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).setContent(getString(R.string.txt_enter_newpass));
                edtValues.setText(listChat.get(posision).content);
                edtValues.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            } else if (listChat.get(posision -1).type.equalsIgnoreCase("VerifyPassword")) {
                ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).setContent(getString(R.string.txt_enter_pass));
                edtValues.setText(listChat.get(posision).content);
                edtValues.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            }
            index = posision;
            edtValues.setFocusableInTouchMode(true);
            layoutInput.setVisibility(View.VISIBLE);
            txtNext.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Boolean checkValidate(EditText edtValues) {
        if (edtValues == null || edtValues.getText().toString().equals("")) {
            showMessage(getString(R.string.txt_not_null));
            return false;
        }
        return true;
    }

    private Boolean checkVerify() {
        if (!listChat.get(1).content.equalsIgnoreCase(edtValues.getText().toString())) {
            return false;
        }
        return true;
    }

    @Override
    public void checkRegisterCondition() {

    }

    @Override
    public void retrievePassword() {
        try {
            CheckNetworkRequest((vn.mobiapps.pruf.view.base.BaseActivity) getActivity(), new CheckNetworkListener() {
                @Override
                public void Connected() {
                    RetrievePassword password = new RetrievePassword();
                    password.cuid = ((vn.mobiapps.pruf.view.activity.ForgotPassActivity) getActivity()).cuid;
                    password.newPassword = listChat.get(3).content;
                    presenter.retrievePassword(password);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccess(Object objectSuccess) {
        try {
            super.onSuccess(objectSuccess);
            BaseRespone reponse = (BaseRespone) objectSuccess;
            if (reponse.result_code.equals("0")) {
                Toast.makeText(getContext(), R.string.txtChangePass, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            } else {
                showMessage(reponse.result_description);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Object objectError) {
        super.onError(objectError);
    }

    @Override
    public void retrieveUsername() {
        try {
            CheckNetworkRequest((vn.mobiapps.pruf.view.base.BaseActivity) getActivity(), new CheckNetworkListener() {
                @Override
                public void Connected() {
                    RetrieveUsername username = new RetrieveUsername();
                    username.cuid = ((vn.mobiapps.pruf.view.activity.ForgotPassActivity) getActivity()).cuid;
                    presenter.retrieveUsername(username);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void methodSend(String methodSend) {

    }

    @Override
    public void onSuccessMethodSend(Object objectSuccess) {

    }

    @Override
    public void onErrorMethodSend(Object objectError) {

    }
}
