package vn.mobiapps.pruf.view.fragment.changepass;

/**
 * Created by Truong Thien on 3/11/2019.
 */

public interface IChangePassView extends vn.mobiapps.pruf.view.base.IBaseView {
    void changePass();
}
