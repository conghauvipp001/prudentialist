package vn.mobiapps.pruf.view.fragment.changepass;

import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.model.model_data.BaseRespone;
import vn.mobiapps.pruf.model.model_data.changepass.ChangePassRequest;
import vn.mobiapps.pruf.presenter.changepass.ChangePassPresenterImpl;
import vn.mobiapps.pruf.presenter.changepass.IChangePassPresenter;
import vn.mobiapps.pruf.utils.Utils;
import vn.mobiapps.pruf.view.base.BaseFragment;
import vn.mobiapps.pruf.widgets.custombutton.CircularProgressButton;

/**
 * Created by Truong Thien on 3/11/2019.
 */

public class ChangePassFragment extends BaseFragment implements vn.mobiapps.pruf.view.fragment.changepass.IChangePassView {

    private EditText edtCurrentPass, edtNewPass, edtVerifyPass;
    private CircularProgressButton btnConfirmPass;
    private ImageView imgBack;
    private TextView txtCurrentPassFail, txtNewPassFail, txtVerifyPassFail;
    private IChangePassPresenter presenter;

    @Override
    protected int resLayout() {
        return R.layout.fragment_change_pass;
    }

    @Override
    protected void createFragment() {
        if (presenter == null) {
            presenter = new ChangePassPresenterImpl(this);
        }
    }

    @Override
    protected void setupFragment(View view) {
        edtCurrentPass = view.findViewById(R.id.edtCurrentPass);
        edtNewPass = view.findViewById(R.id.edtNewPass);
        edtVerifyPass = view.findViewById(R.id.edtVerifyPass);
        btnConfirmPass = view.findViewById(R.id.btnConfirmPass);
        txtCurrentPassFail = view.findViewById(R.id.txtCurrentPassFail);
        txtNewPassFail = view.findViewById(R.id.txtNewPassFail);
        txtVerifyPassFail = view.findViewById(R.id.txtVerifyPassFail);
        imgBack = view.findViewById(R.id.imgBack);
    }

    @Override
    protected void setOnClickListenerFragment() {
        btnConfirmPass.setOnClickListener(this);
        imgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.imgBack:
                hideKeyboard();
                ((vn.mobiapps.pruf.view.activity.ChangePassActivity)getActivity()).onBackPressed();
                break;
            case R.id.btnConfirmPass:
                hideKeyboard();
                if (btnConfirmPass.getProgress() == 0) {
                    if (checkValidate()) {
                        changePass();
                    }
                } else {
                    btnConfirmPass.setProgress(0);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void changePass() {
        CheckNetworkRequest((vn.mobiapps.pruf.view.base.BaseActivity) getActivity(), new CheckNetworkListener() {
            @Override
            public void Connected() {
                ChangePassRequest request = new ChangePassRequest();
                request.newPassword = edtNewPass.getText().toString();
                request.oldPassword = edtCurrentPass.getText().toString();
                presenter.changePass(request);
            }
        });
    }

    @Override
    public void onSuccess(Object objectSuccess) {
        super.onSuccess(objectSuccess);
        try {
            if (objectSuccess != null) {
                BaseRespone respone = (BaseRespone) objectSuccess;
                if (respone.result_code.equals("0")) {
                    simulateSuccessProgress(btnConfirmPass);
                } else {
                    simulateErrorProgress(btnConfirmPass);
                    showMessage(respone.result_description);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Object objectError) {
        super.onError(objectError);
    }

    private Boolean checkValidate() {
        Boolean checkCurrentPass = true, checkNewPass = true, checkVerifyPass = true, checkVeri;

        checkCurrentPass = edtCurrentPass.getText().toString().isEmpty() || !Utils.validatePassword(edtCurrentPass.getText().toString());
        checkNewPass = edtNewPass.getText().toString().isEmpty() || !Utils.validatePassword(edtNewPass.getText().toString());
        checkVerifyPass = edtVerifyPass.getText().toString().isEmpty() || !Utils.validatePassword(edtVerifyPass.getText().toString());
        if (edtNewPass.getText().toString().equals(edtVerifyPass.getText().toString())) {
            checkVeri = false;
        } else {
            checkVeri = true;
        }

        if (checkCurrentPass || checkNewPass || checkVerifyPass || checkVeri) {
            if (checkCurrentPass) {
                txtCurrentPassFail.setVisibility(View.VISIBLE);
                txtCurrentPassFail.setText(R.string.txt_not_pass_);
            } else {
                txtCurrentPassFail.setVisibility(View.INVISIBLE);
            }
            if (checkNewPass) {
                txtNewPassFail.setVisibility(View.VISIBLE);
                txtNewPassFail.setText(getString(R.string.txt_not_pass_));
            } else {
                txtNewPassFail.setVisibility(View.INVISIBLE);
            }
            if (checkVerifyPass) {
                txtVerifyPassFail.setVisibility(View.VISIBLE);
                txtVerifyPassFail.setText(getString(R.string.txt_not_pass_));
            } else {
                if (checkVeri) {
                    txtVerifyPassFail.setVisibility(View.VISIBLE);
                    txtVerifyPassFail.setText(R.string.txt_not_mat);
                    return false;
                } else {
                    txtVerifyPassFail.setVisibility(View.INVISIBLE);
                }
            }
            return false;
        }
        txtVerifyPassFail.setVisibility(View.INVISIBLE);
        return true;
    }

    private void simulateSuccessProgress(final CircularProgressButton button) {
        ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 100);
        widthAnimation.setDuration(1500);
        widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                button.setProgress(value);
            }
        });
        widthAnimation.start();
    }

    private void simulateErrorProgress(final CircularProgressButton button) {
        ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 99);
        widthAnimation.setDuration(1500);
        widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                button.setProgress(value);
                if (value == 99) {
                    button.setProgress(-1);
                }
            }
        });
        widthAnimation.start();
    }
}
