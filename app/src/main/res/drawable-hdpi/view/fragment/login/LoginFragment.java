package vn.mobiapps.pruf.view.fragment.login;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.VideoView;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.apimanager.APIManager;
import vn.mobiapps.pruf.model.model_data.backgroud.BackGroud;
import vn.mobiapps.pruf.model.model_data.login.LoginRequest;
import vn.mobiapps.pruf.model.model_data.login.ResponeLogin;
import vn.mobiapps.pruf.model.model_view.ResponseErrorModel;
import vn.mobiapps.pruf.presenter.login.ILoginPresenter;
import vn.mobiapps.pruf.presenter.login.LoginPresenterImpl;
import vn.mobiapps.pruf.utils.ShareReference;
import vn.mobiapps.pruf.view.activity.CustomerPageActivity;
import vn.mobiapps.pruf.view.activity.LocationActivity;
import vn.mobiapps.pruf.view.activity.RegisterLoanActivity;
import vn.mobiapps.pruf.view.base.BaseFragment;

import static android.content.Context.MODE_PRIVATE;
import static vn.mobiapps.pruf.utils.ShareReference.MY_PREFS_NAME;

/**
 * Created by Truong Thien on 12/13/2018.
 */

public class LoginFragment extends BaseFragment implements ILoginFragmentView {
    public static final String TAG = vn.mobiapps.pruf.view.fragment.login.LoginFragment.class.getSimpleName();

    private TextView txtRegister, txtForgotPassword;
    private Button btn_login;
    private RelativeLayout layoutLogin;
    private EditText edtUsername, edtPassword;
    private TextView tvFailEmail, tvFailPass;
    private VideoView videoView;
    private LinearLayout layoutOffice, layoutContact, layoutFaq;
    private ILoginPresenter presenter;
    private Switch switch_lang;

    @Override
    protected int resLayout() {
        return R.layout.fragment_login;
    }

    @Override
    protected void createFragment() {
        if (presenter == null) {
            presenter = new LoginPresenterImpl(this);
        }
    }

    @Override
    protected void setupFragment(View view) {
        txtRegister = view.findViewById(R.id.txtRegister);
        txtForgotPassword = view.findViewById(R.id.txtForgotPassword);
        btn_login = view.findViewById(R.id.btn_login);
        layoutLogin = view.findViewById(R.id.layoutLogin);
        tvFailEmail = view.findViewById(R.id.tvFailEmail);
        tvFailPass = view.findViewById(R.id.tvFailPassword);
        layoutOffice = view.findViewById(R.id.layoutOffice);
        layoutContact = view.findViewById(R.id.layoutContact);
        layoutFaq = view.findViewById(R.id.layoutFaq);
        edtUsername = view.findViewById(R.id.edtUsername);
        edtPassword = view.findViewById(R.id.edtPassword);
        videoView = view.findViewById(R.id.videoView);
        //switch_lang = view.findViewById(R.id.switch_lang);
        getBackground();

        SharedPreferences preferences = getActivity().getSharedPreferences("Settings", Activity.MODE_PRIVATE);
        String lang = preferences.getString("My_lang", "");
        /*if (lang.equals("vi")) {
            switch_lang.setChecked(false);
        } else if (lang.equals("en")) {
            switch_lang.setChecked(true);
        }*/
    }

    private void setBackGroudVideo(String url) {
        try {
            videoView.setVideoURI(Uri.parse(url));
            videoView.start();
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mp) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            videoView.setBackgroundDrawable(null);
                        }
                    }, 100);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        videoView.start();
    }

    @Override
    protected void setOnClickListenerFragment() {
        txtRegister.setOnClickListener(this);
        txtForgotPassword.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        layoutLogin.setOnClickListener(this);
        layoutOffice.setOnClickListener(this);
        layoutContact.setOnClickListener(this);
        layoutFaq.setOnClickListener(this);

       /* switch_lang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switch_lang.isChecked()) {
                    setLocale("en");
                } else {
                    setLocale("vi");
                }
                hideKeyboard();
                //hideLanguage();
                goToLogin();
            }
        });*/
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.txtForgotPassword: {
                hideKeyboard();
                Intent intent = new Intent(getActivity(), vn.mobiapps.pruf.view.activity.ForgotPassActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
            break;
            case R.id.txtRegister: {
                hideKeyboard();
                Intent intent = new Intent(getActivity(), RegisterLoanActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
            break;
            case R.id.btn_login: {
                hideKeyboard();
                if (checkValidate()) {
                    login();
                }
            }
            break;
            case R.id.layoutLogin:
                hideKeyboard();
                break;
            case R.id.layoutOffice: {
                hideKeyboard();
                Intent intent = new Intent(getActivity(), LocationActivity.class);
                startActivity(intent);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.pop_enter_menu, R.anim.pop_exit_menu);
            }
                break;
            case R.id.layoutContact: {
                hideKeyboard();
                Intent intent = new Intent(getActivity(), vn.mobiapps.pruf.view.activity.ContactActivity.class);
                startActivity(intent);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.pop_enter_menu, R.anim.pop_exit_menu);
            }
            break;
            case R.id.layoutFaq: {
                hideKeyboard();
                Intent intent = new Intent(getActivity(), vn.mobiapps.pruf.view.activity.FAQActivity.class);
                startActivity(intent);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.pop_enter_menu, R.anim.pop_exit_menu);
            }
            break;
            default:
                break;
        }
    }

    private Boolean checkValidate() {
        if (edtUsername.getText().toString().isEmpty() || edtPassword.getText().toString().isEmpty()) {
            if (edtUsername.getText().toString().isEmpty()) {
                tvFailEmail.setVisibility(View.VISIBLE);
                tvFailEmail.setText(R.string.txt_user);
            } else {
                tvFailEmail.setVisibility(View.GONE);
            }
            if (edtPassword.getText().toString().isEmpty()) {
                tvFailPass.setVisibility(View.VISIBLE);
                tvFailPass.setText(R.string.txt_pas);
            } else {
                tvFailPass.setVisibility(View.GONE);
            }
            return false;
        } else {
            tvFailEmail.setVisibility(View.GONE);
            tvFailPass.setVisibility(View.GONE);
        }
        return true;
    }

    /*Call Api Login*/
    @Override
    public void login() {
        try {
            final String deviceId = Settings.Secure.getString(getActivity().getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
            //final String deviceId = "2121dđsd2d1đ21ds2sd1";
            final String deviceName = android.os.Build.MODEL;
            APIManager.deviceID = deviceId;
            CheckNetworkRequest((vn.mobiapps.pruf.view.base.BaseActivity) getActivity(), new CheckNetworkListener() {
                @Override
                public void Connected() {
                    presenter.login(new LoginRequest(edtUsername.getText().toString(), edtPassword.getText().toString(), deviceId, deviceName));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccess(Object objectSuccess) {
        try {
            if (objectSuccess != null) {
                ResponeLogin logndata = (ResponeLogin) objectSuccess;
                if (logndata.data != null) {
                    APIManager.mAccessToken = logndata.data.accessToken;
                    // save loginted
                    SharedPreferences.Editor editor = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putBoolean("isLogin", true);
                    editor.putString("Username", edtUsername.getText().toString().trim());
                    editor.apply();
                    ShareReference.isLogin = true;
                    if (logndata.result_code.equals("0")) {
                        if (Boolean.valueOf(logndata.data.verifyOTP) || Boolean.valueOf(logndata.data.createPin)) {
                            goToHome(logndata);
                        } else {
                            Intent intent = new Intent(getActivity(), CustomerPageActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }
                } else {
                    if (logndata.result_code.equals("-1")){
                        if (logndata.result_description.equals("Password failed")) {
                            tvFailPass.setVisibility(View.VISIBLE);
                            tvFailPass.setText(logndata.result_description);
                        } else {
                            tvFailPass.setVisibility(View.GONE);
                        }

                        if (logndata.result_description.equals("Username incorrect")) {
                            tvFailEmail.setVisibility(View.VISIBLE);
                            tvFailEmail.setText(logndata.result_description);
                        } else {
                            tvFailEmail.setVisibility(View.GONE);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Object objectError) {
        ResponseErrorModel onError = (ResponseErrorModel) objectError;
        if (onError.getResult_description() != null) {
            showMessage(onError.getResult_description());
        }
    }

    private void goToHome(ResponeLogin logndata) {
        Intent intent = new Intent(getActivity(), vn.mobiapps.pruf.view.activity.HomeActivity.class);
        intent.putExtra("LognData", logndata);
        intent.putExtra("isRetrieve", false);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void getBackground() {
        CheckNetworkRequest((vn.mobiapps.pruf.view.base.BaseActivity) getActivity(), new CheckNetworkListener() {
            @Override
            public void Connected() {
                presenter.getBackground();
            }
        });
    }

    @Override
    public void onSuccessGetBackground(Object user) {
        try {
            if (user != null) {
                BackGroud groud = (BackGroud) user;
                if (groud.data != null) {
                    if (groud.data.url != null && !groud.data.url.equals("")) {
                        setBackGroudVideo(groud.data.url);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorGetBackground(Object loginError) {
        super.onError(loginError);
    }
}
