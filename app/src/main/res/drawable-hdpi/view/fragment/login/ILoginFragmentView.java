package vn.mobiapps.pruf.view.fragment.login;


/**
 * Created by Truong Thien on 11/5/2018.
 */

public interface ILoginFragmentView<T> extends vn.mobiapps.pruf.view.base.IBaseView {
    void login();
    void getBackground();

    void onSuccessGetBackground(T user);
    void onErrorGetBackground(T loginError);
}
