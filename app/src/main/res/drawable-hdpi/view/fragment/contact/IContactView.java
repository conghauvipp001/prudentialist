package vn.mobiapps.pruf.view.fragment.contact;

import vn.mobiapps.pruf.view.base.IBaseView;

/**
 * Created by Truong Thien on 1/28/2019.
 */

public interface IContactView extends IBaseView {
    void getCommunication();
}
