package vn.mobiapps.pruf.view.fragment.contact;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Handler;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;

import java.util.ArrayList;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.model.model_data.contact.Contact;
import vn.mobiapps.pruf.presenter.contact.ContactPresenterImpl;
import vn.mobiapps.pruf.presenter.contact.IContactPresenter;
import vn.mobiapps.pruf.view.activity.ContactActivity;
import vn.mobiapps.pruf.view.base.BaseActivity;
import vn.mobiapps.pruf.view.base.BaseFragment;
import vn.mobiapps.pruf.widgets.ripplebackground.RippleBackground;

/**
 * Created by Truong Thien on 12/25/2018.
 */

public class ContactFragment extends BaseFragment implements IContactView {
    private ImageView imgBack, imgCenter, imgZalo, imgFaceBook, imgMail, imgPhone;
    private RippleBackground bg_Content;
    private IContactPresenter presenter;

    @Override
    protected int resLayout() {
        return R.layout.fragment_contact;
    }

    @Override
    protected void createFragment() {
        if (presenter == null) {
            presenter = new ContactPresenterImpl(this);
        }
    }

    @Override
    protected void setupFragment(View view) {
        imgBack = view.findViewById(R.id.imgBack);
        bg_Content = view.findViewById(R.id.bg_Content);
        imgCenter = view.findViewById(R.id.imgCenter);
        imgZalo = view.findViewById(R.id.imgZalo);
        imgFaceBook = view.findViewById(R.id.imgFaceBook);
        imgMail = view.findViewById(R.id.imgMail);
        imgPhone = view.findViewById(R.id.imgPhone);
        startAnimation();
        getCommunication();
    }

    @Override
    protected void setOnClickListenerFragment() {
        imgBack.setOnClickListener(this);
    }

    private void startAnimation() {
        final Handler handler = new Handler();
        bg_Content.startRippleAnimation();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showZalo();
                showFaceBook();
                showMail();
                showPhone();
            }
        }, 3000);
    }


    private void showZalo() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        ArrayList<Animator> animatorList = new ArrayList<Animator>();
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(imgZalo, "ScaleX", 0f, 1.2f, 1f);
        animatorList.add(scaleXAnimator);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(imgZalo, "ScaleY", 0f, 1.2f, 1f);
        animatorList.add(scaleYAnimator);
        animatorSet.playTogether(animatorList);
        imgZalo.setVisibility(View.VISIBLE);
        animatorSet.start();
    }

    private void showFaceBook() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        ArrayList<Animator> animatorList = new ArrayList<Animator>();
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(imgFaceBook, "ScaleX", 0f, 1.2f, 1f);
        animatorList.add(scaleXAnimator);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(imgFaceBook, "ScaleY", 0f, 1.2f, 1f);
        animatorList.add(scaleYAnimator);
        animatorSet.playTogether(animatorList);
        imgFaceBook.setVisibility(View.VISIBLE);
        animatorSet.start();
    }

    private void showMail() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        ArrayList<Animator> animatorList = new ArrayList<Animator>();
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(imgMail, "ScaleX", 0f, 1.2f, 1f);
        animatorList.add(scaleXAnimator);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(imgMail, "ScaleY", 0f, 1.2f, 1f);
        animatorList.add(scaleYAnimator);
        animatorSet.playTogether(animatorList);
        imgMail.setVisibility(View.VISIBLE);
        animatorSet.start();
    }

    private void showPhone() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        ArrayList<Animator> animatorList = new ArrayList<Animator>();
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(imgPhone, "ScaleX", 0f, 1.2f, 1f);
        animatorList.add(scaleXAnimator);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(imgPhone, "ScaleY", 0f, 1.2f, 1f);
        animatorList.add(scaleYAnimator);
        animatorSet.playTogether(animatorList);
        imgPhone.setVisibility(View.VISIBLE);
        animatorSet.start();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            /*case R.id.txtCallPhone:
                Utils.CallPhones(getContext(), txt_hotline.getText().toString());
                break;
            case R.id.txtSendMail:
                Utils.sendEmail(getContext(), txtemail.getText().toString());
                break;
            case R.id.txtChatZalo:
                if (phoneNumber != null) {
                    Utils.openZaloId(getContext(), phoneNumber);
                }
                break;*/
            case R.id.imgBack:
                ((ContactActivity) getActivity()).onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    public void getCommunication() {
        CheckNetworkRequest((BaseActivity) getActivity(), new CheckNetworkListener() {
            @Override
            public void Connected() {
                presenter.getCommunication();
            }
        });
    }

    @Override
    public void onSuccess(Object objectSuccess) {
        updateUI(objectSuccess);
    }

    @Override
    public void onError(Object objectError) {
        super.onError(objectError);
    }

    @Override
    public void updateUI(Object object) {
        super.updateUI(object);
        try {
            Contact contact = (Contact) object;
            /*if (contact.data.hotline != null) {
                txt_hotline.setText(contact.data.hotline);
            }
            if (contact.data.email != null) {
                txtemail.setText(contact.data.email);
            }
            if (contact.data.zaloName != null) {
                txt_NameZalo.setText("Zalo: " + contact.data.zaloName);
            }
            if (contact.data.phoneNumberZalo != null) {
                phoneNumber = contact.data.phoneNumberZalo;
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
