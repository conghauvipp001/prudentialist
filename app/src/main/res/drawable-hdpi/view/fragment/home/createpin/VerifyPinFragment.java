package vn.mobiapps.pruf.view.fragment.home.createpin;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.apimanager.APIManager;
import vn.mobiapps.pruf.model.model_data.home.CreatePin;
import vn.mobiapps.pruf.presenter.home.HomePresenterImpl;
import vn.mobiapps.pruf.presenter.home.IHomePresenter;
import vn.mobiapps.pruf.view.activity.HomeActivity;
import vn.mobiapps.pruf.view.base.BaseFragment;
import vn.mobiapps.pruf.widgets.PinEntryEditText;

/**
 * Created by Truong Thien on 12/26/2018.
 */

public class VerifyPinFragment extends BaseFragment implements vn.mobiapps.pruf.view.fragment.home.IHomeView {

    private PinEntryEditText txtPin;
    private TextView txtFailPin;
    private ImageView imgBack;
    private String pin;
    private IHomePresenter presenter;

    @Override
    protected int resLayout() {
        return R.layout.fragment_verify_pin;
    }

    @Override
    protected void createFragment() {
        pin = getArguments().getString("Pin");
        if (presenter == null) {
            presenter = new HomePresenterImpl(this);
        }
    }

    @Override
    protected void setupFragment(View view) {
        txtPin = view.findViewById(R.id.txtPin);
        txtFailPin = view.findViewById(R.id.txtFailPin);
        imgBack = view.findViewById(R.id.imgBack);
        ((HomeActivity)getActivity()).showSupport();
        ((HomeActivity)getActivity()).setMarginBottom(700);
        ((HomeActivity)getActivity()).setContent("Nhập lại mã PIN để xác nhận");
    }

    @Override
    protected void setOnClickListenerFragment() {
        imgBack.setOnClickListener(this);
        txtPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (txtPin.getText().length() == 6) {
                    hideKeyboard();
                    if (checkValidate()) {
                        CreatePin pin = new CreatePin();
                        pin.accessToken = APIManager.mAccessToken;
                        pin.newPin = txtPin.getText().toString();
                        createPin(pin);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.imgBack:
                hideKeyboard();
                ((HomeActivity)getActivity()).onBackPressed();
            default:
                break;
        }
    }

    private Boolean checkValidate() {
        if (pin != null) {
            if (pin.equalsIgnoreCase(txtPin.getText().toString())) {
                return true;
            } else {
                showMessage(getString(R.string.txt_not_veri));
                return false;
            }
        }
        return false;
    }

    @Override
    public void createPin(final CreatePin pinCode) {
        CheckNetworkRequest((vn.mobiapps.pruf.view.base.BaseActivity) getActivity(), new CheckNetworkListener() {
            @Override
            public void Connected() {
                presenter.createPin(pinCode);
            }
        });
    }

    @Override
    public void onSuccess(Object objectSuccess) {
        super.onSuccess(objectSuccess);
        try {
            CreatePin pin = (CreatePin) objectSuccess;
            if (pin.result_code.equals("0")) {
                txtFailPin.setText("");
                vn.mobiapps.pruf.view.fragment.home.createpin.CreatePinDoneFragment f = new vn.mobiapps.pruf.view.fragment.home.createpin.CreatePinDoneFragment();
                ((HomeActivity) getActivity()).pushFragment(f);
            } else {
                txtFailPin.setText(pin.result_description);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Object objectError) {
        super.onError(objectError);
    }

    @Override
    public void oTPGeneration(Boolean isRetrieve) {

    }

    @Override
    public void oTPVerification(Boolean isRetrieve) {

    }
}
