package vn.mobiapps.pruf.view.fragment.home;

import android.view.View;
import android.widget.Button;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.utils.Utils;
import vn.mobiapps.pruf.view.base.BaseFragment;

/**
 * Created by Truong Thien on 2/28/2019.
 */

public class Not extends BaseFragment {
    Button btnCall;
    @Override
    protected int resLayout() {
        return R.layout.fragment_not_values;
    }

    @Override
    protected void createFragment() {

    }

    @Override
    protected void setupFragment(View view) {
        btnCall = view.findViewById(R.id.btnCall);
    }

    @Override
    protected void setOnClickListenerFragment() {
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.CallPhones(getContext(), "1900545449");
            }
        });
    }
}
