package vn.mobiapps.pruf.view.fragment.home.otpreceiving;

/**
 * Created by Truong Thien on 3/12/2019.
 */

public interface IOTPReceivingView extends vn.mobiapps.pruf.view.base.IBaseView {
    void methodSend(String methodSend);
}
