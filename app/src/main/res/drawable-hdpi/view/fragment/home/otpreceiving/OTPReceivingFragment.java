package vn.mobiapps.pruf.view.fragment.home.otpreceiving;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.model.model_data.home.MethodSend;
import vn.mobiapps.pruf.model.model_data.login.DataLogin;
import vn.mobiapps.pruf.presenter.home.otpreceiving.IOTPPresenter;
import vn.mobiapps.pruf.presenter.home.otpreceiving.OTPPresenterImpl;
import vn.mobiapps.pruf.view.activity.HomeActivity;
import vn.mobiapps.pruf.view.base.BaseActivity;
import vn.mobiapps.pruf.view.base.BaseFragment;
import vn.mobiapps.pruf.view.fragment.home.inputotp.InputOTPFragment;

/**
 * Created by Truong Thien on 3/12/2019.
 */

public class OTPReceivingFragment extends BaseFragment implements IOTPReceivingView {
    private Button btnPhone, btnEmail;
    private IOTPPresenter presenter;
    private DataLogin logndata;
    private ImageView imgBack;

    @Override
    protected int resLayout() {
        return R.layout.fragment_otp_receiving;
    }

    @Override
    protected void createFragment() {
        if (presenter == null) {
            presenter = new OTPPresenterImpl(this);
        }
        if (((HomeActivity) getActivity()).logndata != null && ((HomeActivity) getActivity()).logndata.data != null) {
            logndata = ((HomeActivity) getActivity()).logndata.data;
        }
    }

    @Override
    protected void setupFragment(View view) {
        btnPhone = view.findViewById(R.id.btnPhone);
        btnEmail = view.findViewById(R.id.btnEmail);
        imgBack = view.findViewById(R.id.imgBack);
        ((HomeActivity)getActivity()).showSupport();
        ((HomeActivity)getActivity()).setMarginBottom(400);
        ((HomeActivity)getActivity()).setContent("Chọn phương thức để nhận mã OTP");
        updateUI();
    }

    @Override
    protected void setOnClickListenerFragment() {
        btnPhone.setOnClickListener(this);
        btnEmail.setOnClickListener(this);
        imgBack.setOnClickListener(this);
    }

    private void updateUI() {
        if (logndata.phoneNumber != null && logndata.phoneNumber.length() > 3) {
            String phone = logndata.phoneNumber.substring(logndata.phoneNumber.length() - 3, logndata.phoneNumber.length());
            btnPhone.setText("GỬI SMS ĐẾN *******" + phone);
        }
        if (logndata.email != null && logndata.email.length() > 8) {
            String mail = logndata.email.substring(0,5) + "***" + logndata.email.substring(logndata.email.indexOf("@"), logndata.email.indexOf("@") + 2) + "***";
            btnEmail.setText("GỬI EMAIL ĐẾN " + mail);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.imgBack:
                ((HomeActivity)getActivity()).onBackPressed();
                break;
            case R.id.btnPhone:
                methodSend("SMS");
            break;
            case R.id.btnEmail:
                methodSend("MAIL");
            break;
            default:
                break;
        }
    }

    @Override
    public void methodSend(final String methodSend) {
        CheckNetworkRequest((BaseActivity) getActivity(), new CheckNetworkListener() {
            @Override
            public void Connected() {
                if (logndata.cuid != null) {
                    presenter.methodSend(new MethodSend(logndata.cuid, methodSend));
                }
            }
        });
    }

    @Override
    public void onSuccess(Object objectSuccess) {
        super.onSuccess(objectSuccess);
        InputOTPFragment f = new InputOTPFragment();
        ((HomeActivity)getActivity()).pushFragment(f);
    }

    @Override
    public void onError(Object objectError) {
        super.onError(objectError);
    }


}
