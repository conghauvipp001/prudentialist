package vn.mobiapps.pruf.view.fragment.home.createpin;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.view.base.BaseFragment;
import vn.mobiapps.pruf.widgets.PinEntryEditText;

/**
 * Created by Truong Thien on 12/26/2018.
 */

public class SetupPinFragment extends BaseFragment {

    private PinEntryEditText txtPin;
    private ImageView imgBack;

    @Override
    protected int resLayout() {
        return R.layout.fragment_setup_pin;
    }

    @Override
    protected void createFragment() {

    }

    @Override
    protected void setupFragment(View view) {
        txtPin = view.findViewById(R.id.txtPin);
        imgBack = view.findViewById(R.id.imgBack);
        ((vn.mobiapps.pruf.view.activity.HomeActivity)getActivity()).showSupport();
        ((vn.mobiapps.pruf.view.activity.HomeActivity)getActivity()).setMarginBottom(700);
        ((vn.mobiapps.pruf.view.activity.HomeActivity)getActivity()).setContent("Nhập mã PIN ở đây");
    }

    @Override
    protected void setOnClickListenerFragment() {
        imgBack.setOnClickListener(this);
        txtPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (txtPin.getText().length() == 6) {
                    if (checkValidate()) {
                        vn.mobiapps.pruf.view.fragment.home.createpin.VerifyPinFragment f = new vn.mobiapps.pruf.view.fragment.home.createpin.VerifyPinFragment();
                        Bundle args = new Bundle();
                        args.putString("Pin", txtPin.getText().toString());
                        f.setArguments(args);
                        ((vn.mobiapps.pruf.view.activity.HomeActivity)getActivity()).pushFragment(f);
                        txtPin.setText("");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.imgBack:
                hideKeyboard();
                ((vn.mobiapps.pruf.view.activity.HomeActivity)getActivity()).onBackPressed();
                break;
            default:
                break;
        }
    }

    private Boolean checkValidate() {
        if (txtPin.getText().toString().isEmpty() || txtPin.getText().toString().length() < 4) {
            showMessage(getString(R.string.txt_not_pin));
            return false;
        }
        return true;
    }
}
