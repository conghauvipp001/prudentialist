package vn.mobiapps.pruf.view.fragment.home.inputotp;

import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.apimanager.APIManager;
import vn.mobiapps.pruf.model.model_data.home.CreatePin;
import vn.mobiapps.pruf.model.model_data.home.OTPRequest;
import vn.mobiapps.pruf.model.model_data.home.ResponseOTP;
import vn.mobiapps.pruf.model.model_data.login.DataLogin;
import vn.mobiapps.pruf.presenter.home.HomePresenterImpl;
import vn.mobiapps.pruf.presenter.home.IHomePresenter;
import vn.mobiapps.pruf.view.activity.CustomerPageActivity;
import vn.mobiapps.pruf.view.base.BaseFragment;
import vn.mobiapps.pruf.view.fragment.home.createpin.SetupPinFragment;
import vn.mobiapps.pruf.widgets.PinEntryEditText;
import vn.mobiapps.pruf.widgets.dialog.DialogFactory;

/**
 * Created by Truong Thien on 12/26/2018.
 */

public class InputOTPFragment extends BaseFragment implements vn.mobiapps.pruf.view.fragment.home.IHomeView {

    private TextView txtSetTime, txtFailOTP;
    private ImageView imgBack;
    private Button txtResenOTP;
    private CountDownTimer cTimer = null;
    private ProgressBar mprogressBar;
    private int minuti = 1;
    private PinEntryEditText txtPin;
    private IHomePresenter presenter;
    private Boolean oTPVeri = false;
    private RelativeLayout layoutTime;
    private Boolean isRetrieve = false;
    private String cuid;
    private DataLogin dataLogin;

    @Override
    protected int resLayout() {
        return R.layout.fragment_input_otp;
    }

    @Override
    protected void createFragment() {
        if (presenter == null) {
            presenter = new HomePresenterImpl(this);
        }
        if (((vn.mobiapps.pruf.view.activity.HomeActivity)getActivity()).logndata != null && ((vn.mobiapps.pruf.view.activity.HomeActivity)getActivity()).logndata.data!= null) {
            dataLogin = ((vn.mobiapps.pruf.view.activity.HomeActivity)getActivity()).logndata.data;
        }
        isRetrieve = ((vn.mobiapps.pruf.view.activity.HomeActivity)getActivity()).isRetrieve;
        cuid = dataLogin.cuid;
    }

    @Override
    protected void setupFragment(View view) {
        txtPin = view.findViewById(R.id.txtPin);
        txtSetTime = view.findViewById(R.id.txtSetTime);
        txtFailOTP = view.findViewById(R.id.txtFailOTP);
        txtResenOTP = view.findViewById(R.id.txtResenOTP);
        mprogressBar = view.findViewById(R.id.mprogressBar);
        layoutTime = view.findViewById(R.id.layoutTime);
        imgBack = view.findViewById(R.id.imgBack);
        txtResenOTP.setClickable(true);
        ((vn.mobiapps.pruf.view.activity.HomeActivity)getActivity()).hideSupport();
        cancelTimer();
        setCountDownTimer();
    }

    @Override
    protected void setOnClickListenerFragment() {
        txtResenOTP.setOnClickListener(this);
        imgBack.setOnClickListener(this);

        txtPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (txtPin.getText().length() == 4) {
                    hideKeyboard();
                    if (checkValidate()) {
                        oTPVerification(isRetrieve);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.imgBack:
                hideKeyboard();
                ((vn.mobiapps.pruf.view.activity.HomeActivity)getActivity()).onBackPressed();
                break;
            case R.id.txtResenOTP:
                hideKeyboard();
                txtPin.setText("");
                layoutTime.setVisibility(View.VISIBLE);
                cancelTimer();
                setCountDownTimer();
                oTPGeneration(isRetrieve);
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof vn.mobiapps.pruf.view.activity.ForgotPassActivity) {
            ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).hideSupport();
        }
        txtPin.setText("");
        oTPGeneration(isRetrieve);
    }

    public void setCountDownTimer() {
        try {
            cTimer = new CountDownTimer(60 * minuti * 1000, 500) {
                @Override
                public void onTick(long leftTimeInMilliseconds) {
                    long seconds = leftTimeInMilliseconds / 1000;
                    mprogressBar.setProgress((int) seconds);
                    txtSetTime.setText(/*String.format("%02d", seconds / 60) + ":" +*/ String.format("%2d", seconds % 60) + "s");
                }
                @Override
                public void onFinish() {
                    txtSetTime.setText("0s");
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancelTimer() {
        if (cTimer != null)
            cTimer.cancel();
    }

    @Override
    public void oTPGeneration(final Boolean isRetrieve) {
        oTPVeri = false;
        CheckNetworkRequest((vn.mobiapps.pruf.view.base.BaseActivity) getActivity(), new CheckNetworkListener() {
            @Override
            public void Connected() {
                OTPRequest otpRequest = new OTPRequest();
                otpRequest.accessToken = APIManager.mAccessToken;
                if (cuid != null) {
                    otpRequest.cuid = cuid;
                }
                presenter.oTPGeneration(otpRequest, isRetrieve);
            }
        });
    }

    @Override
    public void onSuccess(Object objectSuccess) {
        try {
            final ResponseOTP responseOTP = (ResponseOTP) objectSuccess;
            if (!oTPVeri) {
                if (responseOTP.result_code.equals("0")) {
                    if (responseOTP.data != null && responseOTP.data.number_of_resend.equals("0")) {
                        txtResenOTP.setTextColor(getResources().getColor(R.color.color1));
                        txtResenOTP.setClickable(false);
                    } else {
                        txtResenOTP.setTextColor(getResources().getColor(R.color.nice_blue));
                        txtResenOTP.setClickable(true);
                    }
                }
                if (responseOTP.data != null && responseOTP.data.number_of_validate.equals("0")) {
                    txtPin.setFocusable(false);
                    txtPin.setEnabled(false);
                } else {
                    txtPin.setFocusable(true);
                    txtPin.setEnabled(true);
                }
                if (responseOTP.result_code.equals("0") && responseOTP.data.number_of_validate.equals("0")) {
                    vn.mobiapps.pruf.view.fragment.home.Not f = new vn.mobiapps.pruf.view.fragment.home.Not();
                    if (getActivity() instanceof vn.mobiapps.pruf.view.activity.HomeActivity) {
                        ((vn.mobiapps.pruf.view.activity.HomeActivity)getActivity()).pushFragment(f);
                    } else if (getActivity() instanceof vn.mobiapps.pruf.view.activity.ForgotPassActivity) {
                        ((vn.mobiapps.pruf.view.activity.ForgotPassActivity)getActivity()).pushFragment(f);
                    }
                }
            } else {
                if (responseOTP.result_code.equals("0")) {
                    txtPin.setText("");
                    txtFailOTP.setVisibility(View.INVISIBLE);
                    if (responseOTP.data != null && responseOTP.data.number_of_validate.equals("0")) {
                        //btnContinueOTP.setEnabled(false);
                        //btnContinueOTP.setBackgroundColor(getResources().getColor(R.color.color1));
                    } else {
                        //btnContinueOTP.setEnabled(true);
                        //btnContinueOTP.setBackgroundColor(getResources().getColor(R.color.nice_blue));
                        //txtTime.setText("");
                        DialogFactory.dialog_Message_OTP(getContext(), getString(R.string.txt_Otp_done), new DialogFactory.DialogListener.OTPListener() {
                            @Override
                            public void next() {
                                nextFlow();
                            }
                        });
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (DialogFactory.getDialog() != null) {
                                    DialogFactory.getDialog().dismiss();
                                }
                                nextFlow();
                            }
                        }, 5000);
                    }
                } else {
                    txtFailOTP.setVisibility(View.VISIBLE);
                    txtFailOTP.setText(responseOTP.result_description);
                    txtFailOTP.setTextColor(getResources().getColor(R.color.colorRed));
                    layoutTime.setVisibility(View.INVISIBLE);

                    /*SetupPinFragment f = new SetupPinFragment();
                    ((HomeActivity) getActivity()).pushFragment(f);*/
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void nextFlow() {
        try {
            if (isRetrieve) {
                vn.mobiapps.pruf.view.fragment.forgotpass.ForFotPassStep4 f = new vn.mobiapps.pruf.view.fragment.forgotpass.ForFotPassStep4();
                ((vn.mobiapps.pruf.view.activity.ForgotPassActivity) getActivity()).pushFragment(f);
            } else {
                if (Boolean.parseBoolean(dataLogin.createPin)) {
                    SetupPinFragment f = new SetupPinFragment();
                    ((vn.mobiapps.pruf.view.activity.HomeActivity) getActivity()).pushFragment(f);
                } else {
                    Intent intent = new Intent(getActivity(), CustomerPageActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Object objectError) {
        super.onError(objectError);
    }

    @Override
    public void oTPVerification(final Boolean isRetrieve) {
        oTPVeri = true;
        CheckNetworkRequest((vn.mobiapps.pruf.view.base.BaseActivity) getActivity(), new CheckNetworkListener() {
            @Override
            public void Connected() {
                OTPRequest otpRequest = new OTPRequest();
                otpRequest.accessToken = APIManager.mAccessToken;
                otpRequest.otpCode = txtPin.getText().toString();
                if (cuid != null) {
                    otpRequest.cuid = cuid;
                }
                presenter.oTPVerification(otpRequest, isRetrieve);
            }
        });
    }

    @Override
    public void createPin(CreatePin pinCode) {

    }

    private Boolean checkValidate() {
        if (txtPin.getText().toString().length() < 4) {
            showMessage(getString(R.string.txt_not_otp));
            return false;
        }
        return true;
    }
}
