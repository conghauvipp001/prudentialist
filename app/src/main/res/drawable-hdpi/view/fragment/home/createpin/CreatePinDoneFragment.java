package vn.mobiapps.pruf.view.fragment.home.createpin;

import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.view.activity.HomeActivity;
import vn.mobiapps.pruf.view.activity.LoginActivity;
import vn.mobiapps.pruf.view.base.BaseFragment;

/**
 * Created by Truong Thien on 3/13/2019.
 */

public class CreatePinDoneFragment extends BaseFragment {
    private ImageView imgBack;

    @Override
    protected int resLayout() {
        return R.layout.fragment_create_pin_done;
    }

    @Override
    protected void createFragment() {

    }

    @Override
    protected void setupFragment(View view) {
        imgBack = view.findViewById(R.id.imgBack);
        ((HomeActivity)getActivity()).hideSupport();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        }, 5000);
    }

    @Override
    protected void setOnClickListenerFragment() {
        imgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.imgBack:
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
                break;
            default:
                break;
        }
    }
}
