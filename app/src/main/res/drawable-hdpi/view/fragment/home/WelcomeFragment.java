package vn.mobiapps.pruf.view.fragment.home;

import android.content.Intent;
import android.view.View;
import android.widget.Button;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.view.activity.CustomerPageActivity;
import vn.mobiapps.pruf.view.activity.HomeActivity;
import vn.mobiapps.pruf.view.base.BaseFragment;
import vn.mobiapps.pruf.view.fragment.home.createpin.SetupPinFragment;

/**
 * Created by Truong Thien on 12/26/2018.
 */

public class WelcomeFragment extends BaseFragment {

    private Button btnContinueWelcome, btnSkip;

    @Override
    protected int resLayout() {
        return R.layout.fragment_welcome;
    }

    @Override
    protected void createFragment() {

    }

    @Override
    protected void setupFragment(View view) {
        btnContinueWelcome = view.findViewById(R.id.btnContinueWelcome);
        btnSkip = view.findViewById(R.id.btnSkip);
    }

    @Override
    protected void setOnClickListenerFragment() {
        btnContinueWelcome.setOnClickListener(this);
        btnSkip.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btnContinueWelcome:
                SetupPinFragment f = new SetupPinFragment();
                ((HomeActivity)getActivity()).pushFragment(f);
                break;
            case R.id.btnSkip:
                Intent intent = new Intent(getActivity(), CustomerPageActivity.class);
                startActivity(intent);
                getActivity().finish();
                break;
            default:
                break;
        }
    }
}
