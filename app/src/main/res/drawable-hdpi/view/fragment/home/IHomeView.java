package vn.mobiapps.pruf.view.fragment.home;

import vn.mobiapps.pruf.model.model_data.home.CreatePin;
import vn.mobiapps.pruf.view.base.IBaseView;

/**
 * Created by Truong Thien on 1/17/2019.
 */

public interface IHomeView extends IBaseView {
    void oTPGeneration(Boolean isRetrieve);
    void oTPVerification(Boolean isRetrieve);
    void createPin(CreatePin pinCode);
}
