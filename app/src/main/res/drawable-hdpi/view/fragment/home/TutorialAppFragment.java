package vn.mobiapps.pruf.view.fragment.home;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.VideoView;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.view.activity.CustomerPageActivity;
import vn.mobiapps.pruf.view.base.BaseFragment;

/**
 * Created by Truong Thien on 12/28/2018.
 */

public class TutorialAppFragment extends BaseFragment {

    private VideoView videoTutprial;
    private Button btnHome;

    @Override
    protected int resLayout() {
        return R.layout.fragment_tutorial_app;
    }

    @Override
    protected void createFragment() {

    }

    @Override
    protected void setupFragment(View view) {
        videoTutprial = view.findViewById(R.id.videoView);
        btnHome = view.findViewById(R.id.btnHome);

        /*MediaController mediaController = new MediaController(getActivity());
        mediaController.setAnchorView(videoTutprial);
        videoTutprial.setMediaController(mediaController);*/
        videoTutprial.setVideoURI(Uri.parse("https://storage.googleapis.com/coverr-main/mp4/Mt_Baker.mp4"));
        videoTutprial.start();
        videoTutprial.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        videoTutprial.setBackgroundDrawable(null);
                    }
                }, 100);
            }
        });
    }

    @Override
    protected void setOnClickListenerFragment() {
        btnHome.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btnHome:
                hideKeyboard();
                Intent intent = new Intent(getActivity(), CustomerPageActivity.class);
                startActivity(intent);
                getActivity().finish();
                break;
            default:
                break;
        }
    }
}
