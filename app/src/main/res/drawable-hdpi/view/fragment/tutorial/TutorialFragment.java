package vn.mobiapps.pruf.view.fragment.tutorial;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.VideoView;

import java.util.HashMap;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.view.base.BaseFragment;

/**
 * Created by Truong Thien on 12/13/2018.
 */

public class TutorialFragment extends BaseFragment implements ITutorialView {

    private VideoView videoTutprial;
    private Button btnHome;

    @Override
    protected int resLayout() {
        return R.layout.fragment_tutorial;
    }

    @Override
    protected void createFragment() {

    }

    @Override
    protected void setupFragment(View view) {
        videoTutprial = view.findViewById(R.id.videoView);
        btnHome = view.findViewById(R.id.btnHome);

        videoTutprial.setVideoURI(Uri.parse("https://storage.googleapis.com/coverr-main/mp4/Mt_Baker.mp4"));
        videoTutprial.start();
        videoTutprial.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        videoTutprial.setBackgroundDrawable(null);
                    }
                }, 100);
            }
        });
    }

    @Override
    protected void setOnClickListenerFragment() {
        btnHome.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btnHome:
                hideKeyboard();
                Intent intent = new Intent(getActivity(), vn.mobiapps.pruf.view.activity.LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
                break;
            default:
                break;
        }
    }

    public static Bitmap retriveVideoFrameFromVideo(String videoPath)
            throws Throwable {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime(-1, MediaMetadataRetriever.OPTION_CLOSEST);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String videoPath)"
                            + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }
}
