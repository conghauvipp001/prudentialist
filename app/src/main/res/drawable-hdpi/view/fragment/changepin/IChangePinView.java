package vn.mobiapps.pruf.view.fragment.changepin;

/**
 * Created by Truong Thien on 3/1/2019.
 */

public interface IChangePinView  extends vn.mobiapps.pruf.view.base.IBaseView {
    void changePin();
}
