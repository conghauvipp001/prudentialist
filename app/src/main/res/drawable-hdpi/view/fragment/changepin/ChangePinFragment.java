package vn.mobiapps.pruf.view.fragment.changepin;

import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import vn.mobiapps.pruf.R;
import vn.mobiapps.pruf.apimanager.APIManager;
import vn.mobiapps.pruf.model.model_data.BaseRespone;
import vn.mobiapps.pruf.model.model_data.changepin.ChangePinRequest;
import vn.mobiapps.pruf.presenter.changepin.ChangePinPresenterImpl;
import vn.mobiapps.pruf.presenter.changepin.IChangePinPresenter;
import vn.mobiapps.pruf.view.base.BaseFragment;
import vn.mobiapps.pruf.widgets.PinEntryEditText;
import vn.mobiapps.pruf.widgets.custombutton.CircularProgressButton;

/**
 * Created by Truong Thien on 2/28/2019.
 */

public class ChangePinFragment extends BaseFragment implements vn.mobiapps.pruf.view.fragment.changepin.IChangePinView {

    public PinEntryEditText edtCurrentPin, edtCurrentPinShow, edtNewPin, edtNewPinShow, edtVerifyPin, edtVerifyPinShow;
    public CircularProgressButton btnConfirm;
    private ImageView imgBack, imgEyeCurrentPin, imgEyeNewPin, imgEyeVerifyPin;
    private Boolean isShowCurrentPin = false, isShowNewPin = false, isShowVerifyPin = false;
    private TextView txtCurrentPinFail, txtNewPinFail, txtVerifyPinFail;
    private IChangePinPresenter presenter;

    @Override
    protected int resLayout() {
        return R.layout.fragment_change_pin;
    }

    @Override
    protected void createFragment() {
        if (presenter == null) {
            presenter = new ChangePinPresenterImpl(this);
        }
    }

    @Override
    protected void setupFragment(View view) {
        edtCurrentPin = view.findViewById(R.id.edtCurrentPin);
        edtCurrentPinShow = view.findViewById(R.id.edtCurrentPinShow);
        edtNewPin = view.findViewById(R.id.edtNewPin);
        edtNewPinShow = view.findViewById(R.id.edtNewPinShow);
        edtVerifyPin = view.findViewById(R.id.edtVerifyPin);
        edtVerifyPinShow = view.findViewById(R.id.edtVerifyPinShow);
        btnConfirm = view.findViewById(R.id.btnConfirm);
        txtCurrentPinFail = view.findViewById(R.id.txtCurrentPinFail);
        txtNewPinFail = view.findViewById(R.id.txtNewPinFail);
        txtVerifyPinFail = view.findViewById(R.id.txtVerifyPinFail);
        imgBack = view.findViewById(R.id.imgBack);
        imgEyeCurrentPin = view.findViewById(R.id.imgEyeCurrentPin);
        imgEyeNewPin = view.findViewById(R.id.imgEyeNewPin);
        imgEyeVerifyPin = view.findViewById(R.id.imgEyeVerifyPin);
    }

    @Override
    protected void setOnClickListenerFragment() {
        btnConfirm.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        imgEyeCurrentPin.setOnClickListener(this);
        imgEyeNewPin.setOnClickListener(this);
        imgEyeVerifyPin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.imgBack:
                hideKeyboard();
                ((vn.mobiapps.pruf.view.activity.ChangePinActivity)getActivity()).onBackPressed();
                break;
            case R.id.imgEyeCurrentPin:
                hideKeyboard();
                if (isShowCurrentPin) {
                    imgEyeCurrentPin.setImageResource(R.drawable.eyeoff);
                    isShowCurrentPin = false;
                    hidePin(edtCurrentPin, edtCurrentPinShow);
                } else {
                    imgEyeCurrentPin.setImageResource(R.drawable.eyeon);
                    isShowCurrentPin = true;
                    showPin(edtCurrentPinShow, edtCurrentPin);
                }
                break;
            case R.id.imgEyeNewPin:
                hideKeyboard();
                if (isShowNewPin) {
                    imgEyeNewPin.setImageResource(R.drawable.eyeoff);
                    isShowNewPin = false;
                    hidePin(edtNewPin, edtNewPinShow);
                } else {
                    imgEyeNewPin.setImageResource(R.drawable.eyeon);
                    isShowNewPin = true;
                    showPin(edtNewPinShow, edtNewPin);
                }
                break;
            case R.id.imgEyeVerifyPin:
                hideKeyboard();
                if (isShowVerifyPin) {
                    imgEyeVerifyPin.setImageResource(R.drawable.eyeoff);
                    isShowVerifyPin = false;
                    hidePin(edtVerifyPin, edtVerifyPinShow);
                } else {
                    imgEyeVerifyPin.setImageResource(R.drawable.eyeon);
                    isShowVerifyPin = true;
                    showPin(edtVerifyPinShow, edtVerifyPin);
                }
                break;
            case R.id.btnConfirm:
                hideKeyboard();
                if (btnConfirm.getProgress() == 0) {
                    if (checkValidate()) {
                        changePin();
                    }
                } else {
                    btnConfirm.setProgress(0);
                }
                break;
            default:
                break;
        }
    }

    private Boolean checkValidate() {
        Boolean checkCurrentPin = edtCurrentPin.getText().toString().isEmpty() && edtCurrentPinShow.getText().toString().isEmpty() ||
                edtCurrentPin.getText().toString().length() < 6 && edtCurrentPinShow.getText().toString().length() < 6;
        Boolean checkNewPin = edtNewPin.getText().toString().isEmpty() && edtNewPinShow.getText().toString().isEmpty() ||
                edtNewPin.getText().toString().length() < 6 && edtNewPinShow.getText().toString().length() < 6;

        Boolean checkVerify = true, checkVerifyPin = true ;
        String newPin = "", verifyPin = "";
        if (!edtNewPin.getText().toString().trim().equals("")) {
            newPin = edtNewPin.getText().toString().trim();
        } else {
            newPin = edtNewPinShow.getText().toString().trim();
        }

        if (!edtVerifyPin.getText().toString().trim().equals("")) {
            verifyPin = edtVerifyPin.getText().toString().trim();
        } else {
            verifyPin = edtVerifyPinShow.getText().toString().trim();
        }

        checkVerifyPin = verifyPin.isEmpty() || verifyPin.length() < 6;

        if (newPin.equals(verifyPin)) {
            checkVerify = false;
        } else {
            checkVerify = true;
        }

        if (checkCurrentPin || checkNewPin || checkVerifyPin || checkVerify) {
            if (checkCurrentPin) {
                txtCurrentPinFail.setVisibility(View.VISIBLE);
                txtCurrentPinFail.setText(getString(R.string.txt_not_pin));
            } else {
                txtCurrentPinFail.setVisibility(View.INVISIBLE);
            }

            if (checkNewPin) {
                txtNewPinFail.setVisibility(View.VISIBLE);
                txtNewPinFail.setText(getString(R.string.txt_not_pin));
            } else {
                txtNewPinFail.setVisibility(View.INVISIBLE);
            }

            if (checkVerifyPin) {
                txtVerifyPinFail.setVisibility(View.VISIBLE);
                txtVerifyPinFail.setText(getString(R.string.txt_not_pin));
            } else {
                if (checkVerify) {
                    txtVerifyPinFail.setVisibility(View.VISIBLE);
                    txtVerifyPinFail.setText(R.string.txt_not_mat);
                    return false;
                } else {
                    txtVerifyPinFail.setVisibility(View.INVISIBLE);
                }
            }
            return false;
        }
        txtVerifyPinFail.setVisibility(View.INVISIBLE);
        return true;
    }

    private void showPin(PinEntryEditText show, PinEntryEditText hide) {
        if (show != null && hide != null) {
            show.setText(hide.getText().toString().trim());
            hide.setText("");
            hide.setVisibility(View.GONE);
            show.setVisibility(View.VISIBLE);
        }
    }

    private void hidePin(PinEntryEditText hide, PinEntryEditText show) {
        if (hide != null && show != null) {
            hide.setText(show.getText().toString().trim());
            show.setText("");
            show.setVisibility(View.GONE);
            hide.setVisibility(View.VISIBLE);
        }
    }

    private void simulateSuccessProgress(final CircularProgressButton button) {
        ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 100);
        widthAnimation.setDuration(1500);
        widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                button.setProgress(value);
            }
        });
        widthAnimation.start();
    }

    private void simulateErrorProgress(final CircularProgressButton button) {
        ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 99);
        widthAnimation.setDuration(1500);
        widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                button.setProgress(value);
                if (value == 99) {
                    button.setProgress(-1);
                }
            }
        });
        widthAnimation.start();
    }

    @Override
    public void changePin() {
        CheckNetworkRequest((vn.mobiapps.pruf.view.base.BaseActivity) getActivity(), new CheckNetworkListener() {
            @Override
            public void Connected() {
                ChangePinRequest pinRequest = new ChangePinRequest();
                pinRequest.accessToken = APIManager.mAccessToken;
                String currentPin = "";
                if (edtCurrentPin.getText().toString() != null && !edtCurrentPin.getText().toString().equals("")) {
                    currentPin = edtCurrentPin.getText().toString().trim();
                } else {
                    currentPin = edtCurrentPinShow.getText().toString().trim();
                }

                String verifyPin = "";
                if (edtVerifyPin.getText().toString() != null && !edtVerifyPin.getText().toString().equals("")) {
                    verifyPin = edtVerifyPin.getText().toString().trim();
                } else {
                    verifyPin = edtVerifyPinShow.getText().toString().trim();
                }
                pinRequest.currentPin = currentPin;
                pinRequest.newPin = verifyPin;
                presenter.changePin(pinRequest);
            }
        });
    }

    @Override
    public void onSuccess(Object objectSuccess) {
        super.onSuccess(objectSuccess);
        try {
            if (objectSuccess != null) {
                BaseRespone respone = (BaseRespone) objectSuccess;
                if (respone.result_code.equals("0")) {
                    simulateSuccessProgress(btnConfirm);
                    /*DialogFactory.dialog_Change_Pin(getContext(), getString(R.string.txt_changePin_ss), new DialogFactory.DialogListener.OTPListener() {
                        @Override
                        public void next() {
                            Intent intent = new Intent(getActivity(), CustomerPageActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    });*/
                } else {
                    simulateErrorProgress(btnConfirm);
                    showMessage(respone.result_description);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Object objectError) {
        super.onError(objectError);
        simulateErrorProgress(btnConfirm);
    }
}
