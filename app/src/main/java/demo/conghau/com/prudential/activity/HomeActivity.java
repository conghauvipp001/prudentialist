package demo.conghau.com.prudential.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import demo.conghau.com.prudential.R;
import demo.conghau.com.prudential.base.BaseActivity;
import demo.conghau.com.prudential.login.ResponseLogin;
import demo.conghau.com.prudential.utils.Constant;

public class HomeActivity extends BaseActivity {

    public ResponseLogin logndata;
    public Boolean isRetrieve;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentChoose(R.layout.activity_home);
        Constant.context = this;
        logndata = (ResponseLogin) getIntent().getSerializableExtra("LognData");
        isRetrieve = getIntent().getBooleanExtra("isRetrieve", false);
        try {
            if (logndata != null && logndata.data != null) {
                if (Boolean.valueOf(logndata.data.verifyOTP)) {
//                    vn.mobiapps.pruf.view.fragment.home.otpreceiving.OTPReceivingFragment f = new vn.mobiapps.pruf.view.fragment.home.otpreceiving.OTPReceivingFragment();
                    //pus;
                } else {
                    //pushFragment());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        popFragment();
    }

    @Override
    public void endFragment() {
        super.endFragment();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        this.finish();
    }

}
