package demo.conghau.com.prudential.faq;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import demo.conghau.com.prudential.R;
import demo.conghau.com.prudential.login.MainActivity;

public class FAQActivity extends AppCompatActivity implements View.OnClickListener {

    public ImageView imgBack;
    public String data;
    public TextView title;
    public WebView webview;
    public ListView lvFAQ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        control();
    }

    public void control(){
        imgBack = findViewById(R.id.imgBack);
        title = findViewById(R.id.tvTitle);
        lvFAQ = findViewById(R.id.lvFAQ);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FAQActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View view) {

    }

    public void onDestroy(){
        super.onDestroy();
        webview = null;
    }

}
