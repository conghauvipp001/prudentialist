package demo.conghau.com.prudential.contact;

import demo.conghau.com.prudential.base.IBaseView;

public interface IContactView extends IBaseView {

    void getCommunication();
}
