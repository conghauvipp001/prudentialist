package demo.conghau.com.prudential.utils;

import java.io.Serializable;

public class ResponseErrorModel implements Serializable{

    String result_code;
    String Codes;
    String result_description;

    public ResponseErrorModel() {
    }

    public ResponseErrorModel(String code, String codes, String message) {
        result_code = code;
        Codes = codes;
        result_description = message;
    }

    public String getResult_code() {
        return result_code;
    }

    public void setResult_code(String result_code) {
        this.result_code = result_code;
    }

    public String getCodes() {
        return Codes;
    }

    public void setCodes(String codes) {
        Codes = codes;
    }

    public String getResult_description() {
        return result_description;
    }

    public void setResult_description(String result_description) {
        this.result_description = result_description;
    }
}
