package demo.conghau.com.prudential.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import demo.conghau.com.prudential.R;
import demo.conghau.com.prudential.location.base.BaseActivity;
import demo.conghau.com.prudential.utils.Constant;

public class LocationActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        Constant.context = this;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void endFragment(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        this.finish();
    }
}
