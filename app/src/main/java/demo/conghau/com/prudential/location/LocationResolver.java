package demo.conghau.com.prudential.location;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.google.android.gms.common.api.GoogleApiClient;

public class LocationResolver implements GoogleApiClient.ConnectionCallbacks {

    private OnLocationResolved mOnLocationResolved;

    //Google Api Client
    private GoogleApiClient mGoogleApiClient;

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        // startLocationPooling();
    }

//    public void resolveLocation(Activity activity, OnLocationResolved onLocationResolved){
//        this.mOnLocationResolved = onLocationResolved;
//        this.mActivity=activity;
//
//        if (isEveryThingEnabled()){
//            startLocationPooling();
//        }
//    }

    public interface OnLocationResolved{
        void onLocationResolved(Location location);
    }

    @Override
    public void onConnectionSuspended(int i) {

        mGoogleApiClient.connect();
    }
}
