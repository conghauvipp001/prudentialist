package demo.conghau.com.prudential.location.offices;

import java.io.Serializable;

public class BaseResponse implements Serializable {
    public String result_code;
    public String result_description;
    public String status;
}
