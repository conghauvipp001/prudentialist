package demo.conghau.com.prudential.register;

import android.graphics.Bitmap;

import java.io.Serializable;

public class RegisterModel implements Serializable {

    public String Email;
    public String PhoneNumber;
    public String Address;
    public String MonthlyIncome;
    public String Amount;
    public String IdentityCardFront;
    public String IdentityCardBehind;
    public Bitmap IncomePapers;
}

