package demo.conghau.com.prudential.utils;

import android.graphics.Bitmap;

public class ShareReference {
    public static boolean isLogin = false;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    public static boolean isCheckLog = true;
    public static Bitmap bitmap;
    //public static StringPrefs firebaseTokenPref;
    private static String firebaseToken;
    public static String DeviceUniqueIdentifier;


    public static String getFirebaseToken() {
        return firebaseToken;
    }

    public static void setFirebaseToken(String firebaseToken) {
        ShareReference.firebaseToken = firebaseToken;
    }
}