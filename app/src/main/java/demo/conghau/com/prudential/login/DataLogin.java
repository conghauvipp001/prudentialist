package demo.conghau.com.prudential.login;

import java.io.Serializable;

public class DataLogin implements Serializable {

    public String accessToken;
    public String verifyOTP;
    public String createPin;
    public String phoneNumber;
    public String email;
    public String cuid;

}
