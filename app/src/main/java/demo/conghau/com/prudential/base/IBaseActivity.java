package demo.conghau.com.prudential.base;

public interface IBaseActivity {
    void showProgressBar();
    void hideProgressBar();
}
