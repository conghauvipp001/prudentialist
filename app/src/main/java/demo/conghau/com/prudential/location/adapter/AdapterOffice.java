package demo.conghau.com.prudential.location.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import demo.conghau.com.prudential.R;
import demo.conghau.com.prudential.location.model.Contact;

public class AdapterOffice extends BaseAdapter {
    private Context context;
    private int resource;
    private ArrayList<Contact> officeArrayList;

    public AdapterOffice(Context context, int resource, ArrayList<Contact> officeArrayList) {
        this.context = context;
        this.resource = resource;
        this.officeArrayList = officeArrayList;
    }

    @Override
    public int getCount() {
        return officeArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_lvgooglemap, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.txtName = convertView.findViewById(R.id.txtName);
            viewHolder.txtAddress = convertView.findViewById(R.id.txtAdd);
            viewHolder.txtSDT = convertView.findViewById(R.id.txtNumberphone);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Contact office = officeArrayList.get(position);
        viewHolder.txtName.setText(office.name);
        viewHolder.txtAddress.setText(office.address);
        viewHolder.txtSDT.setText(office.phone);

        return convertView;
    }

    public class ViewHolder {
        TextView txtName, txtAddress, txtSDT;
    }
}