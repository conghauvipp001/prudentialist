package demo.conghau.com.prudential.contact;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;

import demo.conghau.com.prudential.login.MainActivity;
import demo.conghau.com.prudential.R;
import demo.conghau.com.prudential.faq.RippleBackground;

import static android.content.Intent.ACTION_VIEW;

public class ContactActivity extends AppCompatActivity implements View.OnClickListener {

    public ImageView imgBack, imZalo, imFacebook, imEmail, imPhone;
    public RippleBackground bg_Contact;
    public Button btnGui;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_fragment);
        control();
    }

    public void control() {
        imgBack = findViewById(R.id.imgBacking);
        bg_Contact = findViewById(R.id.bgContent);
        imZalo = findViewById(R.id.imgZalo);
        imFacebook = findViewById(R.id.imgFaceBook);
        imEmail = findViewById(R.id.imgEmail);
        imPhone = findViewById(R.id.imgPhone);
        btnGui = findViewById(R.id.btn_Send);
        startAnimation();

        imZalo.setOnClickListener(this);
        imFacebook.setOnClickListener(this);
        imEmail.setOnClickListener(this);
        imPhone.setOnClickListener(this);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ContactActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    public void startAnimation() {
        final Handler handler = new Handler();
        bg_Contact.startRippleAnimation();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showZalo();
                showFacebook();
                showEmail();
                showPhone();
            }
        }, 3000);
    }

    public void showZalo() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        ArrayList<Animator> animatorList = new ArrayList<Animator>();
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(imZalo, "ScaleX", 0f, 1.2f, 1f);
        animatorList.add(scaleXAnimator);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(imZalo, "ScaleY", 0f, 1.2f, 1f);
        animatorList.add(scaleYAnimator);
        animatorSet.playTogether(animatorList);
        imZalo.setVisibility(View.VISIBLE);
        animatorSet.start();
    }

    public void showFacebook() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        ArrayList<Animator> animatorList = new ArrayList<Animator>();
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(imFacebook, "ScaleX", 0f, 1.2f, 1f);
        animatorList.add(scaleXAnimator);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(imFacebook, "ScaleY", 0f, 1.2f, 1f);
        animatorList.add(scaleYAnimator);
        animatorSet.playTogether(animatorList);
        imFacebook.setVisibility(View.VISIBLE);
        animatorSet.start();
    }

    public void showEmail() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        ArrayList<Animator> animatorList = new ArrayList<Animator>();
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(imEmail, "ScaleX", 0f, 1.2f, 1f);
        animatorList.add(scaleXAnimator);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(imEmail, "ScaleY", 0f, 1.2f, 1f);
        animatorList.add(scaleYAnimator);
        animatorSet.playTogether(animatorList);
        imEmail.setVisibility(View.VISIBLE);
        animatorSet.start();
    }

    private void showPhone() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        ArrayList<Animator> animatorList = new ArrayList<Animator>();
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(imPhone, "ScaleX", 0f, 1.2f, 1f);
        animatorList.add(scaleXAnimator);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(imPhone, "ScaleY", 0f, 1.2f, 1f);
        animatorList.add(scaleYAnimator);
        animatorSet.playTogether(animatorList);
        imPhone.setVisibility(View.VISIBLE);
        animatorSet.start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imgZalo: {
                try {
                    Intent intent = new Intent(ACTION_VIEW, Uri.parse("https://zalo.me/" + "0772918604"));
                    ;
                    startActivity(intent);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            break;

            case R.id.imgFaceBook: {
                try {
                    Intent intent = new Intent(ACTION_VIEW, Uri.parse("fb://profile/MY_PAGE_ID"));
                    startActivity(intent);
                } catch (Exception ex) {
                    startActivity(new Intent(ACTION_VIEW, Uri.parse("https://www.facebook.com/NguyenCongHauIT" + "")));
                }
            }
            break;

            case R.id.imgPhone: {
                String numberphone = "0123456789";
                Uri number = Uri.parse("tel:" + numberphone);
                Intent dial = new Intent(Intent.ACTION_DIAL);
                dial.setData(number);
                startActivity(dial);
            }
            break;

            case R.id.imgEmail: {

                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "abc@gmail.com", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "This is my subject text");
                this.startActivity(Intent.createChooser(emailIntent, null));

//                Intent intent = new Intent(Intent.ACTION_SEND);
//                intent.setType("plain/text");
//                intent.putExtra(Intent.EXTRA_EMAIL, new String[] {"nguyenleconghauit@gmail.com"});
//                intent.putExtra(Intent.EXTRA_SUBJECT, "");
//                intent.putExtra(Intent.EXTRA_TEXT, "");
//                startActivity(intent.createChooser(intent, "Email Informtion"));
            }
            break;
        }
    }

    public void onSuccess(Object objectSuccess) {
        updateUI(objectSuccess);
    }

    public void updateUI(Object object) {

    }


}
