package demo.conghau.com.prudential.login;

public interface ILoginPresenter {

    void login(LoginRequest loginRequest);
    void getBackground();
}
