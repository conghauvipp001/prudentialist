package demo.conghau.com.prudential.pin;

import android.animation.ValueAnimator;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import demo.conghau.com.prudential.login.MainActivity;
import demo.conghau.com.prudential.R;
import demo.conghau.com.prudential.base.BaseRespone;
import demo.conghau.com.prudential.changepass.CircularProgressButton;
import demo.conghau.com.prudential.utils.ShareReference;


public class ChangePinActivity extends AppCompatActivity implements View.OnClickListener {

    private PinEntryEditText edtCurrentPin, edtCurrentPinShow, edtNewPin, edtNewPinShow, edtVerifyPin, edtVerifyPinShow;
    public CircularProgressButton btnConfirm;
    private ImageView imgBack, imgAvatar1, imgEyeCurrentPin, imgEyeNewPin, imgEyeVerifyPin;
    private Boolean isShowCurrentPin = false, isShowNewPin = false, isShowVerifyPin = false;
    private TextView txtCurrentPinFail, txtNewPinFail, txtVerifyPinFail;

    private CircleImageView circleImageView;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    public static final int REQUEST_IMAGE = 100;
    // may cai nay` ong de 10000000 bao nhieu cung duoc , de cho no hieu la no se lang nghe cai su kien nao khi minh click vo do
    public static final int REQUEST_PERMISSION = 200;
    public static final int PICK_IMAGE = 120;
    private String imageFilePath = "";
    private Bitmap selectedImage = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_pin);
        //addListener();
        circleImageView = findViewById(R.id.profile_manager);

        control();
        //image là hình tròn
        //imgAvatar1 = findViewById(R.id.imgAvatar);

        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verticalAlert();
            }
        });
    }

    public void verticalAlert() {
        String[] array = {"Take photo", "From Library"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Thêm ảnh")
                .setItems(array, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int id) {
                        switch (id) {
                            case 0:
                                openCameraIntent();
                                break;
                            case 1:
                                gallery();
                                break;
                        }
                    }
                });
        builder.show();
    }

    public void openCameraIntent() {
        /*Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
            Uri photoUri = FileProvider.getUriForFile(this, getPackageName() + ".provider", photoFile);
            pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            startActivityForResult(pictureIntent, REQUEST_IMAGE);
        }*/

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(intent.resolveActivity(getPackageManager()) != null){
            startActivityForResult(intent, REQUEST_IMAGE);
        }
        //startActivityForResult(intent,REQUEST_IMAGE);
    }

        public void gallery(){
        // Cai nay may cai ham co san dem ra xai thoi
            Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            startActivityForResult(gallery,PICK_IMAGE);
            gallery.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(gallery,"Select Picture"), PICK_IMAGE);
            // .startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
        }
        // cai onActivityResult nay kieu nhu khi ong lam 1 hanh dong gi dong no se tra ve cai nay

        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data){
            if (resultCode == RESULT_OK){
                if(requestCode == PICK_IMAGE){
                    try {
                        Uri imageUri = data.getData();
                        InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        selectedImage = BitmapFactory.decodeStream(imageStream);
                        ShareReference.bitmap = selectedImage;
                        circleImageView.setImageBitmap(selectedImage);

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    circleImageView.setImageURI(data.getData());//circle thay bang imageview la dc

                }else if (requestCode == REQUEST_IMAGE){
                    // luc ma` minh bat du kien truyen du lieu qua man hinh chup hinh do, sau khi chup thi minh truyen nguoc lai de lay gia tri ra
                    selectedImage = (Bitmap) data.getExtras().get("data");
                    ShareReference.bitmap = selectedImage;
                    // Sau khi chup xong thi set no vo cai Imageview cua minh
                    circleImageView.setImageBitmap(selectedImage);//circle thay bang imageview la dc

                }
            }
        }

    public void control(){
        edtCurrentPin = findViewById(R.id.edtPinCurrent);
        edtCurrentPinShow = findViewById(R.id.edtCurrentPinShow);
        edtNewPin = findViewById(R.id.edtNewPin);
        edtNewPinShow = findViewById(R.id.edtNewPinShow);
        edtVerifyPin = findViewById(R.id.edtVerifyPin);
        edtVerifyPinShow = findViewById(R.id.edtVerifyPinShow);
        btnConfirm = findViewById(R.id.btnConfirm);
        imgBack = findViewById(R.id.imgBacking);
        imgEyeCurrentPin = findViewById(R.id.imgEyeCurrentPin);
        imgEyeNewPin = findViewById(R.id.imgEyeNewPin);
        imgEyeVerifyPin = findViewById(R.id.imgEyeVerifyPin);
        txtCurrentPinFail = findViewById(R.id.txtCurrentPinFail);
        txtNewPinFail = findViewById(R.id.txtNewPinFail);
        txtVerifyPinFail = findViewById(R.id.txtVerifyPinFail);
        if (ShareReference.bitmap != null) {
            circleImageView.setImageBitmap(ShareReference.bitmap);
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChangePinActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    protected void setOnClickListener() {
        btnConfirm.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        imgEyeCurrentPin.setOnClickListener(this);
        imgEyeNewPin.setOnClickListener(this);
        imgEyeVerifyPin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgEyeCurrentPin:

                if (isShowCurrentPin) {
                    imgEyeCurrentPin.setImageResource(R.drawable.eyeoff);
                    isShowCurrentPin = false;
                    hidePin(edtCurrentPin, edtCurrentPinShow);
                } else {
                    imgEyeCurrentPin.setImageResource(R.drawable.eyeon);
                    isShowCurrentPin = true;
                    showPin(edtCurrentPinShow, edtCurrentPin);
                }
                break;
            case R.id.imgEyeNewPin:

                if (isShowNewPin) {
                    imgEyeNewPin.setImageResource(R.drawable.eyeoff);
                    isShowNewPin = false;
                    hidePin(edtNewPin, edtNewPinShow);
                } else {
                    imgEyeNewPin.setImageResource(R.drawable.eyeon);
                    isShowNewPin = true;
                    showPin(edtNewPinShow, edtNewPin);
                }
                break;
            case R.id.imgEyeVerifyPin:

                if (isShowVerifyPin) {
                    imgEyeVerifyPin.setImageResource(R.drawable.eyeoff);
                    isShowVerifyPin = false;
                    hidePin(edtVerifyPin, edtVerifyPinShow);
                } else {
                    imgEyeVerifyPin.setImageResource(R.drawable.eyeon);
                    isShowVerifyPin = true;
                    showPin(edtVerifyPinShow, edtVerifyPin);
                }
                break;
            case R.id.btnConfirm:

                if (btnConfirm.getProgress() == 0) {
                    if (checkValidate()) {
                        //changePin();
                    }
                } else {
                    btnConfirm.setProgress(0);
                }
                break;
        }
    }

    private Boolean checkValidate() {
        Boolean checkCurrentPin = edtCurrentPin.getText().toString().isEmpty() && edtCurrentPinShow.getText().toString().isEmpty() ||
                edtCurrentPin.getText().toString().length() < 6 && edtCurrentPinShow.getText().toString().length() < 6;
        Boolean checkNewPin = edtNewPin.getText().toString().isEmpty() && edtNewPinShow.getText().toString().isEmpty() ||
                edtNewPin.getText().toString().length() < 6 && edtNewPinShow.getText().toString().length() < 6;

        Boolean checkVerify = true, checkVerifyPin = true ;
        String newPin = "", verifyPin = "";
        if (!edtNewPin.getText().toString().trim().equals("")) {
            newPin = edtNewPin.getText().toString().trim();
        } else {
            newPin = edtNewPinShow.getText().toString().trim();
        }

        if (!edtVerifyPin.getText().toString().trim().equals("")) {
            verifyPin = edtVerifyPin.getText().toString().trim();
        } else {
            verifyPin = edtVerifyPinShow.getText().toString().trim();
        }

        checkVerifyPin = verifyPin.isEmpty() || verifyPin.length() < 6;

        if (newPin.equals(verifyPin)) {
            checkVerify = false;
        } else {
            checkVerify = true;
        }

        if (checkCurrentPin || checkNewPin || checkVerifyPin || checkVerify) {
            if (checkCurrentPin) {
                txtCurrentPinFail.setVisibility(View.VISIBLE);
                txtCurrentPinFail.setText(getString(R.string.txt_fail_pin));
            } else {
                txtCurrentPinFail.setVisibility(View.INVISIBLE);
            }

            if (checkNewPin) {
                txtNewPinFail.setVisibility(View.VISIBLE);
                txtNewPinFail.setText(getString(R.string.txt_fail_pin));
            } else {
                txtNewPinFail.setVisibility(View.INVISIBLE);
            }

            if (checkVerifyPin) {
                txtVerifyPinFail.setVisibility(View.VISIBLE);
                txtVerifyPinFail.setText(getString(R.string.txt_fail_pin));
            } else {
                if (checkVerify) {
                    txtVerifyPinFail.setVisibility(View.VISIBLE);
                    txtVerifyPinFail.setText(R.string.txt_not_mat);
                    return false;
                } else {
                    txtVerifyPinFail.setVisibility(View.INVISIBLE);
                }
            }
            return false;
        }
        txtVerifyPinFail.setVisibility(View.INVISIBLE);
        return true;
    }

    private void showPin(PinEntryEditText show, PinEntryEditText hide) {
        if (show != null && hide != null) {
            show.setText(hide.getText().toString().trim());
            hide.setText("");
            hide.setVisibility(View.GONE);
            show.setVisibility(View.VISIBLE);
        }
    }

    private void hidePin(PinEntryEditText hide, PinEntryEditText show) {
        if (hide != null && show != null) {
            hide.setText(show.getText().toString().trim());
            show.setText("");
            show.setVisibility(View.GONE);
            hide.setVisibility(View.VISIBLE);
        }
    }

    private void simulateSuccessProgress(final CircularProgressButton button) {
        ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 100);
        widthAnimation.setDuration(1500);
        widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                button.setProgress(value);
            }
        });
        widthAnimation.start();
    }

    private void simulateErrorProgress(final CircularProgressButton button) {
        ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 99);
        widthAnimation.setDuration(1500);
        widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                button.setProgress(value);
                if (value == 99) {
                    button.setProgress(-1);
                }
            }
        });
        widthAnimation.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item, menu);
        return true;
    }

//    @Override
//    public boolean onContextItemSelected(MenuItem item) {
//
//        int id = item.getItemId();
//        switch (id) {
//            case R.id.Take :
//                openCameraIntent();
//                break;
//            case R.id.Choose :
//                galleryIntent();
//                break;
//        }
//        return super.onContextItemSelected(item);
//    }

    public File createImageFile() throws IOException{

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        imageFilePath = image.getAbsolutePath();

        return image;
    }

    public void onSuccess(Object objectSuccess) {

        try {
            if (objectSuccess != null) {
                BaseRespone respone = (BaseRespone) objectSuccess;
                if (respone.result_code.equals("0")) {
                    simulateSuccessProgress(btnConfirm);
                    /*DialogFactory.dialog_Change_Pin(getContext(), getString(R.string.txt_changePin_ss), new DialogFactory.DialogListener.OTPListener() {
                        @Override
                        public void next() {
                            Intent intent = new Intent(getActivity(), CustomerPageActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    });*/
                } else {
                    simulateErrorProgress(btnConfirm);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onError(Object objectError) {
        simulateErrorProgress(btnConfirm);
    }
}


