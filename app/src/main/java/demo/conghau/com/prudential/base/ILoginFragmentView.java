package demo.conghau.com.prudential.base;

public interface ILoginFragmentView extends IBaseView{

    void login();
    void getBackground();

    void onSuccessSetBackground();
    void onErrorSetBackground();

}
