package demo.conghau.com.prudential.dialog;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import demo.conghau.com.prudential.R;
import demo.conghau.com.prudential.activity.LoginActivity;
import demo.conghau.com.prudential.base.BaseActivity;
import demo.conghau.com.prudential.utils.Constant;

public class DialogFactory {
    public static String style = "PinCode";

    public interface DialogListener {
        interface RetryNetListener {
            void retry(TranslucentDialog dialog);
        }

        interface LogoutListener {
            void logOut();
        }

        interface OTPListener {
            void next();
        }

        interface FastLoginListener {
            void login(String style, String pass);
            void logOut();
        }
    }

    public static void dialog_TryNetAgain(final BaseActivity context, String message, final DialogListener.RetryNetListener listener) {
        final TranslucentDialog dialog = new TranslucentDialog(context);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_retry_net);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        TextView tvMessage = (TextView) dialog.findViewById(R.id.txtContent);
        tvMessage.setText(message);
        TextView btTryNet = (TextView) dialog.findViewById(R.id.btTryNet);
        TextView btSetup = (TextView) dialog.findViewById(R.id.btSetup);
        btSetup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_MAIN, null);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.wifi.WifiSettings");
                intent.setComponent(cn);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        btTryNet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.retry(dialog);
            }
        });
        dialog.show();
    }

    public static void dialog_Message(Context context, String message) {
        final TranslucentDialog dialog = new TranslucentDialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_messenger);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        TextView tvMessage = (TextView) dialog.findViewById(R.id.txtContentRegister);
        tvMessage.setText(message);
        TextView btnQuit = (TextView) dialog.findViewById(R.id.btn_OK_DialogRegister);
        btnQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void dialog_Logout(Context context, String message, final DialogListener.LogoutListener listener) {
        final TranslucentDialog dialog = new TranslucentDialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_logout);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        TextView tvMessage = dialog.findViewById(R.id.txtContent);
        tvMessage.setText(message);
        TextView btnYes = dialog.findViewById(R.id.btYes);
        TextView btnNo = dialog.findViewById(R.id.btNo);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.logOut();
                dialog.dismiss();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void createMessageDialogSessionTimeout(final Activity context, final String message, final String code) {
        try {
            final TranslucentDialog dialog = new TranslucentDialog(context);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_messenger);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            final TextView tvMessage = (TextView) dialog.findViewById(R.id.txtContentRegister);
            tvMessage.setText(message);
            TextView btnQuit = (TextView) dialog.findViewById(R.id.btn_OK_DialogRegister);
            btnQuit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goToLogin(code, context);
                }
            });
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void goToLogin(String reponseCode, Activity activity) {
        try {
            if (reponseCode.equals(Constant.RESPONSE_CODE)) {
                Intent intent = new Intent(activity, LoginActivity.class);
                activity.startActivity(intent);
                activity.finish();
                activity.overridePendingTransition(R.anim.pop_enter_menu, R.anim.pop_exit_menu);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void createMessageDialogOpenRPS(Context context, String message, final DialogListener.LogoutListener listener) {
        final TranslucentDialog dialog = new TranslucentDialog(context);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_open_rps);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        TextView tvMessage = (TextView) dialog.findViewById(R.id.txtContent);
        tvMessage.setText(message);
        TextView btnYes = (TextView) dialog.findViewById(R.id.btYes);
        TextView btnNo = (TextView) dialog.findViewById(R.id.btNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.logOut();
                dialog.dismiss();
            }
        });
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static void createMessageDialog(Context context, String message) {
        final TranslucentDialog dialog = new TranslucentDialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_messenger);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        TextView tvMessage = (TextView) dialog.findViewById(R.id.txtContentRegister);
        tvMessage.setText(message);
        TextView btnQuit = (TextView) dialog.findViewById(R.id.btn_OK_DialogRegister);
        btnQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void dialogFastLogin(final Context context, final DialogListener.FastLoginListener listener) {
        final TranslucentDialog dialog = new TranslucentDialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_fast_login);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        final TextView txtPin = (TextView) dialog.findViewById(R.id.txtPin);
        final TextView txtLogout = (TextView) dialog.findViewById(R.id.txtLogout);
        final EditText edtPinCode = dialog.findViewById(R.id.edtPinCode);
        final Button btn_Logout = dialog.findViewById(R.id.btn_Logout);
        final Button btn_Login = dialog.findViewById(R.id.btn_Login);

        txtPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtPinCode.setVisibility(View.VISIBLE);
                btn_Logout.setVisibility(View.GONE);
                btn_Login.setVisibility(View.VISIBLE);
                edtPinCode.setHint("Nhập mã PIN");
                edtPinCode.setText("");
                style = "PinCode";

                txtPin.setBackgroundColor(context.getResources().getColor(R.color.nice_blue1));
                txtPin.setTextColor(context.getResources().getColor(R.color.colorWhite));

                txtLogout.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));
                txtLogout.setTextColor(context.getResources().getColor(R.color.colortextBack));
            }
        });

        txtLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtPinCode.setVisibility(View.GONE);
                btn_Logout.setVisibility(View.VISIBLE);
                btn_Login.setVisibility(View.GONE);

                txtLogout.setBackgroundColor(context.getResources().getColor(R.color.nice_blue1));
                txtLogout.setTextColor(context.getResources().getColor(R.color.colorWhite));

                txtPin.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));
                txtPin.setTextColor(context.getResources().getColor(R.color.colortextBack));
            }
        });

        btn_Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.logOut();
                dialog.dismiss();
            }
        });
        btn_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.login(style, edtPinCode.getText().toString());
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void dialog_Message_OTP(Context context, String message, final DialogListener.OTPListener listener) {
        final TranslucentDialog dialog = new TranslucentDialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_messenger_otp);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        TextView tvMessage = (TextView) dialog.findViewById(R.id.txtContentRegister);
        tvMessage.setText(message);
        TextView btnQuit = (TextView) dialog.findViewById(R.id.btn_OK_DialogRegister);
        btnQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.next();
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}