package demo.conghau.com.prudential.changepass;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import demo.conghau.com.prudential.login.MainActivity;
import demo.conghau.com.prudential.R;
import demo.conghau.com.prudential.base.BaseRespone;
import demo.conghau.com.prudential.utils.Utils;

public class ChangePassActivity extends AppCompatActivity implements View.OnClickListener{

    public ImageView imageBack;
    public EditText edtCurrentPass, edtNewPass, edtRetypePass;
    public TextView tvCurrentPassFail, tvNewPassFail, tvRetypePassFail;
    public CircularProgressButton btnConfirm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password);
        control();
    }

    public void control(){
        imageBack = findViewById(R.id.imgBack);
        edtCurrentPass = findViewById(R.id.currentPassword);
        edtNewPass = findViewById(R.id.newPassword);
        edtRetypePass = findViewById(R.id.retypePassword);
        tvCurrentPassFail = findViewById(R.id.currentPasswordFail);
        tvNewPassFail = findViewById(R.id.newPasswordFail);
        tvRetypePassFail = findViewById(R.id.retypePasswordFail);
        btnConfirm = findViewById(R.id.btnConfirmPass);

        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChangePassActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        btnConfirm.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnConfirmPass:

                if (btnConfirm.getProgress() == 0) {
                    if (checkValidate()) {
                        changePass();
                    }
                } else {
                    btnConfirm.setProgress(0);
                }
                break;
            default:
                break;
        }
    }

    private Boolean checkValidate() {
        Boolean checkCurrentPass = true, checkNewPass = true, checkVerifyPass = true, checkVeri;

        checkCurrentPass = edtCurrentPass.getText().toString().isEmpty() || !Utils.validatePassword(edtCurrentPass.getText().toString());
        checkNewPass = edtNewPass.getText().toString().isEmpty() || !Utils.validatePassword(edtNewPass.getText().toString());
        checkVerifyPass = edtRetypePass.getText().toString().isEmpty() || !Utils.validatePassword(edtRetypePass.getText().toString());
        if (edtNewPass.getText().toString().equals(edtRetypePass.getText().toString())) {
            checkVeri = false;
        } else {
            checkVeri = true;
        }

        if (checkCurrentPass || checkNewPass || checkVerifyPass || checkVeri) {
            if (checkCurrentPass) {
                tvCurrentPassFail.setVisibility(View.VISIBLE);
                tvCurrentPassFail.setText(R.string.txt_not_pass_);
            } else {
                tvCurrentPassFail.setVisibility(View.INVISIBLE);
            }
            if (checkNewPass) {
                tvNewPassFail.setVisibility(View.VISIBLE);
                tvNewPassFail.setText(getString(R.string.txt_not_pass_));
            } else {
                tvNewPassFail.setVisibility(View.INVISIBLE);
            }
            if (checkVerifyPass) {
                tvRetypePassFail.setVisibility(View.VISIBLE);
                tvRetypePassFail.setText(getString(R.string.txt_not_pass_));
            } else {
                if (checkVeri) {
                    tvRetypePassFail.setVisibility(View.VISIBLE);
                    tvRetypePassFail.setText(R.string.txt_not_mat);
                    return false;
                } else {
                    tvRetypePassFail.setVisibility(View.INVISIBLE);
                }
            }
            return false;
        }
        tvRetypePassFail.setVisibility(View.INVISIBLE);
        return true;
    }

    public void changePass(){

    }

    public void onSuccess(Object objectSuccess) {

        try {
            if (objectSuccess != null) {
                BaseRespone respone = (BaseRespone) objectSuccess;
                if (respone.result_code.equals("0")) {
                    simulateSuccessProgress(btnConfirm);
                } else {
                    simulateErrorProgress(btnConfirm);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void simulateSuccessProgress(final CircularProgressButton button) {
        ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 100);
        widthAnimation.setDuration(1500);
        widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                button.setProgress(value);
            }
        });
        widthAnimation.start();
    }

    private void simulateErrorProgress(final CircularProgressButton button) {
        ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 99);
        widthAnimation.setDuration(1500);
        widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                button.setProgress(value);
                if (value == 99) {
                    button.setProgress(-1);
                }
            }
        });
        widthAnimation.start();
    }

}
