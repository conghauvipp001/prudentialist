package demo.conghau.com.prudential.location.model;

public class Contact  {

    public String _mlagn;
    public String _mlong;
    public String name;
    public String address;
    public String phone;

    public Contact(String _mlagn, String _mlong, String name, String address, String phone) {
        this._mlagn = _mlagn;
        this._mlong = _mlong;
        this.name = name;
        this.address = address;
        this.phone = phone;
    }
}
