package demo.conghau.com.prudential.login;

import java.io.Serializable;

public class LoginRequest implements Serializable {

    public String username;
    public String password;
    public String deviceId;
    public String deviceName;

    public LoginRequest() {
    }

    public LoginRequest(String username, String password, String deviceId, String deviceName) {
        this.username = username;
        this.password = password;
        this.deviceId = deviceId;
        this.deviceName = deviceName;
    }

}
