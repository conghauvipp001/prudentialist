package demo.conghau.com.prudential.base;

public interface IBaseView<T> {
    void showLoading();
    void hideLoading();
    void onSuccess(T objectSuccess);
    void onError(T objectError);
    <T> void updateUI(T object);
}