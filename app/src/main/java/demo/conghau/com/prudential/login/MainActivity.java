package demo.conghau.com.prudential.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.VideoView;

import java.util.Objects;

import demo.conghau.com.prudential.R;
import demo.conghau.com.prudential.activity.HomeActivity;
import demo.conghau.com.prudential.activity.LoginActivity;
import demo.conghau.com.prudential.base.BaseActivity;
import demo.conghau.com.prudential.base.BaseFragment;

import demo.conghau.com.prudential.changepass.ChangePassActivity;
import demo.conghau.com.prudential.contact.ContactActivity;
import demo.conghau.com.prudential.faq.FAQActivity;
import demo.conghau.com.prudential.location.API.APIManager;
import demo.conghau.com.prudential.location.LocationActivity;
import demo.conghau.com.prudential.location.model.model_view.ResponseErrorModel;

import demo.conghau.com.prudential.pin.ChangePinActivity;
import demo.conghau.com.prudential.utils.ShareReference;

import static demo.conghau.com.prudential.utils.ShareReference.MY_PREFS_NAME;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvRegister, tvForgotPass;
    private Button btnLogin;
    private RelativeLayout layoutLogin;
    private EditText edtUsername, edtPassword;
    private TextView tvFalseUser, tvFalsePassword;
    private VideoView videoView;
    private LinearLayout layoutOffice, layoutContract, layoutFaq;
    private Switch switch_lang;
    private ILoginPresenter presenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_main);
        control();
        setOnClickListener();
    }

    protected void control() {

        tvRegister = findViewById(R.id.tvRegister);
        tvForgotPass = findViewById(R.id.tvforgotPassword);
        btnLogin = findViewById(R.id.btnLogin);
        layoutLogin = findViewById(R.id.loginLayout);
        edtUsername = findViewById(R.id.edtUsername);
        edtPassword = findViewById(R.id.edtPassword);
        tvFalseUser = findViewById(R.id.tvFalseUserName);
        tvFalsePassword = findViewById(R.id.tvFalsePassword);
        layoutOffice = findViewById(R.id.officeLayout);
        layoutContract = findViewById(R.id.contactLayout);
        layoutFaq = findViewById(R.id.FAQLayout);
        videoView = findViewById(R.id.videoView);
        //switch_lang = findViewById(R.id.switchLanguage);
    }

    protected void setOnClickListener() {

        tvRegister.setOnClickListener(this);
        tvForgotPass.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        layoutLogin.setOnClickListener(this);
        layoutOffice.setOnClickListener(this);
        layoutContract.setOnClickListener(this);
        layoutFaq.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        setBackGroundVideo("https://storage.googleapis.com/coverr-main/mp4/Mt_Baker.mp4");
    }


    private void setBackGroundVideo(String url) {

        try {
            videoView.setVideoURI(Uri.parse(url));
            videoView.start();
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mp) {
                    new android.os.Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            videoView.setBackgroundDrawable(null);
                        }
                    }, 100);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoView.start();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.tvFalsePassword:
                //Intent intent = new Intent(MainActivity.this, ForgotPass.class);
                //startActivity(intent);

            case R.id.officeLayout:
                Intent intent = new Intent(MainActivity.this, LocationActivity.class);
                startActivity(intent);
                break;

            case R.id.contactLayout:
                Intent intent1 = new Intent(MainActivity.this, ContactActivity.class);
                startActivity(intent1);
                break;

            case R.id.tvforgotPassword:
                Intent intent2 = new Intent(MainActivity.this, ChangePassActivity.class);
                startActivity(intent2);
                break;

            case R.id.tvRegister:
                Intent intent3 = new Intent(MainActivity.this, ChangePinActivity.class);
                startActivity(intent3);
                break;

            case R.id.FAQLayout:
                Intent intent4 = new Intent(MainActivity.this, FAQActivity.class);
                startActivity(intent4);
        }
    }

    private boolean CheckValidate() {
        if (edtUsername.getText().toString().isEmpty() || edtPassword.getText().toString().isEmpty()) {
            if (edtUsername.getText().toString().isEmpty()) {
                tvFalseUser.setVisibility(View.VISIBLE);
                tvFalseUser.setText(R.string.txt_use);
            } else {
                tvFalseUser.setVisibility(View.GONE);
            }
            if (edtPassword.getText().toString().isEmpty()) {
                tvFalsePassword.setVisibility(View.VISIBLE);
                tvFalsePassword.setText(R.string.txt_pass);
            } else {
                tvFalsePassword.setVisibility(View.GONE);
            }
            return false;
        } else {
            tvFalseUser.setVisibility(View.GONE);
            tvFalsePassword.setVisibility(View.GONE);
        }
        return true;
    }

    public void login() {
        try {
            final String deviceId = Settings.Secure.getString(this.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
            //final String deviceId = "2121dđsd2d1đ21ds2sd1";
            final String deviceName = android.os.Build.MODEL;
            APIManager.deviceID = deviceId;
                    CheckNetworkRequest(this, new BaseFragment.CheckNetworkListener() {
                        @Override
                        public void Connected() {
                            presenter.login(new LoginRequest(edtUsername.getText().toString(), edtPassword.getText().toString(), deviceId, deviceName));
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void CheckNetworkRequest(MainActivity mainActivity, BaseFragment.CheckNetworkListener listener) {
    }

//    private void onSuccess(Objects objectsSuccess){
//        try {
//            if(objectsSuccess != null) {
//                ResponseLogin logndata = (ResponseLogin) objectsSuccess;
//                if (logndata.data != null) {
//                    APIManager.mAccessToken = logndata.data.accessToken;
//                    // save loginted
//                    SharedPreferences.Editor editor = this.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
//                    editor.putBoolean("isLogin", true);
//                    editor.putString("Username", edtUsername.getText().toString().trim());
//                    editor.apply();
//                    ShareReference.isLogin = true;
//                    if (logndata.result_code.equals("0")) {
//                        if (Boolean.valueOf(logndata.data.verifyOTP) || Boolean.valueOf(logndata.data.createPin)) {
//                            goToHome(logndata);
//                        } else {
//                            Intent intent = new Intent(this, vn.mobiapps.pruf.view.activity.CustomerPageActivity.class);
//                            startActivity(intent);
//                            this.finish();
//                        }
//                    }
//                } else {
//                    if (logndata.result_code.equals("-1")){
//                        if (logndata.result_description.equals("Password failed")) {
//                            tvFalseUser.setVisibility(View.VISIBLE);
//                            tvFalsePassword.setText(logndata.result_description);
//                        } else {
//                            tvFalsePassword.setVisibility(View.GONE);
//                        }
//
//                        if (logndata.result_description.equals("Username incorrect")) {
//                            tvFalseUser.setVisibility(View.VISIBLE);
//                            tvFalseUser.setText(logndata.result_description);
//                        } else {
//                            tvFalseUser.setVisibility(View.GONE);
//                        }
//                    }
//             }
//             }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    private void goToHome(ResponseLogin logndata){
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra("LognData", logndata);
        intent.putExtra("isRetrieve", false);
        startActivity(intent);
        this.finish();
    }

//    public void onError(Objects objectsError){
//        ResponseErrorModel onError = (ResponseErrorModel) objectsError;
//        if (onError.getResult_description() != null) {
//            showMessage(onError.getResult_description());
//        }
//    }

    public void getBackGround(){
        CheckNetworkRequest(this, new BaseFragment.CheckNetworkListener() {
            @Override
            public void Connected() {

            }
        });
    }

}
