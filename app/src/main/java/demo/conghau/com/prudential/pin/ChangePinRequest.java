package demo.conghau.com.prudential.pin;

import java.io.Serializable;

public class ChangePinRequest implements Serializable {

        public String accessToken;
        public String currentPin;
        public String newPin;
}
