package demo.conghau.com.prudential.location;

import java.io.Serializable;

public class ListOffice implements Serializable {

    public String longtitude;
    public String latitude;
    public String name;
    public String address;
    public String phone;


    public ListOffice(String longtitude, String latitude, String name, String address, String phone) {

        this.longtitude = longtitude;
        this.latitude = latitude;
        this.name = name;
        this.address = address;
        this.phone = phone;

    }
}
