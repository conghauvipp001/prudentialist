package demo.conghau.com.prudential.location;

import demo.conghau.com.prudential.base.IBaseView;

public interface ILocationView extends IBaseView {

    void getListOffice (String language, String region);
}
