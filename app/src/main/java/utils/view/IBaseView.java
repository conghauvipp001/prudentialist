package utils.view;

interface IBaseView<T> {

    void showLoading();
    void hideLoading();
    void onSuccess();
    void onError();
}
